<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_orders', function (Blueprint $table) {
            $table->id();
            $table->String("user_id");
            $table->String("packageName");
            $table->String("acknowledged")->comment("false = 0 || true = 1");
            $table->String("orderId");
            $table->String("productId")->comment("coin that purchase from the user");
            $table->text("token");
            $table->String("developerPayload");
            $table->String("purchaseTime")->comment("false = 0 || true = 1");
            $table->String("purchaseState")->comment("false = 0 || true = 1");
            $table->text("purchaseToken");
            $table->String("inapp");
            $table->String("username");
            $table->String("code");
            $table->String("two_factor_identifier")->comment("false = 0 || true = 1");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_orders');
    }
}
