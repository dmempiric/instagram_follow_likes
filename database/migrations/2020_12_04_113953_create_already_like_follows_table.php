<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlreadyLikeFollowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('already_like_follows', function (Blueprint $table) {
            $table->id();
            $table->String('type');
            $table->String('user_id');
            $table->String('custom_user_id')->default("0");
            $table->String('post_id')->nullable();
            $table->String('already_like_follow');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('already_like_follows');
    }
}
