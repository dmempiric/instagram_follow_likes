<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_details', function (Blueprint $table) {
            $table->id();
            $table->integer('quantity');
            $table->text('indian_rate');
            $table->text('other_rate');
            $table->text('notes');
            $table->text('is_popular')->comment('0 = Not Popular / 1 = Popular')->default('0');
            $table->enum('coin_status', ['0', '1'])->default('1')->comment = '1 = active / 0 = deactive';
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_details');
    }
}
