<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('user_id');
            $table->text('ftoken');
            $table->string('parent_id')->default("0");
            $table->string('username');
            $table->string('profile_picture')->nullable();
            $table->string('total_coins');
            $table->string('referral_code');
            $table->enum('is_purchase', ['0', '1'])->default('0')->comment = '0 = NOT PURCHASED / 1 PURCHASE';
            $table->enum('is_referred', ['0', '1'])->default('0')->comment = '0 = NOT REFFERED / 1 REFFERED';
            $table->enum('status', ['0', '1'])->default('1')->comment = '1 = active / 0 = deactive';
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
