<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->String("type")->comment = '1 = like / 0 = follow';;
            $table->String("user_id");
            $table->String("username")->nullable();
            $table->String("custom_user_id")->default("0")->nullable();
            $table->String("post_id");
            $table->String("short_code")->nullable();
            $table->String("needed");
            $table->String("recieved");
            $table->String("image_url");
            $table->String("no_like_log")->nullable()->default("0");
            $table->String("reason_for_cancelorder")->nullable()->comment('reason for cancel the order');
            $table->enum('order_status', ['0', '1'])->default('1')->comment = '1 = active / 0 = deactive';
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
