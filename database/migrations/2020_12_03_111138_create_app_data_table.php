<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_data', function (Blueprint $table) {
            $table->id();
            $table->String('earn_like_coin');
            $table->String('spend_like_coin');
            $table->String('earn_follow_coin');
            $table->String('spend_follow_coin');
            $table->String('referral_coin');
            $table->String('default_coin');
            $table->String('from_add_coin')->nullable();
            $table->String('maintenence_mode');
            $table->String('update_mode');
            $table->String('update_url');
            $table->String('payment_methode');
            $table->String('game_id');
            $table->String('banner_id');
            $table->String('initial_id');
            $table->String('maintenence_message');
            $table->String('notification_title');
            $table->String('notification_message');
            $table->String('notification_show');
            $table->String('update_message');
            $table->String('playstore_version');
            $table->String('web');
            $table->String('website');
            $table->text('user_agent');
            $table->String('share_url');
            $table->String('offer')->comment('0 = offer end / 1 = offer start ');
            $table->String('offer_percentage');
            $table->String('offer_starttime');
            $table->String('offer_endtime');
            $table->String('offer_discount_title'); 
            $table->String('offer_discount_text'); 
            $table->String('offer_discount_image');
            $table->String('web_login')->comment('0 = web / 1 = from app ');
            $table->String('email');
            $table->String('privacy_policy');
            $table->String('facebook');
            $table->String('instagram');
            $table->String('rate_dialog')->comment('0 = no / 1 = yes');
            $table->String('share_dialog')->comment('0 = no / 1 = yes');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_data');
    }
}
