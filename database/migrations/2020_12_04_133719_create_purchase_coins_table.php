<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_coins', function (Blueprint $table) {
            $table->id();
            $table->text('user_id');
            $table->text('payment_id')->nullable();
            $table->text('payment_type')->nullable();
            $table->integer('purchased_coin');
            $table->integer('amount');
            $table->text('payment_state')->nullable();
            $table->text('payment_time')->nullable();
            $table->text('country_code')->nullable();
            $table->text('email')->nullable();
            $table->text('payment_type')->nullable();
            $table->text('payment_method');
            $table->text('transaction_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_coins');
    }
}
