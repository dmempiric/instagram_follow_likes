@extends('layouts.adminmaster')

@section('css')
 
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Order List</h1> </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href='{{ url("home") }}'>Home</a></li>
                            <li class="breadcrumb-item "><a href='{{ url("order") }}'>Order List</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                    </div>
                  @include('layouts.flash-message')
                  <div class="card">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    @include('order.inc.orderbutton')  
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>No</th>
                          <th>Type</th>
                          <th>User Id</th>
                          <th>Custom User Id</th>
                          <th>Post Id</th>
                          <th>Short Code</th>
                          <th>Needed</th>
                          <th>Recieved</th>
                          <th>Image Url</th>
                          <th>From App</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 0;
                            @endphp
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>
                                    @if($order->type == 0)
                                        <span class="badge bg-warning">Like</span>
                                    @elseif($order->type == 1)
                                        <span class="badge bg-info">Follow</span>
                                    @endif
                                </td>
                                <td>{{ $order->user_id  }}</td>
                                <td>
                                    @if (isset($order->custom_user_id))
                                        {{ $order->custom_user_id  }}
                                    @endif
                                </td>
                                <td>{{ $order->post_id  }}</td>
                                <td>{{ $order->short_code  }}</td>
                                <td>{{ $order->needed  }}</td>
                                <td>{{ $order->recieved  }}</td>
                                <td>
                                    @if($order->type == 0)
                                        <img src='{{ url("/public/instagram/like/$order->image_url") }}' width="70px;">
                                    @elseif($order->type == 1)
                                        <img src='{{ url("/public/instagram/follow/$order->image_url") }}' width="70px;" title="follow">
                                @endif
                                    </td>
                                    <td>
                                        @if($order->fromapp == 1)
                                        <span class="badge bg-secondary">App</span>
                                        @else
                                            <span class="badge bg-info">Web</span>
                                        @endif
                                    </td>
                                <td>
                                    @if($order->order_status == 1)
                                        <span class="badge bg-success">Active</span>
                                    @else
                                        <span class="badge bg-danger">InActive</span>
                                    @endif
                                </td>
                                <td>
                                    {{-- <a class="btn btn-primary"  href='{{  url("admin/color/$color->id") }}' >Show</a> --}}
                                    <a class="btn btn-warning" href='{{ url("order/$order->id/edit") }}'>Edit</a>
                                    <form action='{{ url("order/$order->id") }}'  method="POST">
                                    @csrf
                                    @method('delete')
                                        <button type="submit" name="submit" class="btn btn-danger">Delete</button>
                                    </form>   
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
          </section>
        <!-- Main content -->
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });
    </script>    
@endsection
