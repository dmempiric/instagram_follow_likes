@extends('layouts.adminmaster')

@section('css')
 
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Coin Detail</h1> </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href='{{ url("home") }}'>Home</a></li>
                            <li class="breadcrumb-item "><a href='{{ url("coindetail") }}'>Coin Detail</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                    </div>
                  @include('layouts.flash-message')
                  <div class="card">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <a href='{{ url("coindetail/create") }}' class="btn btn-primary">Add Coin Detail</a>
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>No</th>
                          <th>Quantity</th>
                          <th>Indian Rate</th>
                          <th>Other Rate</th>
                          <th>Notes</th>
                          <th>IS Popular</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 0;
                            @endphp
                        @foreach ($coindetails as $coindetail)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $coindetail->quantity  }}</td>
                                <td>{{ $coindetail->indian_rate  }}</td>
                                <td>{{ $coindetail->other_rate  }}</td>
                                <td>{{ $coindetail->notes  }}</td>
                                <td>
                                    @if($coindetail->is_popular == 1)
                                        <span class="badge bg-info">Popular</span>
                                    @else
                                        <span class="badge bg-warning">Not Popular</span>
                                    @endif
                                </td>
                                <td>
                                    @if($coindetail->coin_status == 1)
                                        <span class="badge bg-success">Active</span>
                                    @else
                                        <span class="badge bg-danger">InActive</span>
                                    @endif
                                </td>
                                <td>
                                    {{-- <a class="btn btn-primary"  href='{{  url("admin/color/$color->id") }}' >Show</a> --}}
                                    <a class="btn btn-warning" href='{{ url("coindetail/$coindetail->id/edit") }}'>Edit</a>
                                    <form action='{{ url("coindetail/$coindetail->id") }}'  method="POST">
                                    @csrf
                                    @method('delete')
                                        <button type="submit" name="submit" class="btn btn-danger">Delete</button>
                                    </form>   
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
          </section>
        <!-- Main content -->
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });
    </script>    
@endsection
