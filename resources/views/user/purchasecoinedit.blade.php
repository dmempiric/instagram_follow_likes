@extends('layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>User</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("userlist") }}'>User</a></li>
                  <li class="breadcrumb-item active">create</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Add User</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <form id="quickForm" action='{{ url("purchasecoinupdate/$purchasecoinlist->id") }}' method="POST">
                     @csrf
                     <div class="card-body">
                        <div class="form-group">
                           <label for="user_id">User id</label>
                           <input type="text" name="user_id" value='{{ $purchasecoinlist->user_id }}' class="form-control" id="name" placeholder="User Id" required >
                           <div class="error" style="color: red;">{{ $errors->first('User_id') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="email">Email</label>
                           <input type="email" name="email" value='{{ $purchasecoinlist->email }}' class="form-control" id="name" placeholder="Email" required >
                           <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="payment_id">Payment id</label>
                            <input type="text" name="payment_id" value='{{ $purchasecoinlist->payment_id }}' class="form-control" id="payment_id" placeholder="Payment Id" required >
                            <div class="error" style="color: red;">{{ $errors->first('payment_id') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="payment_type">Payment Type</label>
                            <input type="text" name="payment_type" value='{{ $purchasecoinlist->payment_type }}' class="form-control" id="payment_type" placeholder="Payment Type" required >
                            <div class="error" style="color: red;">{{ $errors->first('payment_type') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="purchased_coin">Payment Coin</label>
                            <input type="text" name="purchased_coin" value='{{ $purchasecoinlist->purchased_coin }}' class="form-control" id="purchased_coin" placeholder="Payment Coin" required >
                            <div class="error" style="color: red;">{{ $errors->first('purchased_coin') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="payment_state">Payment Stae</label>
                            <input type="text" name="payment_state" value='{{ $purchasecoinlist->payment_state }}' class="form-control" id="payment_state" placeholder="Payment State" required >
                            <div class="error" style="color: red;">{{ $errors->first('payment_state') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="payment_time">Payment Time</label>
                            <input type="text" name="payment_time" value='{{ date("m-d-Y H:i:s",strtotime($purchasecoinlist->payment_time)) }} ' class="form-control" id="payment_time" placeholder="Payment Time" required >
                            <div class="error" style="color: red;">{{ $errors->first('payment_time') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="country_code">Country Code</label>
                            <input type="text" name="country_code" value='{{ $purchasecoinlist->country_code }}' class="form-control" id="country_code" placeholder="Country Code" required >
                            <div class="error" style="color: red;">{{ $errors->first('country_code') }}</div>
                        </div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        <a href='{{ url('purchasecoin-list') }}'  class="btn btn-primary">Back</a>
                     </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>

</script>
@endsection
