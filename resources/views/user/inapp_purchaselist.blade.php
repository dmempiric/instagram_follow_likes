@extends('layouts.adminmaster')
@section('css')
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Purchase User List <b>Inapp Purchase</b></h1> </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href='{{ url("home") }}'>Home</a></li>
                            <li class="breadcrumb-item "><a href='{{ url("purchaseuser-list") }}'>Purchase User List/</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                    </div>
                  @include('layouts.flash-message')
                  <div class="card">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <a href='{{ url("purchasecoin-list") }}' class="btn btn-secondary">Paypal/RazorPay</a>
                        <a href='{{ url("inappPurchase-list") }}' class="btn btn-info">InApp</a>
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User Id</th>
                                <th>User Name</th>
                                <th>Order Id</th>
                                <th>Coin</th>
                                <th>Purchase time</th>
                                <th>Country Code</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 0;
                            @endphp
                        @foreach ($userorders as $userorder)
                            <tr>    
                                <td>{{ ++$i }}</td>
                                <td>{{ $userorder->user_id  }}</td>
                                @php
                                    $user = App\Models\User::where("user_id",$userorder->user_id)->first();    
                                @endphp
                                <td>{{ $user->username  }}</td>
                                <td>{{ $userorder->orderId  }}</td>
                                <td>{{ $userorder->productId  }}</td>
                                <td>{{ date('H:i:s d-m-Y',strtotime($userorder->created_at))  }}</td>
                                <td>{{ $userorder->regionCode  }}</td>
                                
                                {{-- <td>{{ date("m-d-Y H:i:s",strtotime($purchasecoinlist->payment_time)) }} </td> --}}
                                {{-- <td>{{ $purchasecoinlist->country_code  }}</td> --}}
                                <td>
                                    {{-- <a class="btn btn-primary"  href='{{  url("admin/color/$color->id") }}' >Show</a> --}}
                                    {{-- <a class="btn btn-warning" href='{{ url("inapp_purchaseedit/$userorder->id") }}'>Edit</a> --}}
                                    <a class="btn btn-danger" href='{{ url("inapp_purchasedelete/$userorder->id") }}'>Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
          </section>
        <!-- Main content -->
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });
    </script>    
@endsection
