@extends('layouts.adminmaster')

@section('css')
 
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Premium User</h1> </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href='{{ url("home") }}'>Home</a></li>
                            <li class="breadcrumb-item "><a href='{{ url("purchaseuser-list") }}'>Premium User</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                    </div>
                  @include('layouts.flash-message')
                  <div class="card">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form action='{{ url("search-premiumuserlist") }}' method="POST">
                            @csrf
                            <input type="text" @if(isset($requeststartdate)) value="{{  $requeststartdate  }}" @endif  id="startdate" name="startdate" class="pl-3" placeholder="Start Date" autocomplete="off"/> 
                            <input type="text"  @if(isset($requestenddate)) value="{{  $requestenddate  }}" @endif  id="enddate" name="enddate"  placeholder="End Date" autocomplete="off"/>
                            <button class="btn btn-primary m-1"> Submit </button>
                            <a class="btn btn-primary m-1" href="{{ url("purchaseuser-list") }}" > Reset</a>
                        </form>
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>No</th>
                          <th>User Id</th>
                          <th>User Name</th>
                          <th>Date Of Registration</th>
                          <th>Total Coins</th>
                          <th>Refferal Code</th>
                          <th>Reffered </th>
                          <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 0;
                            @endphp
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $user->user_id  }}</td>
                                <td>{{ $user->username  }}</td>
                                <td>{{ date('d-m-Y',strtotime($user->created_at))  }}</td>
                                <td>{{ $user->total_coins  }}</td>
                                <td>{{ $user->referral_code  }}</td>
                                @if($user->parent_id != "0")
                                    <td></td>
                                    <td></td>
                                @else
                                    <td>
                                        @if($user->is_referred == 1)
                                        <span class="badge bg-success">Referred</span>
                                        @else
                                            <span class="badge bg-danger">Not Referred</span>
                                        @endif</td>
                                @endif
                                <td>
                                    @if($user->status == 1)
                                        <span class="badge bg-success">Active</span>
                                    @else
                                        <span class="badge bg-danger">InActive</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
          </section>
        <!-- Main content -->
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
            $('#startdate').datetimepicker({
                timepicker:false,
                format:'d/m/Y',
                formatDate:'Y/m/d',
                // minDate:'-1970/01/02', // yesterday is minimum date
                // maxDate:'+1970/01/02' // and tommorow is maximum date calendar
            });
            $('#enddate').datetimepicker({
                timepicker:false,
                format:'d/m/Y',
                formatDate:'Y/m/d',
                // minDate:'-1970/01/02', // yesterday is minimum date
                // maxDate:'+1970/01/02' // and tommorow is maximum date calendar
            });
        });
        
    </script>    
    </script>    
@endsection
