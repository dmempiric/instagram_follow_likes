@extends('layouts.adminmaster')
@section('css')
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Purchase User List <b>PayPal / RazorPay</b></h1> </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href='{{ url("home") }}'>Home</a></li>
                            <li class="breadcrumb-item "><a href='{{ url("purchaseuser-list") }}'>Purchase User List</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                    </div>
                  @include('layouts.flash-message')
                  <div class="card">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <a href='{{ url("purchasecoin-list") }}' class="btn btn-secondary">Paypal/RazorPay</a>
                        <a href='{{ url("inappPurchase-list") }}' class="btn btn-info">InApp</a>
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User Id</th>
                                <th>Transaction Id</th>
                                <th>Email</th>
                                <th>Payment Id</th>
                                <th>Payment Type</th>
                                <th>Purchase Coin</th>
                                <th>Payment State</th>
                                <th>Payment Time</th>
                                <th>Country Code</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 0;
                            @endphp
                        @foreach ($purchasecoinlists as $purchasecoinlist)
                            <tr>    
                                <td>{{ ++$i }}</td>
                                <td>{{ $purchasecoinlist->user_id  }}</td>
                                <td>{{ $purchasecoinlist->transaction_id  }}</td>
                                <td>{{ $purchasecoinlist->email  }}</td>
                                <td>{{ $purchasecoinlist->payment_id  }}</td>
                                <td>{{ $purchasecoinlist->payment_type	  }}</td>
                                <td>{{ $purchasecoinlist->purchased_coin  }}</td>
                                <td>{{ $purchasecoinlist->payment_state  }}</td>
                                <td>{{ date("m-d-Y H:i:s",strtotime($purchasecoinlist->payment_time)) }} </td>
                                <td>{{ $purchasecoinlist->country_code  }}</td>
                                <td>
                                    {{-- <a class="btn btn-primary"  href='{{  url("admin/color/$color->id") }}' >Show</a> --}}
                                    <a class="btn btn-warning" href='{{ url("purchasecoinedit/$purchasecoinlist->id") }}'>Edit</a>
                                    <a class="btn btn-danger" href='{{ url("purchasecoindelete/$purchasecoinlist->id") }}'>Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
          </section>
        <!-- Main content -->
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });
    </script>    
@endsection
