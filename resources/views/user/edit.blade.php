@extends('layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>User</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("userlist") }}'>User</a></li>
                  <li class="breadcrumb-item active">edit</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Edit User</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <form id="quickForm" action='{{ url("userupdate/$user->id") }}' method="POST">
                     @csrf
                     <div class="card-body">
                        <div class="form-group">
                           <label for="user_id">User_id</label>
                           <input type="text" name="user_id" value='{{ $user->user_id }}' class="form-control" id="name" placeholder="User Id" required >
                           <div class="error" style="color: red;">{{ $errors->first('User_id') }}</div>
                        </div>
                        @if($user->parent_id != "0")
                           <div class="form-group">
                              <label for="parent_id">User_id</label>
                              <input type="text" name="parent_id" value='{{ $user->parent_id }}' class="form-control" id="parent_id" placeholder="Parent Id" required >
                              <div class="error" style="color: red;">{{ $errors->first('parent_id') }}</div>
                           </div>
                        @endif
                        <div class="form-group">
                           <label for="username">User Name</label>
                           <input type="text" name="username" value='{{ $user->username }}' class="form-control" id="name" placeholder="User Name" required >
                           <div class="error" style="color: red;">{{ $errors->first('username') }}</div>
                        </div>
                        @if($user->parent_id == "0")
                           <div class="form-group">
                              <label for="total_coins">Total Coins</label>
                              <input type="text" name="total_coins" value='{{ $user->total_coins }}' class="form-control" id="name" placeholder="Total Coins" required >
                              <div class="error" style="color: red;">{{ $errors->first('total_coins') }}</div>
                           </div>
                           <div class="form-group">
                              <label for="referral_code">Referral Code</label>
                              <input type="text" name="referral_code" value='{{ $user->referral_code }}' class="form-control" id="name" placeholder="Total Coins" required >
                              <div class="error" style="color: red;">{{ $errors->first('referral_code') }}</div>
                           </div>
                           <div class="form-group">
                              <label for="is_purchase"> User Purchased</label>
                              <select class="form-control" name="is_purchase" required>
                                 <option disabled>User Purchased</option>
                                 <option value="1" @if($user->is_purchase == 1) selected @endif>Purchased</option>
                                 <option value="0" @if($user->is_purchase == 0) selected @endif>Not Purchased</option>
                               </select> 
                           </div>
                           <div class="form-group">
                              <label for="is_referred"> User Referred</label>
                              <select class="form-control" name="is_referred" required>
                                 <option disabled>User Purchased</option>
                                 <option value="1" @if($user->is_referred == 1) selected @endif>Reffered </option>
                                 <option value="0" @if($user->is_referred == 0) selected @endif>Not Reffered </option>
                               </select> 
                           </div>
                        @endif

                        <div class="form-group">
                           <label for="status"> User Status</label>
                           <select class="form-control" name="status" required>
                                 <option disabled>User Status</option>
                                 <option value="1" @if($user->status == 1) selected @endif>Active</option>
                                 <option value="0" @if($user->status == 0) selected @endif>InActive</option>
                           </select>   
                        </div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        <a href='{{ url('userlist') }}'  class="btn btn-primary">Back</a>
                     </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>

</script>
@endsection
