@extends('get_likes.layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dashboard</h1>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-3">
                        <a href="{{ url("get_likes/userlist") }}" >
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1">
                                    <i class="fas fa-user"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text text-dark">Total Users</span>
                                    <span class="info-box-number text-dark">
                                    <?php   echo number_format($total_users).""; ?></span>
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </a>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                        <div class="col-12 col-sm-6 col-md-3">
                            <a href="{{ url("get_likes/orderlike") }}" >
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text text-dark">Like order</span>
                                        <span class="info-box-number text-dark">
                                        <?php   echo number_format($total_like_orders).""; ?></span>
                                        </span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </a>
                        </div>
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix hidden-md-up"></div>

                   
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-3">
                        <a href="{{ url("get_likes/userlist") }}" >
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text text-dark" >Active Users</span>
                                    <span class="info-box-number text-dark">
                                        <?php   echo number_format($total_active_user).""; ?></span>
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3">
                        <a href="{{ url("get_likes/purchaseuser-list") }}" >
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-light elevation-1"><i class="fas fa-users"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text text-dark">Premium Users</span>
                                    <span class="info-box-number text-dark">
                                        <?php   echo number_format($total_purchase_users).""; ?>
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </a>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar p-->

@endsection