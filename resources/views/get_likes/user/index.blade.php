@extends('get_likes.layouts.adminmaster')

@section('css')
 
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>User List</h1> </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                            <li class="breadcrumb-item "><a href='{{ url("get_likes/userlist") }}'>User List</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                    </div>
                  @include('layouts.flash-message')
                  <div class="card">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form action='{{ url("get_likes/search-userlist") }}' method="POST">
                            <a class="btn btn-primary m-1" href="{{ url("get_likes/user/create") }}" > Add User</a>
                            @csrf
                            <input type="text" @if(isset($requeststartdate)) value="{{  $requeststartdate  }}" @endif  id="startdate" name="startdate" class="pl-3" placeholder="Start Date"/> 
                            <input type="text"  @if(isset($requestenddate)) value="{{  $requestenddate  }}" @endif  id="enddate" name="enddate"  placeholder="End Date"/>
                            <button class="btn btn-primary m-1"> Submit </button>
                            <a class="btn btn-primary m-1" href="{{ url("userlist") }}" > Reset</a>
                        </form>
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>No</th>
                          <th>User Id</th>
                          <th>Parent Id</th>
                          <th>User Name</th>
                          <th>Date of Registation</th>
                          <th>Total Coins</th>
                          <th>Refferal Code</th>
                          <th>From App</th>
                          <th>Purchased </th>
                          <th>Reffered </th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 0;
                            @endphp
                        @foreach ($users as $user)
                            <tr>
                                
                                <td>{{ ++$i }}</td>
                                <td>{{ $user->user_id  }}</td>
                                @if($user->parent_id != "0")
                                    <td>{{ $user->parent_id  }}</td>
                                    @else
                                    <td>0</td>
                                    @endif
                                <td>{{ $user->username  }}</td>
                                <td>{{ date('d-m-Y',strtotime($user->created_at))  }}</td>
                                <td>{{ $user->total_coins  }}</td>
                                <td>{{ $user->referral_code  }}</td>
                                <td>
                                    @if($user->fromapp == 1)
                                    <span class="badge bg-secondary">App</span>
                                    @else
                                        <span class="badge bg-info">Web</span>
                                    @endif
                                </td>
                                @if($user->parent_id != "0")
                                    <td></td>
                                    <td></td>
                                @else
                                    <td>
                                        @if($user->is_purchase == 1)
                                        <span class="badge bg-success">Purchased</span>
                                        @else
                                            <span class="badge bg-danger">Not Purchased</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($user->is_referred == 1)
                                        <span class="badge bg-success">Referred</span>
                                        @else
                                            <span class="badge bg-danger">Not Referred</span>
                                        @endif</td>
                                @endif
                                <td>
                                    @if($user->status == 1)
                                        <span class="badge bg-success">Active</span>
                                    @else
                                        <span class="badge bg-danger">InActive</span>
                                    @endif
                                </td>
                                <td>
                                    {{-- <a class="btn btn-primary"  href='{{  url("admin/color/$color->id") }}' >Show</a> --}}
                                    <a class="btn btn-warning" href='{{ url("get_likes/useredit/$user->id") }}'>Edit</a>
                                    <a class="btn btn-danger" href='{{ url("get_likes/userdelete/$user->id") }}'>Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                       
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
          </section>
        <!-- Main content -->
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
            $('#startdate').datetimepicker({
                timepicker:false,
                format:'d/m/Y',
                formatDate:'Y/m/d',
                // minDate:'-1970/01/02', // yesterday is minimum date
                // maxDate:'+1970/01/02' // and tommorow is maximum date calendar
            });
            $('#enddate').datetimepicker({
                timepicker:false,
                format:'d/m/Y',
                formatDate:'Y/m/d',
                // minDate:'-1970/01/02', // yesterday is minimum date
                // maxDate:'+1970/01/02' // and tommorow is maximum date calendar
            });
        });
        
    </script>    
@endsection
