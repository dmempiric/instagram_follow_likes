<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Instagram Likes</title>
	<meta name="csrf-token" content="{{ csrf_token() }}"> 	
	<!-- jQuery -->
	<script src='{{ asset("assets/get_likes/plugins/jquery/jquery.min.js") }}'></script>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href='{{ asset("assets/get_likes/plugins/fontawesome-free/css/all.min.css") }}'>
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href='{{ asset("assets/get_likes/plugins/overlayScrollbars/css/OverlayScrollbars.min.css") }}'>
	<!-- DataTables -->
	<link rel="stylesheet" href='{{ asset("assets/get_likes/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css") }}'>
	<link rel="stylesheet" href='{{ asset("assets/get_likes/plugins/datatables-responsive/css/responsive.bootstrap4.min.css") }}'>
	<link rel="stylesheet" href='{{ asset("assets/get_likes/plugins/daterangepicker/daterangepicker.css") }}'>
	<!-- Theme style -->
	<link rel="stylesheet" href='{{ asset("assets/get_likes/css/adminlte.min.css") }}'>
	<link rel="stylesheet" href='{{ asset("assets/get_likes/plugins/bootstrap/css/bootstrap.min.css") }}' crossorigin="anonymous">
	<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
	<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> @yield('css') </head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.min.css" />
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
	<div class="wrapper">
		<!-- Navbar -->
		<nav class="main-header navbar navbar-expand navbar-white navbar-light">
			<!-- Left navbar links -->
			<ul class="navbar-nav">
				<li class="nav-item"> <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a> </li>
				<li class="nav-item d-none d-sm-inline-block"> <a href='{{ url("get_likes/home") }}' class="nav-link">Home</a> </li>
				<li class="nav-item d-none d-sm-inline-block">
					<a class="nav-link" href="{{ url('get_likes/logout') }}">Logout</a>
				</li>
			</ul>
			<!-- SEARCH FORM -->
			<form class="form-inline ml-3">
				<div class="input-group input-group-sm">
					<div class="input-group-append">
					</div>
				</div>
			</form>
			<!-- Right navbar links -->
			<ul class="navbar-nav ml-auto">
				<!-- Messages Dropdown Menu -->
				  
				<li class="nav-item">
					<a class="nav-link" data-widget="fullscreen" href="#" role="button"> <i class="fas fa-expand-arrows-alt"></i> </a>
				</li>
				{{-- <li class="nav-item">
					<a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"> <i class="fas fa-th-large"></i> </a>
				</li> --}}
			</ul>
		</nav>
		<!-- /.navbar -->
		<!-- Main Sidebar Container -->
		<aside class="main-sidebar sidebar-dark-primary elevation-4">
			<!-- Brand Logo -->
			<a href="index3.html" class="brand-link"> <img src='{{ url("assets/img/AdminLTELogo.png") }}' alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> <span class="brand-text font-weight-light">Instagram Likes</span> </a>
			<!-- Sidebar -->
			<div class="sidebar">
				<!-- Sidebar user panel (optional) -->
				<div class="user-panel mt-3 pb-3 mb-3 d-flex">
					@php
					@endphp
					<div class="image"> <img @if(isset(Auth::user()->profile_picture)) src='{{ url('assets/get_likes/img/'.Auth::user()->profile_picture) }}' @else src='{{ url("get_likes/assets/img/user2-160x160.jpg") }}' @endif class="img-circle elevation-2" alt="User Image"> </div>
					<div class="info"> <a href="#" class="d-block">{{ Auth::user()->name }}</a> </div>
				</div>
				<!-- SidebarSearch Form -->
				<div class="form-inline">
					<div class="input-group" data-widget="sidebar-search">
						<input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
						<div class="input-group-append">
							<button class="btn btn-sidebar"> <i class="fas fa-search fa-fw"></i> </button>
						</div>
					</div>
				</div>
				<!-- Sidebar Menu -->
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
						<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
						 
						<li class="nav-item">
							<a href='{{ url("get_likes/home") }}' class="nav-link"> <i class="fas fa-tachometer-alt nav-icon"></i>
								<p>Dashboard</p>
							</a>
						</li>
						{{-- <li class="nav-item">
            					<a href='{{ url("get_likes/appdata") }}' class="nav-link"> <i class="nav-icon fas fa-th"></i>
								<p> App Data </p>
							</a>

						</li> --}}
						<li class="nav-item">
							<a href="#" class="nav-link">
							  <i class="nav-icon fas fa-circle"></i>
							  <p>
								App Data
								<i class="right fas fa-angle-left"></i>
							  </p>
							</a>
							<ul class="nav nav-treeview">
							  <li class="nav-item">
								<a href='{{ url("get_likes/appdata-coindetail") }}' class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p>Coin Detail12</p>
								</a>
							  </li>
							  <li class="nav-item">
								<a href='{{ url("get_likes/appdata-offer") }}' class="nav-link ">
									<i class="far fa-circle nav-icon"></i>
									<p>Offer</p>
								</a>
							  </li>
							  <li class="nav-item">
								<a href='{{ url("get_likes/appdata-maintenence") }}' class="nav-link ">
									<i class="far fa-circle nav-icon"></i>
									<p>Maintenence</p>
								</a>
							  </li>
							  <li class="nav-item">
								<a href='{{ url("get_likes/appdata-game") }}' class="nav-link ">
									<i class="far fa-circle nav-icon"></i>
									<p>Ads Detail	</p>
								</a>
							  </li>
							  <li class="nav-item">
								<a href='{{ url("get_likes/appdata-credential-token-notification") }}' class="nav-link ">
									<i class="far fa-circle nav-icon"></i>
									<p>Creadetial Google token || Notification</p>
								</a>
							  </li>
							  <li class="nav-item">
								<a href='{{ url("get_likes/appdata-other") }}' class="nav-link ">
									<i class="far fa-circle nav-icon"></i>
									<p>Other</p>
								</a>
							  </li>
							</ul>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link">
								<i class="nav-icon fas fa-tree"></i>
								<p>
								Notification
								<i class="right fas fa-angle-left"></i>
							  </p>
							</a>
							<ul class="nav nav-treeview">
							  <li class="nav-item">
								<a href='{{ url("get_likes/notification-daily") }}' class="nav-link">
									<i class="far fa-circle nav-icon"></i>
									<p>Daily Notification</p>
								</a>
							  </li>
							  <li class="nav-item">
								<a href='{{ url("get_likes/notification-premium") }}' class="nav-link ">
									<i class="far fa-circle nav-icon"></i>
									<p>Send Notification</p>
								</a>
							  </li>
							</ul>
						</li>
						<li class="nav-item">
								<a href='{{ url("get_likes/coindetail") }}' class="nav-link"> <i class="nav-icon fas fa-th"></i>
								<p>Coin Detail </p>
							</a>
						</li>
						<li class="nav-item">
								<a href='{{ url("get_likes/ipaddress") }}' class="nav-link"> <i class="nav-icon fas fa-th"></i>
								<p>IP BLock</p>
							</a>
						</li>
						<li class="nav-item">
								<a href='{{ url("get_likes/manage_profile") }}' class="nav-link"> <i class="nav-icon fas fa-th"></i>
								<p>Manage Profile</p>
							</a>
						</li>
						<li class="nav-item">
							<a  class="nav-link"> 
								<p></p>
							</a>
						</li>
						<li class="nav-item">
							<a href='{{ url("get_likes/order") }}' class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Order List</p>
								</a>
						</li>
						<li class="nav-item">
							<a href='{{ url("get_likes/userlist") }}' class="nav-link"> <i class="nav-icon fas fa-th"></i>
								<p>User List</p>
							</a>
						</li>
						<li class="nav-item">
							<a href='{{ url("get_likes/purchaseuser-list") }}' class="nav-link"> <i class="nav-icon fas fa-th"></i>
								<p>Premium User </p>
							</a>
						</li>
					 
						<li class="nav-item">
							<a href='{{ url("get_likes/purchasecoin-list") }}' class="nav-link"> <i class="nav-icon fas fa-th"></i>
								<p>Purchase Coin List</p>
							</a>
						</li>
					 
							<a href="pages/widgets.html" class="nav-link"> <i class="nav-icon "></i>
								<p></p>
							</a>
						</li>
					
						<li class="nav-item">
							<a href="pages/widgets.html" class="nav-link"> <i class="nav-icon "></i>
								<p></p>
							</a>
						</li>
						<li class="nav-item">
							<a href="pages/widgets.html" class="nav-link"> <i class="nav-icon "></i>
								<p></p>
							</a>
						</li>
						<li class="nav-item">
							<a href="pages/widgets.html" class="nav-link"> <i class="nav-icon "></i>
								<p></p>
							</a>
						</li>
						<li class="nav-item">
							<a href="pages/widgets.html" class="nav-link"> <i class="nav-icon "></i>
								<p></p>
							</a>
						</li>
						
					</ul>
				</nav>
				<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside> @yield('content');
		<!-- Main Footer -->
	</div>
	<!-- ./wrapper -->
	<!-- REQUIRED SCRIPTS -->
	<!-- Bootstrap -->
	<script src='{{ asset("assets/get_likes/plugins/bootstrap/js/bootstrap.bundle.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/bootstrap/js/bootstrap.min.js") }}'></script>
	<!-- date-range-picker -->
	<script src='{{ asset("assets/get_likes/plugins/daterangepicker/daterangepicker.js") }}'></script>
	<!-- Bootstrap Switch -->
	<script src='{{ asset("assets/get_likes/plugins/bootstrap-switch/js/bootstrap-switch.min.js") }}'></script>
	<!-- overlayScrollbars -->
	<script src='{{ asset("assets/get_likes/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js") }}'></script>
	<!-- AdminLTE App -->
	<script src='{{ asset("assets/get_likes/js/adminlte.js") }}'></script>
	<!-- jquery-validation -->
	<script src='{{ asset("assets/get_likes/plugins/jquery-validation/jquery.validate.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/jquery-validation/additional-methods.min.js") }}'></script>
	<!-- jQuery Mapael -->
	<script src='{{ asset("assets/get_likes/plugins/jquery-mousewheel/jquery.mousewheel.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/raphael/raphael.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/jquery-mapael/jquery.mapael.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/jquery-mapael/maps/usa_states.min.js") }}'></script>
	<!-- ChartJS -->
	<script src='{{ asset("assets/get_likes/plugins/chart.js/Chart.min.js") }}'></script>
	<!-- InputMask -->
	<script src='{{ asset("assets/get_likes/plugins/moment/moment.min.js") }}'></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src='{{ asset("assets/get_likes/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js") }}'></script>
	<!-- DataTables  & Plugins -->
	<script src='{{ asset("assets/get_likes/plugins/datatables/jquery.dataTables.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/datatables-responsive/js/dataTables.responsive.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/datatables-responsive/js/responsive.bootstrap4.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/datatables-buttons/js/dataTables.buttons.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/datatables-buttons/js/buttons.bootstrap4.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/jszip/jszip.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/pdfmake/pdfmake.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/pdfmake/vfs_fonts.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/datatables-buttons/js/buttons.html5.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/datatables-buttons/js/buttons.print.min.js") }}'></script>
	<script src='{{ asset("assets/get_likes/plugins/datatables-buttons/js/buttons.colVis.min.js") }}'></script>
	<!-- AdminLTE for demo purposes -->
	<script src='{{ asset("assets/get_likes/js/demo.js") }}'></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	{{-- <script src='{{ asset("assets/get_likes/js/pages/dashboard2.js") }}'></script> --}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
	<script src="http://cdn.craig.is/js/rainbow-custom.min.js"></script>
	@yield('script') 
</body>
</html>