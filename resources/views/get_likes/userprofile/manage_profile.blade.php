
@extends('get_likes.layouts.adminmaster')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
            <li class="breadcrumb-item active">Manage Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Manage Profile</small></h3>
              </div>
              @if(session()->has('updateprofile'))
                <div class="alert alert-success">
                    {{ session()->get('updateprofile') }}
                </div>
            @endif
              <!-- /.card-header -->
              <!-- form start --> 
                <form id="quickForm" action="{{ url('get_likes/manageprofile') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control"  value="{{ Auth()->user()->name }}" id="name" placeholder="name">
                    <div class="error" style="color: red;">{{ $errors->first('name') }}</div>

                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" value="{{ Auth()->user()->email }}" class="form-control" id="email" placeholder="Email" disabled>
                    <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                  </div>
                  <div class="form-group">
                    <label for="profile_picture">Profile picture</label><br>
                    <img src='{{ url("assets/img/".Auth()->user()->profile_picture) }}' alt="profile picture" width="270px;" >
                    <input type="file" name="profile_picture" class="form-control" id="profile_picture">
                    <div class="error" style="color: red;">{{ $errors->first('notes') }}</div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
        </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">
          
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Change password</small></h3>
              </div>
              @if(session()->has('message'))
                  <div class="alert alert-success">
                      {{ session()->get('message') }}
                  </div>
              @endif
              <!-- /.card-header -->
              <!-- form start --> 
                <form id="quickForm" action='{{ url("get_likes/update_password") }}' method="post">
                @csrf
              
                <div class="card-body">
                  <div class="form-group">
                    <label for="current_password">Current Password</label>
                    <input type="password" name="current_password" class="form-control" id="current_password" placeholder="">
                    <div class="error" style="color: red;"><b>{{ $errors->first('current_password') }}</b></div>
                  </div>
                  <div class="form-group">
                    <label for="new_password">New Password</label>
                    <input type="password" name="new_password" class="form-control" id="new_password" placeholder="">
                    <div class="error" style="color: rgb(255, 0, 0);"><b>{{ $errors->first('new_password') }}</b></div>
                  </div>
                  <div class="form-group">
                    <label for="new_confirm_password">New Confirm Password</label>
                    <input type="password" name="new_confirm_password" class="form-control" id="new_confirm_password" placeholder="">
                    <div class="error" style="color: red;"><b>{{ $errors->first('new_confirm_password') }}</    b></div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
<script>
</script>
@endsection