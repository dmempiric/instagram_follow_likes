@extends('layouts.adminmaster')
@section('content')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>App Data</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
               <li class="breadcrumb-item "><a href='{{ url("appdata") }}'>App Data</a></li>
               <li class="breadcrumb-item active">create</li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <!-- left column -->
         <div class="col-md-2">
         </div>
         <div class="col-md-6">
            <!-- jquery validation -->
            <div class="card card-primary">
               <div class="card-header">
                  <h3 class="card-title">App Data</small></h3>
               </div>
               <!-- /.card-header -->
               <!-- form start --> 
               <form id="quickForm" action="{{ url("appdata") }}"  method="POST">
               @csrf
               <div class="card-body">
                  <div class="form-group">
                     <label for="earn_like_coin"> Earn Like Coin</label>
                     <input type="text" name="earn_like_coin" class="form-control" id="earn_like_coin" value='{{ old("earn_like_coin") }}' placeholder="earn Like Coin" required>
                     <div class="error" style="color: red;">{{ $errors->first('earn_like_coin') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="rate">spend Like Coin</label>
                     <input type="text" name="spend_like_coin" class="form-control" id="spend_like_coin" placeholder="spend Like Coin" required>
                     <div class="error" style="color: red;">{{ $errors->first('rate') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="earn_follower_coin">spend Follow Coin</label>
                     <input type="text" name="earn_follower_coin" class="form-control" id="earn_follower_coin" placeholder="earn Follower Coin" required>
                     <div class="error" style="color: red;">{{ $errors->first('rate') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="spend_follow_coin">earn Follow Coin</label>
                     <input type="text" name="spend_follow_coin" class="form-control" id="spend_follow_coin" placeholder="earn Follow Coin" required>
                     <div class="error" style="color: red;">{{ $errors->first('spend_follow_coin') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="referral_coin">Refferral Coin</label>
                     <input type="text" name="referral_coin" class="form-control" id="referral_coin" placeholder="Refferal Coin" required>
                     <div class="error" style="color: red;">{{ $errors->first('referral_coin') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="default_coin">Default Coin</label>
                     <input type="text" name="default_coin" class="form-control" id="default_coin" placeholder="Default Coin" required> 
                     <div class="error" style="color: red;">{{ $errors->first('default_coin') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="maintenance_mode">Maintenece Mode</label><br>
                     <input type="checkbox" name="maintenence_mode[]" value="like"  id="like"><span for="like"> Like </span>
                     <input type="checkbox" name="maintenence_mode[]" value="follow" id="maintenance_mode"> Follow
                  </div>
                  <div class="form-group">
                     <label for="update_mode"> Update Mode</label>
                     <select class="form-control" name="update_mode" required>
                        <option value="none" selected>None</option>
                        <option value="flexible">Flexible</option>
                        <option value="immiediate">Immiediate</option>
                     </select>
                  </div>
                  <div class="form-group">
                     <label for="update_url">Update Url</label><br>
                     <input type="text" name="update_url" class="form-control"   id="update_url" placeholder="Update Url" >
                     <div class="error" style="color: red;">{{ $errors->first('update_url') }}</div>
                  </div>
                  <div class="form-group">
                     <label>Payment Methode</label><br>
                     <input type="checkbox" name="payment_methode[]" value="inapp"  id="like"><span for="like"> InApp </span>
                     <input type="checkbox" name="payment_methode[]" value="paypal" > Paypal
                  </div>
                  <div class="form-group">
                     <label for="game_id">Game Id</label>
                     <input type="text" name="game_id" class="form-control" id="game_id" placeholder="Game Coin" required>
                     <div class="error" style="color: red;">{{ $errors->first('game_id') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="banner_id">Banner Id</label>
                     <input type="text" name="banner_id" class="form-control" id="banner_id" placeholder="Banner Id" required>
                     <div class="error" style="color: red;">{{ $errors->first('banner_id') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="initial_id">Initial Id</label>
                     <input type="text" name="initial_id" class="form-control" id="initial_id" placeholder="Initial Id" required>
                     <div class="error" style="color: red;">{{ $errors->first('initial_id') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="maintenence_message">Maintenence Message</label>
                     <input type="text" name="maintenence_message" class="form-control" id="maintenence_message" placeholder="Maintenence Message" required>
                     <div class="error" style="color: red;">{{ $errors->first('maintenence_message') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="notification_title">Notification Title</label>
                     <input type="text" name="notification_title" class="form-control" id="notification_title" placeholder="Notification Title" required>
                     <div class="error" style="color: red;">{{ $errors->first('notification_title') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="notification_message">Notification Message</label>
                     <input type="text" name="notification_message" class="form-control" id="notification_message" placeholder="Notification Message" required>
                     <div class="error" style="color: red;">{{ $errors->first('notification_message') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="notification_show">Notification Show</label>
                     <select class="form-control" name="notification_show" required>
                        <option  disabled>Notification Show</option>
                        <option value="1" selected>Yes</option>
                        <option value="0">No</option>
                     </select>
                  </div>
                  <div class="form-group">
                     <label for="update_message">Update Message</label>
                     <input type="text" name="update_message" class="form-control" id="update_message" placeholder="Update Message" required>
                     <div class="error" style="color: red;">{{ $errors->first('update_message') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="playstore_version">Play Store Message</label>
                     <input type="text" name="playstore_version" class="form-control" id="playstore_version" placeholder="Playstore Version" required>
                     <div class="error" style="color: red;">{{ $errors->first('playstore_version') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="web">Web View</label>
                     <select class="form-control" name="web" required>
                        <option  disabled>Login Type</option>
                        <option value="1" selected>Insta Login</option>
                        <option value="0">Form login</option>
                     </select>
                  </div>
                  <div class="form-group">
                     <label for="website">WebSite</label>
                     <input type="text" name="website"  class="form-control" id="website" placeholder="Web Site" required>
                     <div class="error" style="color: red;">{{ $errors->first('website') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="user_agent">User Agent</label>
                  <textarea type="text" style="height: 100px;"  name="user_agent" class="form-control" id="user_agent"  required> {{ old("user_agent") }}</textarea>
                     <div class="error" style="color: red;">{{ $errors->first('user_agent') }}</div>
                  </div>
                  <div class="form-group">
                     <label for="share_url">Share Url</label>
                     <input type="text" name="share_url" class="form-control" id="share_url" placeholder="Share Url" required>
                     <div class="error" style="color: red;">{{ $errors->first('share_url') }}</div>
                  </div>
                  <div class="form-group">
                      <label for="offer">Offer</label>
                      <select class="form-control" name="offer" required>
                        <option  disabled>Offer Status</option>
                        <option value="1" selected>Start</option>
                        <option value="0">End</option>
                      </select>
                     <div class="error" style="color: red;">{{ $errors->first('offer') }}</div>
                  </div>
                  <div class="form-group">
                    <label for="offer_percentage">Offer in Percentage</label>
                    <input type="text" name="offer_percentage" class="form-control" id="offer_percentage" placeholder="Offer in Percentage" required>
                    <div class="error" style="color: red;">{{ $errors->first('offer_percentage') }}</div>
                 </div>
                 <div class="form-group">
                  <label for="offer_percentage">Offer in Percentage</label>
                  <input type="text" name="offer_percentage"class="form-control" id="offer_percentage" placeholder="Offer in Percentage" disabled>
                  <div class="error" style="color: red;">{{ $errors->first('offer_percentage') }}</div>
                </div>
                <div class="form-group">
                  <label for="offer_starttime">Offer Starttime</label>
                  <div class='input-group date' id='datetimepicker1'>
                    <input id="datetimepicker" name="offer_starttime"    class="form-control" value="2020-10-12 10:20"  type="text" disabled>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                  <div class="error" style="color: red;">{{ $errors->first('offer_starttime') }}</div>
               </div>
                 <div class="form-group">
                   <label for="offer_endtime">Offer Endtime</label>
                   <div class='input-group date' id='offerstarttimepicker'>
                     <input id="offerstarttimepicker"   class="form-control" value="2020-10-10 10:20"  type="text" disabled>
                     <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                     </span>
                   </div>
                   <div class="error" style="color: red;">{{ $errors->first('offer_endtime') }}</div>
                </div>
                <div class="form-group">
                  <label for="offer_discount_text">Offer Discount text</label>
                  <input type="text" name="offer_discount_text"  class="form-control" id="offer_discount_text" placeholder="Offer Discount Text" disabled>
                  <div class="error" style="color: red;">{{ $errors->first('offer_discount_text') }}</div>
               </div>
              <div class="form-group">
                <label for="offer_discount_image">Offer Discount Image</label><br>
                
                 
                <div class="error" style="color: red;">{{ $errors->first('offer_discount_image') }}</div>
             </div>
             <div class="form-group">
              <label for="web_login">Web Login</label>
              <select class="form-control" name="web_login" disabled>
                <option  disabled>Web Login </option>
                <option value="1"  >Web Login</option>
                <option value="0" >App Login</option>
              </select>
             <div class="error" style="color: red;">{{ $errors->first('web_login') }}</div>
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="text" name="email"    class="form-control" id="email" placeholder="Email" disabled>
            <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
         </div>
          <div class="form-group">
            <label for="privacy_policy">Privacy Policy</label>
            <input type="text" name="privacy_policy"   class="form-control" id="privacy_policy" placeholder="Privacy Policy" disabled>
            <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
         </div>
          <div class="form-group">
            <label for="facebook">Facebook</label>
            <input type="text" name="facebook"  class="form-control" id="facebook" placeholder="Facebook" disabled>
            <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
         </div>
          <div class="form-group">
            <label for="instagram">Instagram</label>
            <input type="text" name="instagram" class="form-control" id="instagram" placeholder="Instagram" disabled>
            <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
         </div>
         <div class="form-group">
          <label for="rate_dialog">Rate Dialog</label>
          <select class="form-control" name="rate_dialog" disabled>
            <option  disabled>Rate Dialog</option>
            <option value="1" >Show</option>
            <option value="0" >Hide</option>
          </select>
         <div class="error" style="color: red;">{{ $errors->first('rate_dialog') }}</div>
      </div>
         <div class="form-group">
          <label for="share_dialog">Share Dialog</label>
          <select class="form-control" name="share_dialog" disabled>
            <option  disabled>Share Dialog</option>
            <option value="1" >Show</option>
            <option value="0" >Hide</option>
          </select>
         <div class="error" style="color: red;">{{ $errors->first('rate_dialog') }}</div>
      </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                     <a href='{{ url("appdata") }}'  class="btn btn-primary">Back</a>
                  </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $.datetimepicker.setLocale('pt-BR');
     $('#datetimepicker').datetimepicker();
  });
</script>
@endsection
