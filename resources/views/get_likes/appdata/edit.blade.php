@extends('layouts.adminmaster')
@section('content')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>App Data</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
               <li class="breadcrumb-item "><a href='{{ url("appdata") }}'>App Data</a></li>
               <li class="breadcrumb-item active">create</li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <!-- left column -->
         <div class="col-md-2">
         </div>
         <div class="col-md-6">
            <!-- jquery validation -->
            <div class="card card-primary">
               <div class="card-header">
                  <h3 class="card-title">Edit App Data</small></h3>
               </div>
               <!-- /.card-header -->
               <!-- form start --> 
               <form id="quickForm" action="{{ url("appdata/$appdata->
                  id") }}"  method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="card-body">
                     <div class="form-group">
                        <label for="earn_like_coin"> Earn Like Coin</label>
                        <input type="text" name="earn_like_coin" class="form-control" id="earn_like_coin" value="{{ $appdata->earn_like_coin }}" placeholder="earn Like Coin" required>
                        <div class="error" style="color: red;">{{ $errors->first('earn_like_coin') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="rate">spend Like Coin</label>
                        <input type="text" name="spend_like_coin" class="form-control" id="spend_like_coin" value="{{ $appdata->spend_like_coin }}"  placeholder="spend Like Coin" required>
                        <div class="error" style="color: red;">{{ $errors->first('rate') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="earn_follower_coin">spend Follow Coin</label>
                        <input type="text" name="earn_follower_coin" class="form-control" id="earn_follower_coin" value="{{ $appdata->earn_follow_coin }}" placeholder="earn Follower Coin" required>
                        <div class="error" style="color: red;">{{ $errors->first('rate') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="spend_follow_coin">earn Follow Coin</label>
                        <input type="text" name="spend_follow_coin" class="form-control" id="spend_follow_coin" value="{{ $appdata->spend_follow_coin }}" placeholder="earn Follow Coin" required>
                        <div class="error" style="color: red;">{{ $errors->first('spend_follow_coin') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="referral_coin">Refferral Coin</label>
                        <input type="text" name="referral_coin" class="form-control" id="referral_coin" placeholder="Refferal Coin" value="{{ $appdata->referral_coin }}" required>
                        <div class="error" style="color: red;">{{ $errors->first('referral_coin') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="default_coin">Default Coin</label>
                        <input type="text" name="default_coin" class="form-control" id="default_coin" placeholder="Default Coin" value="{{ $appdata->default_coin }}" required> 
                        <div class="error" style="color: red;">{{ $errors->first('default_coin') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="maintenance_mode">Maintenece Mode</label><br>
                        <input type="checkbox" name="maintenence_mode[]" value="like"  id="like"     @if(in_array("like",json_decode($appdata->maintenence_mode)))  checked @endif
                        ><span for="like"> Like </span>
                        <input type="checkbox" name="maintenence_mode[]" @if(in_array("follow",json_decode($appdata->maintenence_mode)))  checked @endif value="follow" id="maintenance_mode"> Follow
                     </div>
                     <div class="form-group">
                        <label for="update_mode"> Update Mode</label>
                        <select class="form-control" name="update_mode" required>
                        <option value="none" @if($appdata->update_mode =='none') selected @endif>None</option>
                        <option value="flexible" @if($appdata->update_mode =='flexible') selected @endif>Flexible</option>
                        <option value="immiediate" @if($appdata->update_mode =='immiediate') selected @endif>Immiediate</option>
                        </select>    
                     </div>
                     <div class="form-group">
                        <label for="update_url">Update Url</label><br>
                        <input type="text" name="update_url" class="form-control"   id="update_url" placeholder="Update Url"  value="{{ $appdata->update_url }}" >
                        <div class="error" style="color: red;">{{ $errors->first('update_url') }}</div>
                     </div>
                     <div class="form-group">
                        <label>Payment Methode</label><br>
                        <input type="checkbox" name="payment_methode[]" value="inapp"  @if(in_array("inapp",json_decode($appdata->payment_methode))) checked  @endif> InApp </span>
                        <input type="checkbox" name="payment_methode[]" value="paypal" @if(in_array("paypal",json_decode($appdata->payment_methode))) checked  @endif > Paypal
                     </div>
                     <div class="form-group">
                        <label for="game_id">Game Id</label>
                        <input type="text" name="game_id" class="form-control" id="game_id" placeholder="Game Coin" value="{{ $appdata->game_id }}" required>
                        <div class="error" style="color: red;">{{ $errors->first('game_id') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="banner_id">Game Id</label>
                        <input type="text" name="banner_id" class="form-control" id="banner_id" placeholder="Banner Id" value="{{ $appdata->banner_id }}" required>
                        <div class="error" style="color: red;">{{ $errors->first('banner_id') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="initial_id">Initial Id</label>
                        <input type="text" name="initial_id" class="form-control" id="initial_id" placeholder="Initial Id" value="{{ $appdata->initial_id }}" required{{--  --}}>
                        <div class="error" style="color: red;">{{ $errors->first('initial_id') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="maintenence_message">Maintenence Message</label>
                        <input type="text" name="maintenence_message" class="form-control" id="maintenence_message" value="{{ $appdata->maintenence_message }}" placeholder="Maintenence Message" required>
                        <div class="error" style="color: red;">{{ $errors->first('maintenence_message') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="notification_title">Notification Title</label>
                        <input type="text" name="notification_title" class="form-control" id="notification_title"  value="{{ $appdata->notification_title }}" placeholder="Notification Title" required>
                        <div class="error" style="color: red;">{{ $errors->first('notification_title') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="notification_message">Notification Message</label>
                        <input type="text" name="notification_message" class="form-control" id="notification_message" placeholder="Notification Message" value="{{ $appdata->notification_message }}"  required>
                        <div class="error"  style="color: red;">{{ $errors->first('notification_message') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="notification_sho w">Notification Show</label>
                        <select class="form-control" name="notification_show" required>
                           <option  disabled>Notification Show</option>
                           <option value="1" @if($appdata->web == "1") selected @endif >Yes</option>
                           <option value="0" @if($appdata->web == "0") selected @endif>No</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for="update_message">Update Message</label>
                        <input type="text" name="update_message"  value="{{ $appdata->update_message }}" class="form-control" id="update_message" placeholder="Update Message" required>
                        <div class="error" style="color: red;">{{ $errors->first('update_message') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="playstore_version">Play Store Message</label>
                        <input type="text" name="playstore_version"  value="{{ $appdata->playstore_version }}" class="form-control" id="playstore_version" placeholder="Playstore Version" required>
                        <div class="error" style="color: red;">{{ $errors->first('playstore_version') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="web">Web View</label>
                        <select class="form-control" name="web" required>
                           <option  disabled>Login Type</option>
                           <option value="1" @if($appdata->web == "1") selected @endif >Insta Login</option>
                           <option value="0" @if($appdata->web == "0") selected @endif>Form login</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for="website">WebSite</label>
                        <input type="text" name="website"  value="{{ $appdata->website }}" class="form-control" id="website" placeholder="Web Site" required>
                        <div class="error" style="color: red;">{{ $errors->first('website') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="user_agent">User Agent</label>
                        <textarea type="text" style="height: 100px;"  name="user_agent" class="form-control" id="user_agent"  required> {{ $appdata->user_agent }}</textarea>
                        <div class="error" style="color: red;">{{ $errors->first('user_agent') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="share_url">Share Url</label>
                        <input type="text" name="share_url" value="{{ $appdata->share_url }}" class="form-control" id="share_url" placeholder="Share Url" required>
                        <div class="error" style="color: red;">{{ $errors->first('share_url') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer">Offer</label>
                        <select class="form-control" name="offer" required>
                           <option  disabled>Offer Status</option>
                           <option value="1"  @if($appdata->offer == "1") selected @endif>Start</option>
                           <option value="0" @if($appdata->offer == "0") selected @endif>End</option>
                        </select>
                        <div class="error" style="color: red;">{{ $errors->first('offer') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_percentage">Offer in Percentage</label>
                        <input type="text" name="offer_percentage" value="{{ $appdata->offer_percentage }}" class="form-control" id="offer_percentage" placeholder="Offer in Percentage" required>
                        <div class="error" style="color: red;">{{ $errors->first('offer_percentage') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_starttime">Offer Starttime</label>
                        <div class='input-group date' id='datetimepicker1'>
                           <input id="datetimepicker" name="offer_starttime" value="2020-10-05 10:20"   class="form-control" value="2020-10-12 10:20"  type="text">
                           <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                        <div class="error" style="color: red;">{{ $errors->first('offer_starttime') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_endtime">Offer Endtime</label>
                        <div class='input-group date' id='offerstarttimepicker'>
                           <input id="offerstarttimepicker" value="{{ $appdata->offer_endtime }}"   class="form-control" value="2020-10-10 10:20"  type="text">
                           <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                        <div class="error" style="color: red;">{{ $errors->first('offer_endtime') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_discount_text">Offer Discount text</label>
                        <input type="text" name="offer_discount_text" value="{{ $appdata->offer_discount_text }}"   class="form-control" id="offer_discount_text" placeholder="Offer Discount Text" required>
                        <div class="error" style="color: red;">{{ $errors->first('offer_discount_text') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_discount_image">Offer Discount Image</label><br>
                        <img src='{{ url("/public/instagram/app_data/$appdata->offer_discount_image") }}' alt="Preview" width="170px;">
                        <input type="file" name="offer_discount_image" class="form-control" id="offer_discount_image" >
                        <div class="error" style="color: red;">{{ $errors->first('offer_discount_image') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="web">Web Login</label>
                        <select class="form-control" name="web" required>
                           <option  disabled>Web Login </option>
                           <option value="1"  @if($appdata->web_login == "1") selected @endif>Web Login</option>
                           <option value="0" @if($appdata->web_login == "0") selected @endif>App Login</option>
                        </select>
                        <div class="error" style="color: red;">{{ $errors->first('web_login') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" value="{{ $appdata->email }}"   class="form-control" id="email" placeholder="Email" required>
                        <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="privacy_policy">Privacy Policy</label>
                        <input type="text" name="privacy_policy" value="{{ $appdata->privacy_policy }}"   class="form-control" id="privacy_policy" placeholder="Privacy Policy" required>
                        <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="facebook">Facebook</label>
                        <input type="text" name="facebook" value="{{ $appdata->facebook }}"   class="form-control" id="facebook" placeholder="Facebook" required>
                        <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="instagram">Instagram</label>
                        <input type="text" name="instagram" value="{{ $appdata->instagram }}"   class="form-control" id="instagram" placeholder="Instagram" required>
                        <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="rate_dialog">Rate Dialog</label>
                        <select class="form-control" name="rate_dialog" required>
                           <option  disabled>Rate Dialog</option>
                           <option value="1"  @if($appdata->rate_dialog == "1") selected @endif>Show</option>
                           <option value="0" @if($appdata->rate_dialog == "0") selected @endif>Hide</option>
                        </select>
                        <div class="error" style="color: red;">{{ $errors->first('rate_dialog') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="share_dialog">Share Dialog</label>
                        <select class="form-control" name="share_dialog" required>
                           <option  disabled>Share Dialog</option>
                           <option value="1"  @if($appdata->share_dialog == "1") selected @endif>Show</option>
                           <option value="0" @if($appdata->share_dialog == "0") selected @endif>Hide</option>
                        </select>
                        <div class="error" style="color: red;">{{ $errors->first('rate_dialog') }}</div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                        <button type="submit" name="submit" value="submit"  class="btn btn-primary">Submit</button>
                        <a href='{{ url("appdata") }}'  class="btn btn-primary">Back</a>
                     </div>
               </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection
@section('script')
<script>
   $(document).ready(function() {
       $.datetimepicker.setLocale('pt-BR');
      $('#datetimepicker').datetimepicker();
   });
   $(document).ready(function() {
       $.offerstarttimepicker.setLocale('pt-BR');
      $('#offerstarttimepicker').datetimepicker();
   });
</script>
@endsection