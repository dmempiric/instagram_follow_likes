@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Ads Detail</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("get_likes/appdata-game") }}'>Ads Detail</a></li>
                  <li class="breadcrumb-item active">Index</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Ads Detail List</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <div class="card-body">
                     <div class="form-group">
                        <label for="game_id">Game Id</label>
                        <input type="text" name="game_id" class="form-control" id="game_id" placeholder="Game Coin" value="{{ $appdata->game_id }}" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('game_id') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="banner_id">Banner Id</label>
                        <input type="text" name="banner_id" class="form-control" id="banner_id" placeholder="Banner Id" value="{{ $appdata->banner_id }}" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('banner_id') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="initial_id">Initial Id</label>
                        <input type="text" name="initial_id" class="form-control" id="initial_id" placeholder="Initial Id" value="{{ $appdata->initial_id }}" disabled >
                        <div class="error" style="color: red;">{{ $errors->first('initial_id') }}</div>
                     </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <a href='{{ url("get_likes/appdata-game/$appdata->id") }}' class="btn btn-primary">Edit</a>
                  </div>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script></script>
@endsection
