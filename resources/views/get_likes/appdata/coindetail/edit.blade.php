@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>App Data Coin Detail</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                    <li class="breadcrumb-item "><a href='{{ url("get_likes/appdata-coindetail") }}'>App Data Coin Detail</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>
            </div>
        </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Edit App Data  CoinDetail</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                <form id="quickForm" action="{{ url("get_likes/appdata-coindetailupdate/$appdata->id") }}"  method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                      <div class="form-group">
                        <label for="earn_like_coin"> Earn Like Coin</label>
                      <input type="text" name="earn_like_coin" class="form-control" id="earn_like_coin" value="{{ $appdata->earn_like_coin }}" placeholder="earn Like Coin" required>
                        <div class="error" style="color: red;">{{ $errors->first('earn_like_coin') }}</div>
                      </div>
                      <div class="form-group">
                        <label for="rate">spend Like Coin</label>
                        <input type="text" name="spend_like_coin" class="form-control" id="spend_like_coin" value="{{ $appdata->spend_like_coin }}"  placeholder="spend Like Coin" required>
                        <div class="error" style="color: red;">{{ $errors->first('rate') }}</div>
                      </div>
                      
                      <div class="form-group">
                        <label for="referral_coin">Refferral Coin</label>
                        <input type="text" name="referral_coin" class="form-control" id="referral_coin" placeholder="Refferal Coin" value="{{ $appdata->referral_coin }}" required>
                        <div class="error" style="color: red;">{{ $errors->first('referral_coin') }}</div>
                      </div>
                      <div class="form-group">
                        <label for="default_coin">Default Coin</label>
                        <input type="text" name="default_coin" class="form-control" id="default_coin" placeholder="Default Coin" value="{{ $appdata->default_coin }}" required> 
                        <div class="error" style="color: red;">{{ $errors->first('default_coin') }}</div>
                      </div>
                      <div class="form-group">
                        <label for="from_add_coin">From Add Coin</label>
                        <input type="text" name="from_add_coin" class="form-control" id="from_add_coin" placeholder="Add Coin" value="{{ $appdata->from_add_coin }}" required> 
                        <div class="error" style="color: red;">{{ $errors->first('from_add_coin') }}</div>
                     </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <button type="submit" name="submit" value="submit"  class="btn btn-primary">Submit</button>
                      <a href='{{ url("get_likes/appdata-coindetail") }}' class="btn btn-primary">Back</a>
                    </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $.datetimepicker.setLocale('pt-BR');
     $('#datetimepicker').datetimepicker();
  });
  $(document).ready(function() {
      $.offerstarttimepicker.setLocale('pt-BR');
     $('#offerstarttimepicker').datetimepicker();
  });
</script>
@endsection
