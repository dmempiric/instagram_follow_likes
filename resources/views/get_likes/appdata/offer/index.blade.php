@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>App Data Offer</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("get_likes/appdata-offer") }}'>App Data Offer</a></li>
                  <li class="breadcrumb-item active">Index</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">App Data Offer List</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <div class="card-body">
                     <div class="form-group">
                        <label for="offer">Offer</label><br>
                        @if($appdata->offer == "1")
                        <button class="btn btn-primary">Start</button>
                        @endif
                        @if($appdata->offer == "0")
                        <button class="btn btn-primary">End</button>
                        @endif 
                        <div class="error" style="color: red;">{{ $errors->first('offer') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_percentage">Offer in Percentage</label>
                        <input type="text" name="offer_percentage" value="{{ $appdata->offer_percentage }}" class="form-control" id="offer_percentage" placeholder="Offer in Percentage" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('offer_percentage') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_starttime">Offer Starttime</label>
                        <div class='input-group date' id='datetimepicker1'>
                           <input id="datetimepicker" name="offer_starttime"  value="{{ $appdata->offer_starttime }}"  class="form-control" value="2020-10-12 10:20"  type="text" disabled>
                           <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                        <div class="error" style="color: red;">{{ $errors->first('offer_starttime') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_endtime">Offer Endtime</label>
                        <div class='input-group date' id='offerstarttimepicker'>
                           <input id="offerstarttimepicker" value="{{ $appdata->offer_endtime }}"   class="form-control" value="2020-10-10 10:20"  type="text" disabled>
                           <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                        <div class="error" style="color: red;">{{ $errors->first('offer_endtime') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_discount_text">Offer Discount Title*</label>
                        <input type="text" name="offer_discount_title" value="{{ $appdata->offer_discount_title }}"   class="form-control" id="offer_discount_title" placeholder="Offer Discount Title" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('offer_discount_title') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_discount_text">Offer Discount Description*</label>
                        <textarea type="text" name="offer_discount_text" class="form-control" id="offer_discount_text" placeholder="Offer Discount Description" disabled>{{ $appdata->offer_discount_text }}</textarea>
                        <div class="error" style="color: red;">{{ $errors->first('offer_discount_text') }}</div>
                     </div>
                     @if($appdata->offer_discount_image)
                     <div class="form-group">
                        <label for="offer_discount_image">Offer Discount Image</label><br>
                           <img src='{{ url("/public/get_likes/instagram/app_data/$appdata->offer_discount_image") }}' alt="Preview" width="170px;">
                        </div>
                     </div>
                     @endif
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <a href='{{ url("get_likes/appdata-offer/$appdata->id") }}' class="btn btn-primary">Edit</a>
                  </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <div class="col-md-6">
            </div>
         </div>
      </div>
   </section>
</div>
@endsection
@section('script')
<script></script>
@endsection
