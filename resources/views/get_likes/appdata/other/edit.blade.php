@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>App Data other</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("get_likes/appdata-other") }}'>App Data other</a></li>
                  <li class="breadcrumb-item active">Edit</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Edit App Data other</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                <form id="quickForm" action="{{ url("get_likes/appdata-otherupdate/$appdata->id") }}"  method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                           <label for="playstore_version">Play Store Message</label>
                           <input type="text" name="playstore_version"  value="{{ $appdata->playstore_version }}" class="form-control" id="playstore_version" placeholder="Playstore Version" required>
                           <div class="error" style="color: red;">{{ $errors->first('playstore_version') }}</div>
                        </div>   
                        <div class="form-group">
                           <label for="web_login">Web Login</label>
                           <select class="form-control" name="web_login" required>
                              <option  disabled>Web Login </option>
                              <option value="1"  @if($appdata->web_login == "1") selected @endif>Web Login</option>
                              <option value="0" @if($appdata->web_login == "0") selected @endif>App Login</option>
                           </select>
                           <div class="error" style="color: red;">{{ $errors->first('web_login') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="web">Web View</label>
                           <select class="form-control" name="web" required>
                              <option  disabled>Web Login </option>
                              <option value="1" @if($appdata->web == "1") selected @endif>Web Login</option>
                              <option value="0" @if($appdata->web == "0") selected @endif>App Login</option>
                           </select>
                           <div class="error" style="color: red;">{{ $errors->first('web_login') }}</div>
                        </div>
                         <div class="form-group">
                           <label for="user_agent">User Agent</label>
                           <textarea type="text" style="height: 100px;"  name="user_agent" class="form-control" id="user_agent"  required> {{ $appdata->user_agent }}</textarea>
                           <div class="error" style="color: red;">{{ $errors->first('user_agent') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="share_url">Share Url</label>
                           <input type="text" name="share_url" value="{{ $appdata->share_url }}" class="form-control" id="share_url" placeholder="Share Url" required>
                           <div class="error" style="color: red;">{{ $errors->first('share_url') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="email">Email</label>
                           <input type="text" name="email" value="{{ $appdata->email }}"   class="form-control" id="email" placeholder="Email" required>
                           <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="website">WebSite</label>
                           <input type="text" name="website"  value="{{ $appdata->website }}" class="form-control" id="website" placeholder="Web Site" required>
                           <div class="error" style="color: red;">{{ $errors->first('website') }}</div>
                        </div> 
                        
                        <div class="form-group">
                           <label for="privacy_policy">Privacy Policy</label>
                           <input type="text" name="privacy_policy" value="{{ $appdata->privacy_policy }}"   class="form-control" id="privacy_policy" placeholder="Privacy Policy" required>
                           <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="facebook">Facebook</label>
                           <input type="text" name="facebook" value="{{ $appdata->facebook }}"   class="form-control" id="facebook" placeholder="Facebook" required>
                           <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="instagram">Instagram</label>
                           <input type="text" name="instagram" value="{{ $appdata->instagram }}"   class="form-control" id="instagram" placeholder="Instagram" required>
                           <div class="error" style="color: red;">{{ $errors->first('email') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="twitter">Twitter</label>
                           <input type="text" name="twitter" value="{{ $appdata->twitter }}"   class="form-control" id="twitter" placeholder="Twitter" required>
                           <div class="error" style="color: red;">{{ $errors->first('twitter') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="telegram">Telegram</label>
                           <input type="text" name="telegram" value="{{ $appdata->telegram }}"   class="form-control" id="telegram" placeholder="Telegram" required>
                           <div class="error" style="color: red;">{{ $errors->first('telegram') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="rate_dialog">Rate Dialog</label>
                           <select class="form-control" name="rate_dialog" required>
                              <option  disabled>Rate Dialog</option>
                              <option value="1"  @if($appdata->rate_dialog == "1") selected @endif>Show</option>
                              <option value="0" @if($appdata->rate_dialog == "0") selected @endif>Hide</option>
                           </select>
                           <div class="error" style="color: red;">{{ $errors->first('rate_dialog') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="share_dialog">Share Dialog</label>
                           <select class="form-control" name="share_dialog" required>
                              <option  disabled>Share Dialog</option>
                              <option value="1"  @if($appdata->share_dialog == "1") selected @endif>Show</option>
                              <option value="0" @if($appdata->share_dialog == "0") selected @endif>Hide</option>
                           </select>
                           <div class="error" style="color: red;">{{ $errors->first('rate_dialog') }}</div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" name="submit" value="submit"  class="btn btn-primary">Submit</button>
                        <a href='{{ url("get_likes/appdata-other") }}' class="btn btn-primary">Back</a>
                    </div>
                </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $.datetimepicker.setLocale('pt-BR');
     $('#datetimepicker').datetimepicker();
  });
  $(document).ready(function() {
      $.other_endtime.setLocale('pt-BR');
     $('#other_endtime').other_endtime();
  });
</script>
@endsection
