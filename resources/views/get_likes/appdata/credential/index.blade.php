@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Firebase And Google Refresh Token Credential</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("get_likes/appdata-credential-token-notification") }}'>Creadential</a></li>
                  <li class="breadcrumb-item active">Index</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Firebase And Google Refresh Token Credential  </small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <form id="quickForm" action="{{ url("appdata-game") }}"  method="POST">
                  @csrf
                  <div class="card-body">
                     <h4><b>Firebase Notification Token</b></h4>
                     <div class="form-group">
                        <label for="google_notification_server_api_key">API Key</label>
                        <textarea name="google_notification_server_api_key" class="form-control" id="google_notification_server_api_key" placeholder="API Key" disabled cols="30" rows="5">{{ $appdata->google_notification_server_api_key }}</textarea>
                        <div class="error" style="color: red;">{{ $errors->first('google_notification_server_api_key') }}</div>
                     </div>
                     <h4><b>Google Token Credential</b></h4>
                     <div class="form-group">
                        <label for="client_id">Client ID</label>
                        <textarea name="client_id" class="form-control" id="client_id" placeholder="Client ID"  disabled cols="30" rows="3">{{ $appdata->client_id }}</textarea>
                        <div class="error" style="color: red;">{{ $errors->first('client_id') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="client_secret">Client Secret</label>
                        <input type="text" name="client_secret" class="form-control" id="client_secret" placeholder="Client Secret" value="{{ $appdata->client_secret }}" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('client_secret') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="refresh_token">Refresh Token</label>
                        <textarea name="refresh_token" class="form-control" id="refresh_token" placeholder="Refresh Token"   disabled cols="30" rows="5">{{ $appdata->refresh_token }}</textarea>
                        <div class="error" style="color: red;">{{ $errors->first('refresh_token') }}</div>
                     </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <a href='{{ url("get_likes/appdata-credential-token-notification/$appdata->id") }}' class="btn btn-primary px-4">Edit</a>
                  </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script></script>
@endsection
