@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>App Data Maintenence</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
               <li class="breadcrumb-item "><a href='{{ url("get_likes/appdata-maintenence") }}'>App Data Maintenence</a></li>
               <li class="breadcrumb-item active">edit</li>
            </ol>
         </div>
      </div>
   </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Edit App Data Maintenence</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                <form id="quickForm" action="{{ url("get_likes/appdata-maintenenceupdate/$appdata->id") }}"  method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                      <div class="form-group">
                        <label for="maintenence_message">Maintenence Message</label>
                        <input type="text" name="maintenence_message" class="form-control" id="maintenence_message" value="{{ $appdata->maintenence_message }}" placeholder="Maintenence Message" required>
                        <div class="error" style="color: red;">{{ $errors->first('maintenence_message') }}</div>
                      </div>
                      <div class="form-group">
                        <label for="maintenance_mode">Maintenece Mode</label><br>
                        <input type="checkbox" name="maintenence_mode[]" value="like"  id="like"     @if(in_array("like",json_decode($appdata->maintenence_mode)))  checked @endif
                        ><span for="like"> Like </span>
                     </div>
                     <div class="form-group">
                        <label for="update_mode"> Update Mode</label>
                        <select class="form-control" name="update_mode" required>
                        <option value="none" @if($appdata->update_mode =='none') selected @endif>None</option>
                        <option value="flexible" @if($appdata->update_mode =='flexible') selected @endif>Flexible</option>
                        <option value="immiediate" @if($appdata->update_mode =='immiediate') selected @endif>Immiediate</option>
                        </select>    
                     </div>
                     <div class="form-group">
                        <label for="update_url">Update Url</label><br>
                        <input type="text" name="update_url" class="form-control"   id="update_url" placeholder="Update Url"  value="{{ $appdata->update_url }}" >
                        <div class="error" style="color: red;">{{ $errors->first('update_url') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="update_message">Update Message</label>
                        <input type="text" name="update_message"  value="{{ $appdata->update_message }}" class="form-control" id="update_message" placeholder="Update Message" required>
                        <div class="error" style="color: red;">{{ $errors->first('update_message') }}</div>
                     </div>
                     <div class="form-group">
                        <label>Payment Methode</label><br>
                        <input type="checkbox" name="payment_methode[]" value="inapp"  @if(in_array("inapp",json_decode($appdata->payment_methode))) checked  @endif> InApp </span>
                        <input type="checkbox" name="payment_methode[]" value="paypal" @if(in_array("paypal",json_decode($appdata->payment_methode))) checked  @endif > Paypal
                     </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" name="submit" value="submit"  class="btn btn-primary">Submit</button>
                        <a href='{{ url("get_likes/appdata-maintenence") }}' class="btn btn-primary">Back</a>
                    </div>
                </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $.datetimepicker.setLocale('pt-BR');
     $('#datetimepicker').datetimepicker();
  });
  $(document).ready(function() {
      $.offerstarttimepicker.setLocale('pt-BR');
     $('#offerstarttimepicker').datetimepicker();
  });
</script>
@endsection
