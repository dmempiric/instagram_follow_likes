@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Coin Detail</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("coindetail") }}'>Coin Detail</a></li>
                  <li class="breadcrumb-item active">Edit</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Edit Coin Detail</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <form id="quickForm" action='{{ url("order/$order->id") }}' method="POST" enctype="multipart/form-data" >
                     @csrf
                     @method('PUT')
                     <div class="card-body">
                        <div class="form-group">
                           <label for="type"> Type</label>
                           <select class="form-control" name="type" required>
                            <option value="0" @if($order->type == "0") selected @endif>Like</option>
                          </select>    
                        </div>
                        <div class="form-group">
                           <label for="user_id">User Id</label>
                           <input type="text" name="user_id" value='{{ $order->user_id }}' class="form-control" id="user_id" placeholder="User Id" disabled >
                           <div class="error" style="color: red;">{{ $errors->first('user_id') }}</div>
                        </div>
                        @if(isset($order->custom_user_id))
                           <div class="form-group">
                              <label for="custom_user_id">Custom User Id</label>
                              <input type="text" name="user_id" value='{{ $order->custom_user_id }}' class="form-control" id="user_id" placeholder="User Id" disabled >
                              <div class="error" style="color: red;">{{ $errors->first('user_id') }}</div>
                           </div>
                        @endif
                        <div class="form-group">
                           <label for="post_id">Post Id</label>
                           <input type="text" name="post_id" value='{{ $order->user_id }}' class="form-control" id="post_id" placeholder="Post Id" disabled >
                           <div class="error" style="color: red;">{{ $errors->first('post_id') }}</div>
                        </div>
                     @if($order->type == "0")
                        <div class="form-group">
                           <label for="short_code">Short Code</label>
                           <input type="text" name="short_code" value='{{ $order->short_code }}' class="form-control" id="short_code" placeholder="Short Code" required >
                           <div class="error" style="color: red;">{{ $errors->first('short_code') }}</div>
                        </div>
                     @endif
                        <div class="form-group">
                           <label for="needed">Needed</label>
                           <input type="text" name="needed" value='{{ $order->needed }}' class="form-control" id="needed" placeholder="Needed" required >
                           <div class="error" style="color: red;">{{ $errors->first('short_code') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="recieved">Recieved</label>
                           <input type="text" name="recieved" value='{{ $order->recieved }}' class="form-control" id="recieved" placeholder="Recieved" required >
                           <div class="error" style="color: red;">{{ $errors->first('recieved') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="image_url">Image</label><br>
                           @if($order->type == 0)
                              <img src='{{ url("/public/instagram/like/$order->image_url") }}' width="70px;">
                           @endif
                           <input type="file" name="image_url" value='{{ $order->recieved }}' class="form-control" id="image_url" placeholder="image_url"  >
                           <div class="error" style="color: red;">{{ $errors->first('recieved') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="order_status">Order Status</label>
                           <select class="form-control" name="order_status" required>
                            <option disabled>Order Status</option>
                            <option value="1" @if($order->order_status == 1) selected @endif>Active</option>
                            <option value="0" @if($order->order_status == 0) selected @endif>InActive</option>
                          </select>    
                        </div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        <a href='{{ url('order') }}'  class="btn btn-primary">Back</a>
                     </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>

</script>
@endsection
