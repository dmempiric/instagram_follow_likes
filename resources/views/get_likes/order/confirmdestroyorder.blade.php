@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Order Delete</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("get_likes/order") }}'>Order Delete</a></li>
                  <li class="breadcrumb-item active">Destroy</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Delete the Order</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <form id="quickForm" action='{{ url("get_likes/canceltheorder/$order->id") }}' method="POST" enctype="multipart/form-data" >
                     @csrf
                     <div class="card-body">
                        <div class="form-group">
                           <label for="type"> Type</label>
                           <select class="form-control" name="type" disabled>
                            <option value="0" @if($order->type == "0") selected @endif>Like</option>
                          </select>    
                        </div>
                        <div class="form-group">
                           <label for="user_id">User Id</label>
                           <input type="text" name="user_id" value='{{ $order->user_id }}' class="form-control" id="user_id" placeholder="User Id" disabled >
                           <div class="error" style="color: red;">{{ $errors->first('user_id') }}</div>
                        </div>
                        @if(isset($order->custom_user_id))
                           <div class="form-group">
                              <label for="custom_user_id">Custom User Id</label>
                              <input type="text" name="user_id" value='{{ $order->custom_user_id }}' class="form-control" id="user_id" placeholder="User Id" disabled >
                              <div class="error" style="color: red;">{{ $errors->first('user_id') }}</div>
                           </div>
                        @endif
                        <div class="form-group">
                           <label for="post_id">Post Id</label>
                           <input type="text" name="post_id" value='{{ $order->user_id }}' class="form-control" id="post_id" placeholder="Post Id" disabled >
                           <div class="error" style="color: red;">{{ $errors->first('post_id') }}</div>
                        </div>
                     @if($order->type == "0")
                        <div class="form-group">
                           <label for="short_code">Short Code</label>
                           <input type="text" name="short_code" value='{{ $order->short_code }}' class="form-control" id="short_code" placeholder="Short Code" disabled >
                           <div class="error" style="color: red;">{{ $errors->first('short_code') }}</div>
                        </div>
                     @endif
                        <div class="form-group">
                           <label for="needed">Needed</label>
                           <input type="text" name="needed" value='{{ $order->needed }}' class="form-control" id="needed" placeholder="Needed" disabled >
                           <div class="error" style="color: red;">{{ $errors->first('short_code') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="recieved">Recieved</label>
                           <input type="text" name="recieved" value='{{ $order->recieved }}' class="form-control" id="recieved" placeholder="Recieved" disabled >
                           <div class="error" style="color: red;">{{ $errors->first('recieved') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="image_url">Image</label><br>
                           @if($order->type == 0)
                              <img src='{{ url("/public/get_likes/instagram/like/$order->image_url") }}' width="70px;">
                           @endif
                           <div class="error" style="color: red;">{{ $errors->first('recieved') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="recieved">Cancel Order Reason</label>
                            <textarea name="reason_for_cancelorder" class="form-control" rows="5" cols="10"></textarea>
                         </div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Delete</button>
                        <a href='{{ url('get_likes/order') }}'  class="btn btn-primary">Back</a>
                     </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>

</script>
@endsection
