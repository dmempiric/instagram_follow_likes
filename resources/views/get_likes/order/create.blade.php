@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Order </h1> </div>
               <div class="col-sm-6">
                   <ol class="breadcrumb float-sm-right">
                       <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                       <li class="breadcrumb-item "><a href='{{ url("get_likes/order") }}'>Order List</a></li>
                  <li class="breadcrumb-item active">create</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Add Order</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <form id="quickForm" action="{{ url('get_likes/order') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                     <div class="card-body">
                        <div class="form-group">
                           <label for="type">Type*</label>
                           <select class="form-control" onChange="checktype(this.value);" name="type" id="type" required>
                              <option >Type</option>
                              <option value="0" @if(old("type") == "1") selected @endif  selected >Like</option>
                            </select>  
                           <div class="error" style="color: red;">{{ $errors->first('type') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="user_id">User Id*</label>
                           <input type="text" name="user_id" value='{{ old("user_id") }}' class="form-control" id="user_id" placeholder="User Id" required >
                           <div class="error" style="color: red;">{{ $errors->first('user_id') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="custom_user_id">Custom User Id</label>
                           <input type="text" name="custom_user_id" value='{{ old("custom_user_id") }}' class="form-control" id="custom_user_id" placeholder="Custom User Id"  >
                           <div class="error" style="color: red;">{{ $errors->first('custom_user_id') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="needed">Needed*</label>
                           <input type="text" name="needed" value='{{ old("needed") }}' class="form-control" id="needed" placeholder="Needed" required >
                           <div class="error" style="color: red;">{{ $errors->first('needed') }}</div>
                        </div>
                        <div class="form-group like"  @if(old("type") == "1") style="display: none;" @endif>
                           <label for="short_code">Short code*</label>
                           <input type="text" name="short_code" value='{{ old("short_code") }}' class="form-control" id="short_code" placeholder="Short code" >
                           <div class="error" style="color: red;">{{ $errors->first('short_code') }}</div>
                        </div>
                        <div class="form-group like" @if(old("type") == "1") style="display: none;" @endif>
                           <label for="post_id">Post Id*</label>
                           <input type="text" name="post_id" value='{{ old("post_id") }}' class="form-control" id="post_id" placeholder="Post Id" >
                           <div class="error" style="color: red;">{{ $errors->first('post_id') }}</div>
                        </div>
                        <div class="form-group follow" @if(old("type") == "0") style="display: none;" @endif>
                           <label for="username">User Name*</label>
                           <input type="text" name="username" value='{{ old("username") }}' class="form-control" id="username" placeholder="User Name"  >
                           <div class="error" style="color: red;">{{ $errors->first('username') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="image">Image*</label>
                           <input type="file" name="image" value='{{ old("image") }}' class="form-control" id="image" required >
                           <div class="error" style="color: red;">{{ $errors->first('image') }}</div>
                        </div>
                        <div class="form-group">
                           <label for="order_status">Order Status*</label>
                           <select class="form-control" name="order_status" required>
                            <option>Color Status</option>
                            <option value="1" selected>Active</option>
                            <option value="0">InActive</option>
                          </select>    
                        </div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        <a href='{{ url('get_likes/order') }}'  class="btn btn-primary">Back</a>
                     </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>

   function checktype(value)
   {
      if(value == 0)
      {
         $(".like").show();
         $(".follow").hide();
      } else if (value == 1)
      {
         $(".like").hide();
         $(".follow").show();
      }
   }

</script>
@endsection
