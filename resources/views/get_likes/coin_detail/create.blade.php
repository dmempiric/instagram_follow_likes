@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Coin Detail</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("get_likes/coindetail") }}'>Coin Detail</a></li>
                  <li class="breadcrumb-item active">create</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Add Coin Detail</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <form id="quickForm" action="{{ url('get_likes/coindetail') }}" method="POST">
                     @csrf
                     <div class="card-body">
                        <div class="form-group">
                           <label for="quantity">Quantity</label>
                           <input type="text" name="quantity" value='{{ old("quantity") }}' class="form-control" id="name" placeholder="Coin Quantity" required >
                           <div class="error" style="color: red;">{{ $errors->first('quantity') }}</div>
                        </div>
                     </div>
                     <div class="card-body">
                        <div class="form-group">
                           <label for="indian_rate">Indian Rate</label>
                           <input type="text" name="indian_rate" value='{{ old("indian_rate") }}' class="form-control" id="indian_rate" placeholder="Indin Coin Rate" required >
                           <div class="error" style="color: red;">{{ $errors->first('indian_rate') }}</div>
                        </div>
                     </div>
                     <div class="card-body">
                        <div class="form-group">
                           <label for="other_rate">Other Rate</label>
                           <input type="text" name="other_rate" value='{{ old("other_rate") }}' class="form-control" id="other_rate" placeholder="Other Coin Rate" required >
                           <div class="error" style="color: red;">{{ $errors->first('other_rate') }}</div>
                        </div>
                     </div>
                     <div class="card-body">
                        <div class="form-group">
                           <label for="notes">Notes</label>
                           <input type="text" name="notes" value='{{ old("notes") }}' class="form-control" id="notes" placeholder="Notes" required >
                           <div class="error" style="color: red;">{{ $errors->first('notes') }}</div>
                        </div>
                     </div>
                     <div class="card-body">
                        <div class="form-group">
                           <label for="is_popular"> Popular Status</label>
                           <div class="custom-control custom-switch">
                              <input type="checkbox" class="custom-control-input" name="is_popular" id="customSwitch" checked >
                              <label class="custom-control-label" for="customSwitch" ></label>
                           </div>
                        </div>
                     </div>
                     <div class="card-body">
                        <div class="form-group">
                           <label for="coin_status"> Color Status</label>
                           <select class="form-control" name="coin_status" required>
                            <option>Color Status</option>
                            <option value="1" selected>Active</option>
                            <option value="0">InActive</option>
                          </select>    
                        </div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        <a href='{{ url('get_likes/coindetail') }}'  class="btn btn-primary">Back</a>
                     </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>

</script>
@endsection
