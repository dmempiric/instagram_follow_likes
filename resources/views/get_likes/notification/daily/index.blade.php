@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Notification Daily</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("get_likes/notification-daily") }}'>Notification Daily</a></li>
                  <li class="breadcrumb-item active">Index</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                <div class="card-header">
                   <h3 class="card-title">App Data Other List</small></h3>
                </div>
                <!-- /.card-header -->
                <!-- form start --> 
                <div class="card-body">
                    <div class="form-group">
                        <label for="notification_title">Notification Title</label>
                        <input type="text" name="notification_title" class="form-control" id="notification_title"  value="{{ $appdata->notification_title }}" placeholder="Notification Title" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('notification_title') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="notification_message">Notification Message</label>
                        <input type="text" name="notification_message" class="form-control" id="notification_message" placeholder="Notification Message" value="{{ $appdata->notification_message }}"  disabled>
                        <div class="error"  style="color: red;">{{ $errors->first('notification_message') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="notification_sho w">Notification Show</label><br>
                        @if($appdata->notification_show == "1")
                        <button class="btn btn-primary">Yes</button>
                        @endif
                        @if($appdata->notification_show == "0")
                        <button class="btn btn-primary">No</button>
                        @endif 
                     </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                   <a href='{{ url("get_likes/notification-daily/$appdata->id") }}' class="btn btn-primary">Edit</a>
                </div>
                </form>
             </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script></script>
@endsection
