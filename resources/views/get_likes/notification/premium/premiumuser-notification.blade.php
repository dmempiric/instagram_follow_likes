@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Send Notification </h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                  <li class="breadcrumb-item active">Send Notification</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Send Notification</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                <form id="quickForm" action="{{ url("get_likes/notification-premium") }}"  method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                           <label for="playstore_version">Title</label>
                           <input type="text" name="title" class="form-control" id="title" placeholder="title" required>
                           <div class="error" style="color: red;">{{ $errors->first('title') }}</div>
                        </div>   
                        <div class="form-group">
                           <label for="description">Description</label>
                           <input type="text" name="description" class="form-control" id="description" placeholder="description" required>
                           <div class="error" style="color: red;">{{ $errors->first('description') }}</div>
                        </div>   
                        <div class="form-group">
                           <label for="notification_image">Notification Message</label>
                           <input type="file" name="notification_image" class="form-control" id="notification_image">
                        </div>   
                        <div class="form-group">
                            <label for="reciever_type">Reciever Type</label>
                            <select class="form-control" name="reciever_type" required>
                                <option  disabled>Notification User Type</option>
                                <option value="1">Premium</option>
                                <option value="2">None Premium</option>
                                <option value="3">All</option>
                            </select>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" name="submit" value="submit"  class="btn btn-primary">Submit</button>
                    </div>
                </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $.datetimepicker.setLocale('pt-BR');
     $('#datetimepicker').datetimepicker();
  });
  $(document).ready(function() {
      $.other_endtime.setLocale('pt-BR');
     $('#other_endtime').other_endtime();
  });
</script>
@endsection
