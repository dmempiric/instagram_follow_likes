@extends('get_likes.layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>IP Address </h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("get_likes/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("get_likes/ipaddress") }}'>IP Address </a></li>
                  <li class="breadcrumb-item active">create</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Add Block Ip Address</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <form id="quickForm" action="{{ url('get_likes/ipaddress') }}" method="POST">
                     @csrf
                     <div class="card-body">
                        <div class="form-group">
                           <label for="ip">IP Address</label>
                           <input type="text" name="ip" value='{{ old("ip") }}' class="form-control" id="ip" placeholder="IP Address" required >
                           <div class="error" style="color: red;">{{ $errors->first('ip') }}</div>
                        </div>
                     </div>
                     <div class="card-body">
                        <div class="form-group">
                           <label for="status">Status</label>
                           <select class="form-control" name="status" required>
                            <option>Status</option>
                            <option value="1" selected>Active</option>
                            <option value="0">InActive</option>
                          </select>    
                        </div>
                     </div>
                     <!-- /.card-body -->
                     <div class="card-footer">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        <a href='{{ url('get_likes/ipaddress') }}'  class="btn btn-primary">Back</a>
                     </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>

</script>
@endsection
