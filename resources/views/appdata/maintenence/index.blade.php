@extends('layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>App Data Maintenence</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("appdata-maintenence") }}'>App Data Maintenence</a></li>
                  <li class="breadcrumb-item active">Edit</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">App data Maintenence List</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <div class="card-body">
                     <div class="form-group">
                        <label for="maintenence_message">Maintenence Message</label>
                        <input type="text" name="maintenence_message" class="form-control" id="maintenence_message" value="{{ $appdata->maintenence_message }}" placeholder="Maintenence Message" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('maintenence_message') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="maintenance_mode">Maintenece Mode</label><br>
                        @if(in_array("like",json_decode($appdata->maintenence_mode)))
                           <button class="btn btn-primary">Like</button>
                        @endif
                        @if(in_array("follow",json_decode($appdata->maintenence_mode)))
                           <button class="btn btn-primary">Follow</button>
                        @endif
                     </div>
                     <div class="form-group">
                        <label for="update_mode"> Update Mode</label><br>
                        @if($appdata->update_mode == "none")
                        <button class="btn btn-primary">None</button>
                        @endif
                        @if($appdata->update_mode == "flexible")
                           <button class="btn btn-primary">Flexible</button>
                        @endif
                        @if($appdata->update_mode == "immiediate")
                           <button class="btn btn-primary">Immiediate</button>
                        @endif 
                     </div>
                     <div class="form-group">
                        <label for="update_url">Update Url</label><br>
                        <input type="text" name="update_url" class="form-control"   id="update_url" placeholder="Update Url"  value="{{ $appdata->update_url }}" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('update_url') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="update_message">Update Message</label>
                        <input type="text" name="update_message"  value="{{ $appdata->update_message }}" class="form-control" id="update_message" placeholder="Update Message" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('update_message') }}</div>
                     </div>
                     <div class="form-group">
                        <label>Payment Methode</label><br>
                        @if(in_array("inapp",json_decode($appdata->payment_methode)))
                           <button class="btn btn-primary">In App</button>
                        @endif
                        @if(in_array("paypal",json_decode($appdata->payment_methode)))
                           <button class="btn btn-primary">PayPal</button>
                        @endif
                     </div>
                       
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <a href='{{ url("appdata-maintenence/$appdata->id") }}' class="btn btn-primary">Edit</a>
                  </div>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script></script>
@endsection
