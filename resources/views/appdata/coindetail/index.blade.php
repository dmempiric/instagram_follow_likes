@extends('layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>App Data Coin Detail</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("appdata-coindetail") }}'>App Data Coin Detail</a></li>
                  <li class="breadcrumb-item active">index</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">App data  Coin Detail List</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                  <form id="quickForm" action="{{ url("appdata-coindetailupdate") }}"  method="POST">
                  @csrf
                  <div class="card-body">
                     <div class="form-group">
                        <label for="earn_like_coin"> Earn Like Coin</label>
                        <input type="text" name="earn_like_coin" class="form-control" id="earn_like_coin" value="{{ $appdata->earn_like_coin }}" placeholder="earn Like Coin" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('earn_like_coin') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="rate">spend Like Coin</label>
                        <input type="text" name="spend_like_coin" class="form-control" id="spend_like_coin" value="{{ $appdata->spend_like_coin }}"  placeholder="spend Like Coin" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('rate') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="earn_follower_coin">Earn Follow Coin</label>
                        <input type="text" name="earn_follower_coin" class="form-control" id="earn_follower_coin" value="{{ $appdata->earn_follow_coin }}" placeholder="earn Follower Coin" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('rate') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="spend_follow_coin">Spend Follow Coin</label>
                        <input type="text" name="spend_follow_coin" class="form-control" id="spend_follow_coin" value="{{ $appdata->spend_follow_coin }}" placeholder="earn Follow Coin" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('spend_follow_coin') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="referral_coin">Refferral Coin</label>
                        <input type="text" name="referral_coin" class="form-control" id="referral_coin" placeholder="Refferal Coin" value="{{ $appdata->referral_coin }}" disabled>
                        <div class="error" style="color: red;">{{ $errors->first('referral_coin') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="default_coin">Default Coin</label>
                        <input type="text" name="default_coin" class="form-control" id="default_coin" placeholder="Default Coin" value="{{ $appdata->default_coin }}" disabled> 
                        <div class="error" style="color: red;">{{ $errors->first('default_coin') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="from_add_coin">From Add Coin</label>
                        <input type="text" name="from_add_coin" class="form-control" id="from_add_coin" placeholder="Add Coin" value="{{ $appdata->from_add_coin }}" disabled> 
                     </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <a href='{{ url("appdata-coindetail/$appdata->id") }}' class="btn btn-primary">Edit</a>
                  </div>
                  </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script></script>
@endsection
