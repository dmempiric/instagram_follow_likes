@extends('layouts.adminmaster')
@section('content')


@section('css')

@endsection

<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>App Data Offer</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("appdata-offer") }}'>App Data Offer</a></li>
                  <li class="breadcrumb-item active">Edit</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Edit App Data Offer</small></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start --> 
                <form id="quickForm" action="{{ url("appdata-offerupdate/$appdata->id") }}"  method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                     <div class="form-group">
                        <label for="offer">Offer*</label>
                        <select class="form-control" name="offer" required>
                           <option  disabled>Offer Status</option>
                           <option value="1"  @if($appdata->offer == "1") selected @endif>Start</option>
                           <option value="0" @if($appdata->offer == "0") selected @endif>End</option>
                        </select>
                        <div class="error" style="color: red;">{{ $errors->first('offer') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_percentage">Offer in Percentage*</label>
                        <input type="text" name="offer_percentage" value="{{ $appdata->offer_percentage }}" class="form-control" id="offer_percentage" placeholder="Offer in Percentage" required>
                        <div class="error" style="color: red;">{{ $errors->first('offer_percentage') }}</div>
                     </div>
                     
                     <div class="form-group">
                        <label for="offer_starttime">Offer Starttime*</label><br>
                        <div class='input-group date'>
                           <input id="datetimepicker" name="offer_starttime" class="form-control"  type="text" required>
                           <input type="hidden" id="offerstarttime" value="{{  date("Y/m/d h:i",strtotime($appdata->offer_starttime)) }}" >
                           <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                        <div class="error" style="color: red;">{{ $errors->first('offer_starttime') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_endtime">Offer Endtime*</label>
                        <div class='input-group date' >
                           <input id="offer_endtime" name="offer_endtime" class="form-control" type="text" required>
                           <input type="hidden" id="offerendtime" value="{{  date("Y/m/d h:i",strtotime($appdata->offer_endtime)) }}" >
                           <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                        <div class="error" style="color: red;">{{ $errors->first('offer_endtime') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_discount_text">Offer Discount Title*</label>
                        <input type="text" name="offer_discount_title" value="{{ $appdata->offer_discount_title }}"   class="form-control" id="offer_discount_title" placeholder="Offer Discount Title" required>
                        <div class="error" style="color: red;">{{ $errors->first('offer_discount_title') }}</div>
                     </div>
                     <div class="form-group">
                        <label for="offer_discount_text">Offer Discount Description*</label>
                        <textarea type="text" name="offer_discount_text" class="form-control" id="offer_discount_text" placeholder="Offer Discount Description" required>{{ $appdata->offer_discount_text }}</textarea>
                        <div class="error" style="color: red;">{{ $errors->first('offer_discount_text') }}</div>
                     </div>
                     <div class="form-group"> 
                        <label for="offer_discount_image">Offer Discount Image</label><br>
                        <div class="imageshow">
                           @php
                              //  dd($appdata->offer_discount_image);
                           @endphp
                        @if(isset($appdata->offer_discount_image))
                           <img src='{{ url("/public/instagram/app_data/$appdata->offer_discount_image") }}' alt="Preview" width="170px;" >
                           <a class="remove-image" onclick="removeOfferImage()" style="color: black; font-size: 26px" style="display: inline;">&#215;</a>
                           @endif
                        </div>
                        <input type="file" name="offer_discount_image" class="form-control" id="offer_discount_image" >
                        <div class="error" style="color: red;">{{ $errors->first('offer_discount_image') }}</div>
                     </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" name="submit" value="submit"  class="btn btn-primary">Submit</button>
                        <a href='{{ url("appdata-offer") }}' class="btn btn-primary">Back</a>
                    </div>
                </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script>
   var start_time = $('#offerstarttime').val();
   $('#datetimepicker').datetimepicker();
   $('#datetimepicker').datetimepicker({
      value:start_time,
      minDate: 0
   });

   var end_time = $('#offerendtime').val();
   $('#offer_endtime').datetimepicker();
   $('#offer_endtime').datetimepicker({
      value:end_time,
      minDate: 0
});

   function removeOfferImage()
   {
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });

      var imageremove = 'imageremove';
      $('.imageshow').hide();
      
      $.ajax({
         url: "{{ url('removeofferimage') }}", 
         type: 'POST',
         data: {removeimage:imageremove},
         success:function(data){
               
         }
      });
   }
</script>
@endsection
