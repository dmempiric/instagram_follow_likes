@extends('layouts.adminmaster')
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Notification Daily</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href='{{ url("/home") }}'>Home</a></li>
                  <li class="breadcrumb-item "><a href='{{ url("notification-daily") }}'>Notification Daily</a></li>
                  <li class="breadcrumb-item active">Index</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <!-- left column -->
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
               <!-- jquery validation -->
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Notification Daily</small></h3>
                  </div>
                  <form id="quickForm" action='{{ url("notification-dailyupdate/$appdata->id") }}'  method="POST" enctype="multipart/form-data">3
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="notification_title">Notification Title</label>
                            <input type="text" name="notification_title" class="form-control" id="notification_title"  value="{{ $appdata->notification_title }}" placeholder="Notification Title" required>
                            <div class="error" style="color: red;">{{ $errors->first('notification_title') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="notification_message">Notification Message</label>
                            <input type="text" name="notification_message" class="form-control" id="notification_message" placeholder="Notification Message" value="{{ $appdata->notification_message }}"  required>
                            <div class="error"  style="color: red;">{{ $errors->first('notification_message') }}</div>
                        </div>
                        <div class="form-group">
                            <label for="notification_sho w">Notification Show</label>
                            <select class="form-control" name="notification_show" required>
                            <option  disabled>Notification Show</option>
                            <option value="1" @if($appdata->web == "1") selected @endif >Yes</option>
                            <option value="0" @if($appdata->web == "0") selected @endif>No</option>
                            </select>
                        </div>
                    </div>
                  <div class="card-footer">
                    <button type="submit" name="submit" value="submit"  class="btn btn-primary">Submit</button>
                    <a href='{{ url("appdata-other") }}' class="btn btn-primary">Back</a>
                </div>
                </form>
               </div>
               <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
            </div>
            <!--/.col (right) -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('script')
<script></script>
@endsection
