<?php

namespace App\Console;

use App\Console\Commands\Offer;
use App\Console\Commands\OfferCheck;
use App\Console\Commands\accessgoogletoken;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\insta_likes_offer_check;
use App\Console\Commands\insta_likes_accessgoogletoken;
use App\Console\Commands\insta_likes_offer_notification;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\OfferCheck::class,
        Commands\accessgoogletoken::class,
        Commands\Offer::class,
        Commands\insta_likes_accessgoogletoken::class,
        Commands\insta_likes_offer_notification::class,
        Commands\insta_likes_offer_check::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //instagram_likes_follows
        $schedule->command('offer:start')->dailyAt('05:00');
        $schedule->command('access:token')->hourlyAt(55);
        $schedule->command('offer:check')->everyMinute();
        
        //instagram_likes
        $schedule->command('insta_likes_access:token')->hourlyAt(55);
        $schedule->command('insta_likes_offer:start')->dailyAt('05:00');
        $schedule->command('insta_likes_offer:check')->everyMinute();
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
