<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\get_likes\appData;
use App\Http\Controllers\get_likes\API\BaseController;

class insta_likes_accessgoogletoken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insta_likes_access:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'refresh the access token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $basecontroller = new BaseController;
        $data = $basecontroller->getRefreshToken(); 
        $updatetoken = array(
            "access_token" => $data->access_token
        );
        appData::where("id",1)->update($updatetoken);
        $this->info('Access:Token updateed successfully!');
    }
}
