<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\appData;
use Illuminate\Console\Command;
use App\Http\Controllers\API\BaseController;

class Offer extends Command 
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offer:start';

    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Offer start command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $appdata = appData::find(1);
        $firebaseToken = User::where("ftoken","!=","")->whereNotNull('ftoken')->pluck('ftoken')->all();
        if($appdata->offer == "1")
        {
            $offer_end = date('Y-m-d h:i:s',strtotime($appdata->offer_endtime)-19800);
            $current_time = date('Y-m-d h:i:s');
            if($offer_end < $current_time)
            {
                $offerData = array(
                    "offer" => "0"
                );
                appdata::where("id","1")->update($offerData);

            } 
            $appdataupdate = appData::find(1);
            if($appdataupdate->offer == "1")
            {
                $detailNotification = array();
                $detailNotification['title'] =  $appdata->offer_discount_title;
                $detailNotification['description'] =  $appdata->offer_discount_text;
                if(isset($appdata->offer_discount_image))
                {
                    $file = '/public/instagram/app_data/'.$appdata->offer_discount_image;
                    $image_path = env('APP_URL').$file;
                    $detailNotification['image'] = $image_path;
                }

                $SERVER_API_KEY = "AAAAisqJu_Y:APA91bHPUEIGk7fGrfO-ZsqvqQRgk4itAIdfDVtnyZoSTi9WMRKz5omjxCfx1Loi1mkI-3Z72NnZd4EUEak8m_WWpLkHL9V2hoFbW-3X7VB4YOQ4dXm41Lkvbnsxw8DwSEdI9U1U0lfL";
        
                if(count($detailNotification) == "3") {
                    $data = [
                        "registration_ids" => $firebaseToken,
                        "notification" => [
                            "title" => $detailNotification['title'],
                            "body" => $detailNotification['description'],  
                            "image" => $detailNotification['image'],  
                        ]
                    ];
                } else if (count($detailNotification) == "2")
                {
                    $data = [
                        "registration_ids" => $firebaseToken,
                        "notification" => [
                            "title" => $detailNotification['title'],
                            "body" => $detailNotification['description'],  
                        ]
                    ];
                }
                $dataString = json_encode($data);
                $headers = [
                    'Authorization: key=' . $SERVER_API_KEY,
                    'Content-Type: application/json',
                ];
            
                $ch = curl_init();
            
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                $response = curl_exec($ch);
                // dd($response);
            }
        
        }
    }
}