<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\get_likes\appData;

class insta_likes_offer_check extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insta_likes_offer:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the offer status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $appData = appData::find(1);
        if($appData->offer == 0)
        {
            if(date('Y-m-d H:i', strtotime($appData->offer_starttime)) <= date('Y-m-d H:i') && date('Y-m-d H:i', strtotime($appData->offer_endtime)) >= date('Y-m-d H:i'))
            {
                $appDataRecord = array('offer' => 1);
                appData::where('id',1)->update($appDataRecord);
            }
        }
        else if($appData->offer == 1)
        {
            if(date('Y-m-d H:i', strtotime($appData->offer_endtime)) <= date('Y-m-d H:i'))
            {
                $appDataRecord = array('offer' => 0);
                appData::where('id',1)->update($appDataRecord);
            }
        } 
        $this->info('Work properly!');

    }
}
