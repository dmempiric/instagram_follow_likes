<?php

namespace App\Console\Commands;

use App\Models\appData;
use Illuminate\Console\Command;
use App\Http\Controllers\API\BaseController;

class accessgoogletoken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'access:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'refresh the access token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $basecontroller = new BaseController;
        $data = $basecontroller->getRefreshToken(); 

        $updatetoken = array(
            "access_token" => $data->access_token
        );
        appData::where("id",1)->update($updatetoken);
        $this->info('Access:Token updateed successfully!');
    }
}
