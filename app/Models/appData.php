<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class appData extends Model
{
    use HasFactory, softDeletes;

    public $fillable = [
                    'earn_like_coin',
                    'spend_like_coin',
                    'earn_follow_coin',
                    'spend_follow_coin',
                    'referral_coin',
                    'default_coin',
                    'from_add_coin',
                    'maintenence_mode',
                    'update_mode',
                    'update_url',
                    'payment_methode',
                    'game_id',
                    'banner_id',
                    'initial_id',
                    'maintenence_message',
                    'notification_title',
                    'notification_message',
                    'notification_show',
                    'update_message',
                    'playstore_version',
                    'user_agent',
                    'share_url',
                    'offer',
                    'offer_percentage',
                    'offer_starttime',
                    'offer_endtime', 
                    'offer_discount_title', 
                    'offer_discount_text', 
                    'offer_discount_image', 
                    'web_login', 
                    'web', 
                    'website', 
                    'email', 
                    'privacy_policy', 
                    'facebook', 
                    'instagram', 
                    'rate_dialog', 
                    'share_dialog', 
                ];

    protected $dates = ['deleted_at'];
}
