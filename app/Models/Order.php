<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory , softDeletes;

    protected $fillable = [
        "type",
        "user_id",
        "username",
        "custom_user_id",
        "post_id",
        "short_code",
        "needed",
        "recieved",
        "image_url",
        "no_like_log",
        "reason_for_cancelorder",
        "order_status",
    ];

    protected $dates = ['deleted_at'];
}
