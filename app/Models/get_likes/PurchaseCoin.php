<?php

namespace App\Models\get_likes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseCoin extends Model
{
    use HasFactory, softDeletes;
    protected $connection = 'mysql2';

    public $fillable = ['user_id','email','payment_id','payment_type','purchased_coin','payment_state','amount','payment_time','country_code','payment_method','transaction_id'];

    protected $dates = ['deleted_at'];
}
