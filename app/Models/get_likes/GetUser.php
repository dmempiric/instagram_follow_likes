<?php

namespace App\Models\get_likes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class GetUser extends Model
{
    use HasFactory ,Notifiable;

    protected $connection = 'mysql2';
    protected $table = 'get_users';
    protected $fillable = [
        'name',
        'email',
        'password',
        'user_id',
        'ftoken',
        'parent_id',
        'username',
        'profile_picture',
        'total_coins',
        'referral_code',
        'is_purchase',
        'is_referred',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
