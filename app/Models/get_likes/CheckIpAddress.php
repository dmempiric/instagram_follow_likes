<?php

namespace App\Models\get_likes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CheckIpAddress extends Model
{
    use HasFactory, softDeletes;
    protected $connection = 'mysql2';

    protected $fillable = ['ip','status'];

    protected $dates = ['deleted_at'];
}
