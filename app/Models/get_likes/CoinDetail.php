<?php

namespace App\Models\get_likes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

class CoinDetail extends Model
{
    use HasFactory, softDeletes;
    protected $connection = 'mysql2';

    public $fillable = ['quantity','indian_rate','other_rate','notes','is_popular','coin_status'];

    
    protected $dates = ['deleted_at'];

}
