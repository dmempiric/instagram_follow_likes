<?php

namespace App\Models\get_likes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testing_purpose extends Model
{
    use HasFactory;
    protected $connection = 'mysql2';

    protected $fillable = ['data'];
}
