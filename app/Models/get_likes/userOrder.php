<?php

namespace App\Models\get_likes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class userOrder extends Model
{
    use HasFactory,softDeletes;
    protected $connection = 'mysql2';
    protected $fillable = [
                    "user_id",
                    "packageName",
                    "acknowledged",
                    "orderId",
                    "productId",
                    "token",
                    "developerPayload",
                    "purchaseTime",
                    "purchaseState",
                    "purchaseToken",
                    "inapp",
                    "username",
                    "code",
                    "two_factor_identifier"];

    protected $dates = ['deleted_at']; 
        
}
