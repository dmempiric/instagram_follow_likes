<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class already_like_follow extends Model
{
    use HasFactory, softDeletes;

    protected $fillable = [
        'type',
        'user_id',
        'custom_user_id',
        'post_id',
        'already_like_follow',
    ];

    protected $dates = ['deleted_at'];
}