<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

class CoinDetail extends Model
{
    use HasFactory, softDeletes;

    public $fillable = ['quantity','indian_rate','other_rate','notes','is_popular','coin_status'];

    
    protected $dates = ['deleted_at'];

}
