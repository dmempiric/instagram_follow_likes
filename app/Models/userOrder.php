<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class userOrder extends Model
{
    use HasFactory,softDeletes;

    protected $fillable = [
                    "user_id",
                    "packageName",
                    "acknowledged",
                    "orderId",
                    "productId",
                    "token",
                    "developerPayload",
                    "purchaseTime",
                    "purchaseState",
                    "purchaseToken",
                    "inapp",
                    "username",
                    "code",
                    "two_factor_identifier"];

    protected $dates = ['deleted_at']; 
        
}
