<?php

namespace App\Http\Controllers;

use App\Models\CoinDetail;
use Illuminate\Http\Request;

class CoinDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coindetails = CoinDetail::all();
        return view('coin_detail.index',compact('coindetails'));         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coin_detail.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coinDetails = $request->all();
        if($request->is_popular == "on")
        {
            $coinDetails['is_popular'] = "1";
        } else {
            
            $coinDetails['is_popular'] = "0";
        }
        CoinDetail::create($coinDetails);
        return redirect('coindetail')->with('success','Coin Detail Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function show(CoinDetail $coinDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(CoinDetail $coindetail)
    {

        return view('coin_detail.edit',compact('coindetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CoinDetail $coindetail)
    {
        $coinDetails = $request->all();
        if($request->is_popular == "on")
        {
            $coinDetails['is_popular'] = "1";
        } else {
            
            $coinDetails['is_popular'] = "0";
        }
        $coindetail->update($coinDetails);
        return redirect('coindetail')->with('info','Coin Detail Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            CoinDetail::find($id)->delete();
            return redirect('coindetail')->with('error','Coin Detail Deleted Successfully');

    }
}
