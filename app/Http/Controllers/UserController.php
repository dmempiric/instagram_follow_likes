<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\userOrder;
use App\Models\PurchaseCoin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function userList()
    {
        $users = User::where("id","!=","1")->orderBy('id','desc')->get();
        return view('user.index',compact("users"));
    }
    
    public function userCreate()
    {

        return view('user.create');
    }

    public function userCreates(Request $request)
    {
        // dd($request->all());
        $user = new User();
        $user->user_id = $request->user_id;
        $user->username = $request->username;
        $user->total_coins = $request->total_coins;
        $user->referral_code = $request->referral_code;
        $user->is_purchase = $request->is_purchase;
        $user->is_referred = $request->is_referred;
        $user->fromapp = 0;
        $user->status = $request->status;
        $user->save();
        return redirect('userlist')->with("success","User Added Successfully");
    }

    public function userEdit($id)
    {
        $user = User::find($id);
        return view('user.edit',compact('user'));
    }

    public function userUpdate(Request $request ,$id)
    {
        $user = User::find($id);
        $userStatus = array("status" => $request->status);
        if($user->parent_id == "0")
        {
            $parentId = $user->user_id;
            User::where("user_id",$user->user_id)->update($userStatus);
        } else {
            $parentId = $user->parent_id;
            User::where("user_id",$user->parent_id)->update($userStatus);

        } 
        $childAccounts =  User::where("parent_id",$parentId)->get();
        if(count($childAccounts) > 0)
        {
            foreach($childAccounts as $childAccount)
            {
                User::where("user_id",$childAccount->user_id)->update($userStatus);
            }
        }
        if($user->parent_id == "0")
        {
            $data = array(
                "user_id" => $request->user_id,
                "username" => $request->username,
                "total_coins" => $request->total_coins,
                "referral_code" => $request->referral_code,
                "is_purchase" => $request->is_purchase,
                "is_referred" => $request->is_referred,
            );
        } else {
            $data = array(
                "user_id" => $request->user_id,
                "username" => $request->username,
                "parent_id" => $request->parent_id,
            );
        }
        
        User::where('id',$id)->update($data);
        return redirect('userlist')->with("info","User Updated Successfully");

    }


    public function userDelete($id)
    {
         User::find($id)->delete();
        return redirect('userlist')->with("error","User Deleted Successfully");
    }

    public function purchaseUserList()
    {
        $users = User::where("is_purchase","1")->get();
        return view('user.purchaseuser',compact("users"));
    }
   
    public function inAppPurchaseList()
    {
       $userorders =  userOrder::all();
       return view('user.inapp_purchaselist',compact("userorders"));
    }

    public function inAppPurchaseDelete($id)
    {
        userOrder::where("id",$id)->delete();
        return redirect('inappPurchase-list')->with("error","Purchase Coin Deleted Successfully");
        // return view('user.inapp_purchaselist',compact("userorders"));
    }

    public function purchaseCoinList()
    {
        $purchasecoinlists = PurchaseCoin::orderBy('id', 'DESC')->get();
        return view('user.purchasecoinlist',compact("purchasecoinlists"));
    }

    public function purchaseCoinEdit($id)
    {
        $purchasecoinlist = PurchaseCoin::find($id);
        return view('user.purchasecoinedit',compact("purchasecoinlist"));
    }

    public function purchaseCoinUpdate(Request $request,$id)
    {
        $data = array(
            "user_id" => $request->user_id,
            "email" => $request->email,
            "payment_id" => $request->payment_id,
            "payment_type" => $request->payment_type,
            "purchased_coin" => $request->purchased_coin,
            "payment_state" => $request->payment_state,
            "payment_time" => date("Y-m-dT H:i:s",strtotime($request->payment_time)),
            "country_code" => $request->country_code
        );

        PurchaseCoin::where("id",$id)->update($data);
        return redirect('purchasecoin-list')->with("info","Purchase Coin Updated Successfully");
    }

    public function purchaseCoinDelete($id)
    {
        PurchaseCoin::find($id)->delete();
        return redirect('purchasecoin-list')->with("error","Purchase Coin Deleted Successfully");
    }

    public function SearchUserList(Request $request)
    {
        $startDate =  str_replace("/","-",$request->startdate);
        $startdates = date("Y-m-d",strtotime($startDate));
        $endDate =  str_replace("/","-",$request->enddate);
        $enddates = date("Y-m-d",strtotime($endDate));
        
        $users = User::whereBetween('created_at', [$startdates, $enddates])->where("id","!=","1")->get();
 
        $requeststartdate = $request->startdate;
        $requestenddate = $request->enddate;

        return view('user.index',compact("users","requeststartdate","requestenddate"));
    }

    public function SearchPremiumUserList(Request $request)
    {
        $startDate =  str_replace("/","-",$request->startdate);
        $startdates = date("Y-m-d",strtotime($startDate));
        $endDate =  str_replace("/","-",$request->enddate);
        $enddates = date("Y-m-d",strtotime($endDate));
        $users = User::where("is_purchase","1")->whereBetween('created_at', [$startdates, $enddates])->get();
        $requeststartdate = $request->startdate;
        $requestenddate = $request->enddate;
        // dd(count($users));
        
        return view('user.purchaseuser',compact("users","requeststartdate","requestenddate"));
    }
}

