<?php

namespace App\Http\Controllers\get_likes;

use Illuminate\Http\Request;
use App\Models\get_likes\Order;
use App\Models\get_likes\appData;
use App\Models\get_likes\GetUser;
use App\Models\get_likes\already_like_follow;
use App\Http\Controllers\get_likes\API\BaseController;

class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders =  Order::where('needed','!=','0')->orderBy('id','desc')->get();
        return view('get_likes.order.index',compact("orders"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('get_likes.order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = GetUser::where("user_id",$request->user_id)->first();
        if(isset($user->id))
        {
            //user is registered
            if($user->parent_id == "0")
            {
                $ChildAccount = GetUser::where("parent_id",$request->user_id)->pluck('user_id')->all();
                array_push($ChildAccount,$request->user_id);
            } else {
                $ChildAccount = GetUser::where("parent_id",$user->parent_id)->pluck('user_id')->all();
                array_push($ChildAccount,$user->parent_id);
            }
            if(isset($request->custom_user_id))
            {
                array_push($ChildAccount,$request->custom_user_id);
            }
            $encode_already_like_user = json_encode($ChildAccount);
        } else {
            //user is not registered
            if(isset($request->custom_user_id))
            {
                $userAccount = array($request->user_id,$request->custom_user_id);
            } else {
                $userAccount = array($request->user_id);
            }
            $encode_already_like_user = json_encode($userAccount);
        }
        if($request->type == 0)
        {
            $validatedData = $request->validate([
                'user_id' => 'required',
                'needed' => 'required',
                'short_code' => 'required',
                'post_id' => 'required',
                'image' => 'required'
            ]);
            $order = Order::where('post_id',$request->post_id)->where('user_id',$request->user_id)->where('type',$request->type)->first();
            if(isset($order->id))
            {   
                $total_needed = $order->needed+$request->needed;
                $order_data = array(
                    'needed' =>  $total_needed
                );
                Order::where('id',$order->id)->update($order_data);
                if(isset($request->custom_user_id))
                {
                    $already_like_follow = already_like_follow::where("user_id",$request->user_id)->where('custom_user_id',$request->custom_user_id)->where('type',$request->type)->first();
                } else {
                    $already_like_follow = already_like_follow::where("user_id",$request->user_id)->where('custom_user_id',$request->user_id)->where('type',$request->type)->first();
                }
                $already_like_follow = json_decode($already_like_follow->already_like_follow);
                if(!in_array($request->custom_user_id,$already_like_follow))
                {
                    array_push($already_like_follow,$request->custom_user_id);
                } 
                $already_like_follow_update = array(
                    'already_like_follow' => json_encode($already_like_follow)
                );
                if(isset($request->custom_user_id))
                {
                    $already_like_follow = already_like_follow::where("user_id",$request->user_id)->where('custom_user_id',$request->custom_user_id)->where('type',$request->type)->update($already_like_follow_update);
                } else {
                    $already_like_follow = already_like_follow::where("user_id",$request->user_id)->where('custom_user_id',$request->user_id)->where('type',$request->type)->update($already_like_follow_update);
                }

                return redirect('get_likes/order')->with('success','Order Created Succesfully');
            } else {
                $likeImage = $request->image;
                $originalPath = public_path().'/get_likes/instagram/like/';
                $filename = basename($request->image);
                $likeFileName = uniqid().'_like_order' . '.' .'png';
                $likeImage->move($originalPath, $likeFileName);
                
                $order = new Order();
                $order->type = $request->type;
                $order->user_id = $request->user_id;
                if(isset($request->custom_user_id))
                {
                    $order->custom_user_id = $request->custom_user_id;
                } else {
                    $order->custom_user_id = $request->user_id;
                }
                $order->post_id = $request->post_id;
                $order->short_code = $request->short_code;
                $order->needed = $request->needed;
                $order->image_url = $likeFileName;
                $order->fromapp = 0;
                $order->save();

                $already_like_follow = new already_like_follow;
                $already_like_follow->type  = $request->type;
                $already_like_follow->post_id  = $request->post_id;
                if(isset($request->custom_user_id))
                {
                    $already_like_follow->custom_user_id  = $request->custom_user_id;
                } else {
                    $already_like_follow->custom_user_id  = $request->user_id;
                }
                $already_like_follow->user_id  = $request->user_id;
                $already_like_follow->already_like_follow  = $encode_already_like_user;
                $already_like_follow->save();
                return redirect('get_likes/order')->with('success','Order Created Successfully');
            }
        } else if($request->type == 1){
            $validatedData = $request->validate([
                'user_id' => 'required',
                'needed' => 'required',
                'username' => 'required',
                'image' => 'required'
            ]);
            if(isset($request->custom_user_id))
            {
                $order = Order::where('user_id',$request->user_id)->where('custom_user_id',$request->custom_user_id)->where('type',$request->type)->first();
            } else {
                $order = Order::where('user_id',$request->user_id)->where('custom_user_id',0)->where('type',$request->type)->first();
            }
            if(isset($order->id))
            {
                //order exist

                $total_needed = $order->needed+$request->needed;
                $order_data = array(
                    'needed' =>  $total_needed
                );
                Order::where('id',$order->id)->update($order_data);
                if(isset($request->custom_user_id))
                {
                    $already_like_follow = already_like_follow::where("user_id",$request->user_id)->where('custom_user_id',$request->custom_user_id)->where('type',$request->type)->first();
                } else {
                    $already_like_follow = already_like_follow::where("user_id",$request->user_id)->where('custom_user_id',$request->user_id)->where('type',$request->type)->first();
                }
                $already_like_follow = json_decode($already_like_follow->already_like_follow);
                if(!in_array($request->custom_user_id,$already_like_follow))
                {
                    array_push($already_like_follow,$request->custom_user_id);
                } 
                $already_like_follow_update = array(
                    'already_like_follow' => json_encode($already_like_follow)
                );
                if(isset($request->custom_user_id))
                {
                    $already_like_follow = already_like_follow::where("user_id",$request->user_id)->where('custom_user_id',$request->custom_user_id)->where('type',$request->type)->update($already_like_follow_update);
                } else {
                    $already_like_follow = already_like_follow::where("user_id",$request->user_id)->where('custom_user_id',$request->user_id)->where('type',$request->type)->update($already_like_follow_update);
                }
                return redirect('get_likes/order')->with('success','Order Created Succesfully');
            } else {
                $followImage = $request->image;
                $originalPath = public_path().'/get_likes/instagram/follow/';
                $filename = basename($request->image);
                $followFileName = uniqid().'_follow_order' . '.' .'png';
                $followImage->move($originalPath, $followFileName);
                
                $order = new Order();
                $order->type = $request->type;
                $order->user_id = $request->user_id;
                if(isset($request->custom_user_id))
                {
                    $order->custom_user_id = $request->custom_user_id;
                } else {
                    $order->custom_user_id = $request->user_id;
                }
                $order->username = $request->username;
                $order->needed = $request->needed;
                $order->image_url = $followFileName;
                $order->fromapp = 0;
                $order->save();

                $already_like_follow = new already_like_follow;
                $already_like_follow->type  = $request->type;
                if(isset($request->custom_user_id))
                {
                    $already_like_follow->custom_user_id  = $request->custom_user_id;
                } else {
                    $already_like_follow->custom_user_id  = $request->user_id;
                }
                $already_like_follow->user_id  = $request->user_id;
                $already_like_follow->already_like_follow  = $encode_already_like_user;
                $already_like_follow->save();
                return redirect('get_likes/order')->with('success','Order Created Succesfully');
                // dd($already_like_follow,$request->all());
                // dd("follow order created successfully");
            } 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        return view('get_likes.order.edit',compact("order"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $orderRequest = $request->all();
        if(isset($request->image_url))
        {
            
            $filename = basename($request->image_url);
            $imagename =  (explode(".",$filename));
            if($request->type == "0")
            {
                $originalPath = public_path().'/get_likes/instagram/like/';
                $likeFileName = uniqid().'_like_order' . '.' .'png';
            }
            $request->image_url->move($originalPath, $likeFileName);
            $cimage = $order->image_url;
            
            if(file_exists($originalPath.$cimage)){
                unlink($originalPath.$cimage);
            }
            $orderRequest['image_url']  = $likeFileName;
        } else {
            $appdata = Order::find($id);
            $orderRequest['image_url']  = $order->image_url;
        }
        Order::find($id)->update($orderRequest);
        return redirect("get_likes/order")->with("info","Order Updated Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        return view('get_likes.order.confirmdestroyorder',compact("order"));

        // Order::find($id)->delete();
        // return redirect("order")->with("error","Order Deleted Successfully");
    }

    public function likeOrder()
    {

        $orders =  Order::where("type","0")->orderBy('id','desc')->get();
        return view('get_likes.order.likeorder',compact("orders"));
    }

    

    public function completeOrder()
    {
        $orders =  Order::where("needed","0")->orderBy('id','desc')->get();
        return view('get_likes.order.completeorder',compact("orders"));
    }
    public function allOrder()
    {
        $orders =  Order::orderBy('id','desc')->get();
        return view('get_likes.order.allorder',compact("orders"));
    }
    public function cancelTheOrder(Request $request,$id)
    {

        $addreasonfor_order = array(
            "reason_for_cancelorder" => $request->reason_for_cancelorder
        ); 
        Order::where("id",$id)->update($addreasonfor_order);
        $order = Order::find($id);
        $userDetail = User::where("user_id",$order->user_id)->first();
        $likefollowcoin = $order->needed-$order->recieved;
        if($likefollowcoin > 0)
        {
            $appData = appData::find(1);
            if($order->type == "0")
            {
                $coins = $likefollowcoin*$appData->spend_like_coin;
            }  
            if($userDetail->parent_id == "0") 
            {
                $total_coin = $userDetail->total_coins + $coins;
                $total_coin = array(
                    "total_coins" => $total_coin
                );
                User::where("user_id",$userDetail->user_id)->update($total_coin);
            } else {

                $userDetail = User::where("user_id",$order->user_id)->first();
                $parentuserDetail = User::where("user_id",$userDetail->parent_id)->first();
                $total_coin = $parentuserDetail->total_coins + $coins;
                $total_coin = array(
                    "total_coins" => $total_coin
                );
                User::where("user_id",$userDetail->parent_id)->update($total_coin);
            }
        }
        if(isset($userDetail->ftoken))
        {
            $firebaseToken = array($userDetail->ftoken);
            $data = array(
                "title" => "Instagram Like Follow",
                "description" => $request->reason_for_cancelorder,
            );
            $this->sendNotification($firebaseToken,$data);
        }
        Order::find($id)->delete();
        if($order->type == "0")
        {
            already_like_follow::where('post_id',$order->post_id)->where('type','0')->delete();
        }  
        return redirect('get_likes/order')->with('info','order remove successfully');
    }
    
}


