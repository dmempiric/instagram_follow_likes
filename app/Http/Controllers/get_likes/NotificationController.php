<?php

namespace App\Http\Controllers\get_likes;

use App\Models\get_likes\appData;
use App\Models\get_likes\GetUser;
use Illuminate\Http\Request;
use App\Http\Controllers\get_likes\API\BaseController;

class NotificationController extends BaseController
{
    public function NotificationDaily()
    {
        $appdata = appData::find("1");
        return view('get_likes.notification.daily.index',compact('appdata'));

    }
    public function NotificationDailyEdit($id)
    {
        $appdata = appData::find("1");
        return view('get_likes.notification.daily.edit',compact('appdata'));
    }

    public function NotificationDailyUpdate(Request $request,$id)
    {
        $notificationdata = array(
            "notification_title" => $request->notification_title,
            "notification_message" => $request->notification_message,
            "notification_show" => $request->notification_show,
        );
        appData::where("id",$id)->update($notificationdata);
        return redirect('get_likes/notification-daily');
    }

    public function NotificationPremium()
    {
        return view('get_likes.notification.premium.premiumuser-notification');
    }

    public function NotificationPremiumSend(Request $request)
    {
        if($request->reciever_type == "1")
        {   
            $firebaseToken = GetUser::whereNotNull('ftoken')->where("ftoken","!=","")->where("is_purchase","1")->pluck('ftoken')->all();
        } else if($request->reciever_type == "2") {
            $firebaseToken = GetUser::whereNotNull('ftoken')->where("ftoken","!=","")->where("is_purchase","0")->pluck('ftoken')->all();
        } else if($request->reciever_type == "3") {
            $firebaseToken = GetUser::whereNotNull('ftoken')->where("ftoken","!=","")->pluck('ftoken')->all();
        }
        $title  = $request->title;
        $description  = $request->description;
        $originalPath = 'public/get_likes/notification/';

        if(isset($request->notification_image))
        {
            $filename = basename($request->notification_image);
            $originalPath = 'public/get_likes/notification/';
            $product_image_name = uniqid().'_notitication' . '.' .'png';
            $request->notification_image->move($originalPath, $product_image_name);
            $image = env("APP_URL").$originalPath.$product_image_name;
        } else {
            $product_image_name = "test.png";
        }
        // $image = "https://www.itsolutionstuff.com/frontTheme/images/logo.png";
        if(isset($image))
        {
            $data = array(
                "title" => $title,
                "description" =>  $description,
                "image" =>  $image,
            );
        } else {
            $data = array(
                "title" => $title,
                "description" =>  $description,
            );
        }
        $this->sendNotification($firebaseToken,$data);
        if(file_exists($originalPath.$product_image_name)){
            unlink($originalPath.$product_image_name);
        }
        return back()-> with("success","Notication Send Successfull");



    }
}
