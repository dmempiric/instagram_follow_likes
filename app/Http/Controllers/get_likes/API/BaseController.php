<?php

namespace App\Http\Controllers\get_likes\API;

use App\Models\get_likes\appData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function sendResponse($result,$message)
    {
        $response = [
            "success"   => true,
            'data'      => $result,
            'message'   => $message     
        ];

        return response()->json($response, 200);
    }

    public function sendPlainResponse($message)
    {
        $response = [
            "success"   => true,
            'message'   => $message     
        ];

        return response()->json($response, 200);
    }

    public function sendError($error, $errorMessages = [],$code = 200)
    {
        $response = [
            'success'   =>  false,
            'message'   =>$error    
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errormessages;
        }

        return response()->json($response , $code);
    }

    public function sendNotification($firebaseToken, $detailNotification)
    {
        $appdata = appData::find(1);
        $SERVER_API_KEY = $appdata->google_notification_server_api_key;
  
        if(count($firebaseToken) >= "1")
        {
            if(count($detailNotification) == "3") {
                $data = [
                    "registration_ids" => $firebaseToken,
                    "notification" => [
                        "title" => $detailNotification['title'],
                        "body" => $detailNotification['description'],  
                        "image" => $detailNotification['image'],  
                    ]
                ];
            } else if (count($detailNotification) == "2")
            {
                $data = [
                    "registration_ids" => $firebaseToken,
                    "notification" => [
                        "title" => $detailNotification['title'],
                        "body" => $detailNotification['description'],  
                        "sound" =>  "default",
                    ]
                ];
            }
            
            $dataString = json_encode($data);
            $headers = [
                'Authorization: key=' . $SERVER_API_KEY,
                'Content-Type: application/json',
            ];
        
           $ch = curl_init();
           
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            $response = curl_exec($ch);

            // dd($response);
        }
       
    }


    function getRefreshToken()
    {
        $appdata = appData::find(1);
        $client_id = $appdata->client_id;
        $client_secret = $appdata->client_secret;
        $refresh_token = $appdata->refresh_token;
        
        $post_data = "grant_type=refresh_token&client_id=$client_id&client_secret=$client_secret&refresh_token=$refresh_token";

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://accounts.google.com/o/oauth2/token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POSTFIELDS => $post_data,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) { 			 
            $info = $err;
                $msg = "Not fetched"; 
        } else {  
            $decode_res = json_decode($response);
            return $decode_res;
        }

    }

}
