<?php

namespace App\Http\Controllers\get_likes\API;

use Image; 
use App\Models\get_likes\Order;
use App\Models\get_likes\appData;
use App\Models\get_likes\CoinDetail;
use Illuminate\Support\Str;
use App\Models\get_likes\PurchaseCoin;
use Illuminate\Http\Request;
use App\Models\get_likes\GetUser;
use App\Models\get_likes\already_like_follow;
use App\Http\Controllers\Controller;
use App\Http\Controllers\get_likes\API\BaseController;


class UserOrderController extends BaseController
{
    public function getProfile(Request $request)
    {
        // dd($request->user_id,$request->username,$request->all());
        if(isset($request->user_id) && isset($request->username) && isset($request->fromapp))
        {
            if($request->fromapp != 1)
            {
                return $this->sendError("Argument Required");
            }
            $user = GetUser::where("user_id",$request->user_id)->first();
            if(isset($user->user_id))
            {
                if($user->status == "0")
                {
                    return $this->sendError("Your Account is Suspended");

                } else {
                    if(isset($request->ftoken))
                    {
                        $updateftoken = array(
                            "ftoken" => $request->ftoken
                        );
                        GetUser::where("user_id",$request->user_id)->update($updateftoken);
                    }

                    $success['user_id'] = $request->user_id;
                    $success['username'] = $request->username;
                   
                    $likeOrderArray = array();
                    
                    $childAccounts = GetUser::where('parent_id',$request->user_id)->get();
                    if($user->parent_id != "0"){
                         
                        $childAccounts = GetUser::where('parent_id',$user->parent_id)->orderBy("id","desc")->get();
                        $childarray = array();
                        foreach($childAccounts as $childAccount)
                        {
                            if($childAccount->user_id != $request->user_id)
                            {
                                $childrecordsingle['user_id'] = $childAccount->user_id;
                                $childrecordsingle['user_name'] = $childAccount->username;
                                array_push($childarray,$childrecordsingle); 
                            }
                           
                            $child_orders = Order::where("user_id",$childAccount->user_id)->where("needed","!=","0")->get();
                            
                            foreach($child_orders as $child_order)
                            {
                                $thubnail_url = $child_order->image_url;
                                if($child_order->type  == "0")
                                {
                                    $file = '/public/get_likes/instagram/like/'.$thubnail_url;
                                    $image_path = env('APP_URL').$file;
                                    $likeordersingle['user_id'] = $child_order->user_id; 
                                    $likeordersingle['needed'] = $child_order->needed;
                                    if(isset($child_order->custom_user_id))
                                    {
                                        $likeordersingle['custom_user_id'] = $child_order->custom_user_id; 
                                    } 
                                    $likeordersingle['recieved'] = $child_order->recieved; 
                                    $likeordersingle['image_url'] = $image_path; 
                                    $likeordersingle['order_owner '] = "0"; 
                                    array_push($likeOrderArray,$likeordersingle);
                                }  
                            }
                        }
                         
                        $parentAccountdata = GetUser::where('user_id',$user->parent_id)->first();
                        $ParentAccount['user_id'] = $parentAccountdata['user_id'];
                        $ParentAccount['user_name'] = $parentAccountdata['username'];
                        array_push($childarray,$ParentAccount); 
                        $success['child_list'] = $childarray;
                    } else if(count($childAccounts) > 0) {
                        // $parent['parent_id'] = $parentAccount->user_id;
                        // $parent['name'] = $parentAccount->username;
                        $childarray = array();
                        foreach($childAccounts as $childAccount)
                        {
                            if($childAccount->user_id != $request->user_id)
                            {
                                $childrecordsingle['user_id'] = $childAccount->user_id;
                                $childrecordsingle['user_name'] = $childAccount->username;
                                array_push($childarray,$childrecordsingle); 
                            } 
                            $child_orders = Order::where("user_id",$childAccount->user_id)->where("needed","!=","0")->get();
                            foreach($child_orders as $child_order)
                            {
                                $thubnail_url = $child_order->image_url;
                                if($child_order->type  == "0")
                                {
                                    $file = '/public/get_likes/instagram/like/'.$thubnail_url;
                                    $image_path = env('APP_URL').$file;
                                    $likeordersingle['user_id'] = $child_order->user_id; 
                                    if(isset($child_order->custom_user_id))
                                    {
                                        $likeordersingle['custom_user_id'] = $child_order->custom_user_id; 
                                    }
                                    $likeordersingle['needed'] = $child_order->needed; 
                                    $likeordersingle['recieved'] = $child_order->recieved; 
                                    $likeordersingle['image_url'] = $image_path; 
                                    $likeordersingle['order_owner '] = "0"; 
                                    array_push($likeOrderArray,$likeordersingle);
                                }   
                            }
                        }
                        $success['child_list'] = $childarray;
                    } else {
                        $success['child_list'] = [];
                    }

                    if($user->parent_id == "0")
                    {
                        $success['parent_account'] = []; 
                        $success['total_coins'] = $user->total_coins;
                        $success['referral_code'] = $user->referral_code;
                        $success['is_referred'] = $user->is_referred;

                        $parent_orders = Order::where("user_id",$user->user_id)->where("needed","!=","0")->get();
                            foreach($parent_orders as $parent_order)
                            {
                                $thubnail_url = $parent_order->image_url;
                                if($parent_order->type  == "0")
                                {
                                    $file = '/public/get_likes/instagram/like/'.$thubnail_url;
                                    $image_path = env('APP_URL').$file;
                                    $likeordersingle['user_id'] = $parent_order->user_id; 
                                    if(isset($parent_order->custom_user_id))
                                    {
                                        $likeordersingle['custom_user_id'] = $parent_order->custom_user_id; 
                                    }
                                    $likeordersingle['needed'] = $parent_order->needed; 
                                    $likeordersingle['recieved'] = $parent_order->recieved; 
                                    $likeordersingle['image_url'] = $image_path; 
                                    $likeordersingle['order_owner '] = "1"; 
                                    array_push($likeOrderArray,$likeordersingle);
                                }  
                            }
                    } else {
                        
                        $parentAccountdata = GetUser::where('user_id',$user->parent_id)->first();
                        $ParentAccount['user_id'] = $parentAccountdata['user_id'];
                        $ParentAccount['user_name'] = $parentAccountdata['username'];
                        
                        $success['total_coins'] = $parentAccountdata['total_coins'];
                        $success['referral_code'] = $parentAccountdata['referral_code'];
                        $success['is_referred'] = $parentAccountdata['is_referred'];
                        $success['parent_account'] = $ParentAccount; 
                        $parent_orders = Order::where("user_id",$parentAccountdata['user_id'])->where("needed","!=","0")->get();
                        
                        foreach($parent_orders as $parent_order)
                        {
                            $thubnail_url = $parent_order->image_url;
                            if($parent_order->type  == "0")
                            {
                                $file = '/public/get_likes/instagram/like/'.$thubnail_url;
                                $image_path = env('APP_URL').$file;
                                $likeordersingle['user_id'] = $parent_order->user_id; 
                                if(isset($parent_order->custom_user_id))
                                {
                                    $likeordersingle['custom_user_id'] = $parent_order->custom_user_id; 
                                }
                                $likeordersingle['needed'] = $parent_order->needed; 
                                $likeordersingle['recieved'] = $parent_order->recieved; 
                                $likeordersingle['image_url'] = $image_path; 
                                $likeordersingle['order_owner '] = "1"; 
                                array_push($likeOrderArray,$likeordersingle);
                            }  
                        }
                    }
                    $success['likeorder'] = $likeOrderArray;
                    return $this->sendResponse($success,"User Login Successfully");
                }
            } else {
                $appdata = appData::find(1);
                $referralcode = Str::random(6);
                $user = new GetUser;
                $user->username = $request->username;
                $user->user_id = $request->user_id;
                $user->total_coins = $appdata->default_coin;
                $user->referral_code =  $referralcode;
                $user->fromapp =  1;
                if(isset($request->ftoken))
                {
                    $user->ftoken =  $request->ftoken;
                }
                $user->save();
                $userData = GetUser::where("id",$user->id)->first();
                $success['user_id'] = $userData->user_id;
                $success['username'] = $userData->username;
                $success['total_coins'] = $userData->total_coins;
                $success['referral_code'] = $userData->referral_code;
                $success['is_referred'] = $userData->is_referred;
                $success['likeorder'] = [];
                return $this->sendResponse($success,"User Login Successfully");
            }
        }  else {
            return $this->sendError("Arghument Required");
        }
    }

    public function getProfileParent(Request $request)
    {
        if(isset($request->user_id) && isset($request->username) && isset($request->reference_id) && isset($request->fromapp))
        {
            if($request->fromapp != 1)
            {
                return $this->sendError("Argument Required");
            }
            $user = GetUser::where("user_id",$request->reference_id)->first();
            if(isset($user->user_id)) {
                $likeOrderArray = array();
                $parentuser = GetUser::where("user_id",$request->user_id)->first();
                if(isset($parentuser->user_id)) {
                    $childAccounts = GetUser::where('parent_id',$request->reference_id)->get();
                    if(count($childAccounts) >= 0){
                        
                        // $parent['parent_id'] = $parentAccount->user_id;
                        // $parent['name'] = $parentAccount->username;
                        $childarray = array();
                        foreach($childAccounts as $childAccount)
                        {
                            $childrecordsingle = $childAccount->user_id;
                            array_push($childarray,$childrecordsingle); 
                        }
                    }
                    // dd($request->user_id,$childarray);
                    if(!in_array($request->user_id,$childarray))
                    {
                        return $this->sendError("Your account is already linked with another account");
                    }
                    $updateftoken = array(
                        "ftoken" => $request->ftoken
                    );
                    GetUser::where("user_id",$request->user_id)->update($updateftoken);

                    if($user->parent_id == "0")
                    {
                        $parent_id = $user->user_id; 
                    } else {
                        $parent_id = $user->parent_id; 
                    }

                    $parent_orders = Order::where("user_id",$parent_id)->where("needed","!=","0")->get();
                    foreach($parent_orders as $parent_order)
                    {
                        $thubnail_url = $parent_order->image_url;
                        if($parent_order->type  == "0")
                        {
                            $file = '/public/get_likes/instagram/like/'.$thubnail_url;
                            $image_path = env('APP_URL').$file;
                            $likeordersingle['user_id'] = $parent_order->user_id; 
                            if(isset($parent_order->custom_user_id))
                            {
                                $likeordersingle['custom_user_id'] = $parent_order->custom_user_id; 
                            }
                            $likeordersingle['needed'] = $parent_order->needed; 
                            $likeordersingle['recieved'] = $parent_order->recieved; 
                            $likeordersingle['image_url'] = $image_path; 
                            $likeordersingle['order_owner '] = "1"; 
                            array_push($likeOrderArray,$likeordersingle);
                        }  
                    }

                    $success['user_id'] = $request->user_id;
                    $success['username'] = $request->username;
                    $success['parent_id'] = $parent_id;
                    $success['total_coins'] = $user->total_coins;
                    $success['is_referred'] = $user->is_referred;
                    $success['referral_code'] = $user->referral_code;

                    $childAccounts = GetUser::where('parent_id',$parent_id)->get();
                    if(count($childAccounts) >= 0){
                        
                        // $parent['parent_id'] = $parentAccount->user_id;
                        // $parent['name'] = $parentAccount->username;
                        $childarray = array();
                        $childrecordsingle = array();

                        foreach($childAccounts as $childAccount)
                        {
                            $childrecordsingle['user_id'] = $childAccount->user_id;
                            $childrecordsingle['username'] = $childAccount->username;
                            if($request->user_id == $childAccount->user_id)
                            {
                            } else {
                                array_push($childarray,$childrecordsingle); 
                            }
                            $child_orders = Order::where("user_id",$childAccount->user_id)->where("needed","!=","0")->get();
                            foreach($child_orders as $child_order)
                            {
                                $thubnail_url = $child_order->image_url;
                                if($child_order->type  == "0")
                                {
                                    $file = '/public/get_likes/instagram/like/'.$thubnail_url;
                                    $image_path = env('APP_URL').$file;
                                    $likeordersingle['user_id'] = $child_order->user_id; 
                                    if(isset($child_order->custom_user_id))
                                    {
                                        $likeordersingle['custom_user_id'] = $child_order->custom_user_id; 
                                    }
                                    $likeordersingle['needed'] = $child_order->needed; 
                                    $likeordersingle['recieved'] = $child_order->recieved; 
                                    $likeordersingle['image_url'] = $image_path; 
                                    $likeordersingle['order_owner '] = "0"; 
                                    array_push($likeOrderArray,$likeordersingle);
                                }  
                            }
                        }
                        $success['child_list'] = $childarray;
                    } else {
                        $success['child_list'] = [];
                    }
                    $success['likeorder'] = $likeOrderArray;
                    return $this->sendResponse($success,"User Login Successfully");
                } else { 
                    if($user->parent_id == "0")
                    {
                        $parent_id = $user->user_id; 
                    } else {
                        $parent_id = $user->parent_id; 
                    }

                    $parent_orders = Order::where("user_id",$parent_id)->where("needed","!=","0")->get();
                    foreach($parent_orders as $parent_order)
                    {
                        $thubnail_url = $parent_order->image_url;
                        if($parent_order->type  == "0")
                        {
                            $file = '/public/get_likes/instagram/like/'.$thubnail_url;
                            $image_path = env('APP_URL').$file;
                            $likeordersingle['user_id'] = $parent_order->user_id; 
                            if(isset($parent_order->custom_user_id))
                            {
                                $likeordersingle['custom_user_id'] = $parent_order->custom_user_id; 
                            }
                            $likeordersingle['needed'] = $parent_order->needed; 
                            $likeordersingle['recieved'] = $parent_order->recieved; 
                            $likeordersingle['image_url'] = $image_path; 
                            $likeordersingle['order_owner '] = "1"; 
                            array_push($likeOrderArray,$likeordersingle);
                        } 
                    }

                    $newuser = new GetUser;
                    $newuser->username = $request->username;
                    $newuser->user_id = $request->user_id;
                    $newuser->parent_id = $parent_id;
                    $newuser->fromapp = 1;
                    if(isset($request->ftoken))
                    {
                        $newuser->ftoken = $request->ftoken;
                    }
                    $newuser->save();
                    $success['user_id'] = $request->user_id;
                    $success['username'] = $request->username;
                    $success['parent_id'] = $parent_id;
                    $success['total_coins'] = $user->total_coins;
                    $success['is_referred'] = $user->is_referred;
                    $success['referral_code'] = $user->referral_code;

                    $childAccounts = GetUser::where('parent_id',$parent_id)->get();
                    if(count($childAccounts) >= 0){
                        
                        // $parent['parent_id'] = $parentAccount->user_id;
                        // $parent['name'] = $parentAccount->username;
                        $childarray = array();
                        foreach($childAccounts as $childAccount)
                        {
                            $childrecordsingle['user_id'] = $childAccount->user_id;
                            $childrecordsingle['username'] = $childAccount->username;
                            if($request->user_id == $childAccount->user_id)
                            {
                                
                            } else {
                                array_push($childarray,$childrecordsingle); 
                            }
                            $child_orders = Order::where("user_id",$childAccount->user_id)->where("needed","!=","0")->get();
                            foreach($child_orders as $child_order)
                            {
                                $thubnail_url = $child_order->image_url;
                                if($child_order->type  == "0")
                                {
                                    $file = '/public/get_likes/instagram/like/'.$thubnail_url;
                                    $image_path = env('APP_URL').$file;
                                    $likeordersingle['user_id'] = $child_order->user_id; 
                                    $likeordersingle['needed'] = $child_order->needed; 
                                    $likeordersingle['recieved'] = $child_order->recieved; 
                                    $likeordersingle['image_url'] = $image_path; 
                                    $likeordersingle['order_owner '] = "0"; 
                                    array_push($likeOrderArray,$likeordersingle);
                                }  
                            }
                        }
                        $success['child_list'] = $childarray;
                    } else {
                        $success['child_list'] = [];
                    }

                    return $this->sendResponse($success,"User Login Successfully");
                } 
            } else {
                return $this->sendError("Refernce Account is not valid");

            }
        }  else {
            return $this->sendError("Arghument Required");
        }
    }

    public function Order(Request $request)
    {
        if(isset($request->type) && isset($request->user_id)  && isset($request->needed) &&  isset($request->image_url) && isset($request->fromapp))
        {
            if($request->fromapp != 1)
            {
                return $this->sendError("Argument Required");
            }
            $user = GetUser::where("user_id",$request->user_id)->first();
            if(isset($user->user_id))
            {
                if($user->parent_id == "0")
                {
                    $user_id = $user->user_id;
                    $ftoken = $user->ftoken;
                } elseif($request->user_id != "0"){
                    $user_id = $user->parent_id;
                    $parentUser = GetUser::where("user_id",$user->parent_id)->first();
                    $ftoken  =  $parentUser->ftoken;
                }
                if($user->status == "0")
                {
                    return $this->sendError("Your Account is Suspended");
                } else {
                    if($request->type == "0")
                    {
                        if(empty($request->post_id) && empty($request->short_code))
                        {
                            return $this->sendError("Argument Required");
                        }
                        if(empty($request->post_id))
                        {
                            return $this->sendError("Argument Required");
                        }
                        $appdata = appData::find(1);
                        if(in_array("like",json_decode($appdata->maintenence_mode)))
                        {
                            $spend_like_coin = $appdata->spend_like_coin; 

                            $neededcoinfor_like = $spend_like_coin*$request->needed;
                            if($user->parent_id != "0")
                            {
                                $parent =  GetUser::where("user_id",$user->parent_id)->first();
                                $usercoinbalance = $parent->total_coins;
                            } else {
                                $usercoinbalance = $user->total_coins;
                            }
                            $currentusercoinbalance = $usercoinbalance - $neededcoinfor_like;
                            
                            if($currentusercoinbalance <= 0)
                            {
                                return $this->sendError('Insufficient Balance.');
                            } else {
                               
                                $order = Order::where('post_id',$request->post_id)->where('user_id',$user_id)->where('type',$request->type)->first();
                                
                                if(isset($order->post_id))
                                {
                                    $total_needed = $request->needed + $order->needed; 
                                    $likeorder =  [
                                        "needed" =>$total_needed
                                    ];
                                    Order::where('user_id',$user_id)->where('post_id',$request->post_id)->update($likeorder);
                                    $userdata = [
                                        "total_coins" => $currentusercoinbalance
                                    ];
                                    GetUser::where('user_id',$user_id)->update($userdata);
                                    
                                    $success['user_id'] = $request->user_id;
                                    if(isset($request->custom_user_id))
                                    {
                                        $success['custom_user_id'] = $request->custom_user_id;
                                    }
                                    $success['total_coin'] = $currentusercoinbalance;
                                    // if(isset($ftoken))
                                    // {
                                    //     $firebaseToken = array($ftoken);
                                    //     $data = array(
                                    //         "title" => "Instagram Like Follow",
                                    //         "description" => "Your ".$request->needed." Like Order Successfully Added",
                                    //     );
                                    //     $this->sendNotification($firebaseToken,$data);
                                    // }
                                    return $this->sendResponse($success,"Order Placed Created Successfully");
                                    
                                } else {
                                     
                                    $originalPath = public_path().'/get_likes/instagram/like/';
                                    $filename = basename($request->image_url);
                                    $imagename =  (explode(".",$filename));
                                    $likeFileName = uniqid().'_like_order' . '.' .'png';
                                    Image::make($request->image_url)->save($originalPath.$likeFileName);

                                    $order = new Order;
                                    $order->user_id  = $user_id;
                                    $order->type  = $request->type;
                                    $order->post_id  = $request->post_id;
                                    $order->fromapp  = $request->fromapp;
                                    if($request->custom_user_id > "0")
                                    {
                                        $order->custom_user_id = $request->custom_user_id;
                                    } else {
                                        $order->custom_user_id = $request->user_id;
                                    }
                                    $order->needed  = $request->needed;
                                    $order->short_code  = $request->short_code;
                                    $order->image_url  = $likeFileName;
                                    $order->save();
                                    if(isset($request->custom_user_id))
                                    {
                                        $array_user_id = array($request->user_id,$request->custom_user_id);
                                    } else {
                                        $array_user_id = array($request->user_id);
                                    }

                                    $child_accounts = GetUser::where("parent_id",$user_id)->pluck('user_id')->all();
                                    array_push($child_accounts,$user_id);

                                    if(isset($request->custom_user_id))
                                    {
                                        array_push($child_accounts,$request->custom_user_id);
                                    }  
                                    $encode_already_like_user = json_encode($child_accounts);

                                    $already_like_follow = new already_like_follow;
                                    $already_like_follow->type  = $request->type;
                                    $already_like_follow->post_id  = $request->post_id;
                                    if(isset($request->custom_user_id))
                                    {
                                        $already_like_follow->custom_user_id  = $request->custom_user_id;
                                    } else {
                                        $already_like_follow->custom_user_id  = $request->user_id;
                                    }
                                    $already_like_follow->user_id  = $user_id;
                                    $already_like_follow->already_like_follow  = $encode_already_like_user;
                                    $already_like_follow->save();

                                    $userdata = [
                                        "total_coins" => $currentusercoinbalance
                                    ];
                                    GetUser::where('user_id',$user_id)->update($userdata);
                                    $success['user_id'] = $request->user_id;
                                    if(isset($request->custom_user_id))
                                    {
                                        $success['custom_user_id'] = $request->custom_user_id;
                                    }
                                    $success['total_coin'] = $currentusercoinbalance;
                                    
                                    // if(isset($ftoken))
                                    // {
                                    //     $firebaseToken = array($ftoken);
                                    //     $data = array(
                                    //         "title" => "Instagram Like Follow",
                                    //         "description" => "Your ".$request->needed." Like Order Successfully Added",
                                    //     );
                                
                                    //     $this->sendNotification($firebaseToken,$data);
                                    // }
                                    return $this->sendResponse($success,"Order Placed Successfully");
                                }
                            }   
                        } else {
                            return $this->sendError('Like Currently in Maintenence Mode');
                        }
                    } else if($request->type == "1") {
                        
                        if(empty($request->username))
                        {
                            return $this->sendError("Argument Required");
                        }

                        $appdata = appData::find(1);
                        if(in_array("follow",json_decode($appdata->maintenence_mode)))
                        {
                            $spend_follow_coin = $appdata->spend_follow_coin; 

                            $neededcoinfor_follow = $spend_follow_coin*$request->needed;
                            if($user->parent_id != "0")
                            {
                                $parent =  GetUser::where("user_id",$user->parent_id)->first();
                                $usercoinbalance = $parent->total_coins;
                            } else {
                                $usercoinbalance = $user->total_coins;
                            }
                            $currentusercoinbalance = $usercoinbalance - $neededcoinfor_follow;
                            if($currentusercoinbalance <= 0)
                            {
                                return $this->sendError('insufficent balance.');
                            } else {
                                if(isset($request->custom_user_id) &&  isset($user_id) )
                                {
                                    $order = Order::where('custom_user_id',$request->custom_user_id)->where("type","1")->where('user_id',$user_id)->first();
                                } else if(empty($request->custom_user_id) &&  isset($user_id)) {
                                    // dd($request->user_id,$user_id);
                                    $order = Order::where('user_id',$user_id)->where("type","1")->where('custom_user_id',$request->user_id)->first();
                                }  
                                
                                if(isset($order->user_id))
                                { 
                                    $total_needed = $request->needed + $order->needed; 
                                    $likeorder =  [
                                        "needed" =>$total_needed
                                    ];
                                    if(isset($request->custom_user_id))
                                    {
                                        Order::where('user_id',$user_id)->where('custom_user_id',$request->custom_user_id)->where("type","1")->update($likeorder);
                                    } else {
                                        Order::where('user_id',$user_id)->where('custom_user_id',$request->user_id)->where("type","1")->update($likeorder);
                                    }
                                    $userdata = [
                                        "total_coins" => $currentusercoinbalance
                                    ];
                                    GetUser::where('user_id',$user_id)->update($userdata);
                                    $success['user_id'] = $user_id;
                                    if(isset($request->custom_user_id))
                                    {
                                        $success['custom_user_id'] = $request->custom_user_id;
                                    }
                                    $success['total_coin'] = $currentusercoinbalance;
                                    // if(isset($ftoken))
                                    // {
                                    //     $firebaseToken = array($ftoken);
                                    //     $data = array(
                                    //         "title" => "Instagram Like Follow",
                                    //         "description" => "Your ".$request->needed." Follow Order Successfully Placed",
                                    //     );
                                    //     $this->sendNotification($firebaseToken,$data);
                                    // }
                                    return $this->sendResponse($success,"Order Placed Successfully");
                                } else {
                                    
                                    $originalPath = public_path().'/instagram/follow/';
                                    $filename = basename($request->image_url);
                                    $imagename =  (explode(".",$filename));
                                    $likeFileName = uniqid().'_follow_order' . '.' .'png';
                                    Image::make($request->image_url)->save($originalPath.$likeFileName);

                                    $order = new Order;
                                    $order->user_id  = $user_id;
                                    $order->type  = $request->type;
                                    $order->username  = $request->username;
                                    $order->needed  = $request->needed;
                                    $order->fromapp  = $request->fromapp;
                                    $order->image_url  = $likeFileName;
                                    if($request->custom_user_id > "0")
                                    {
                                        $order->custom_user_id = $request->custom_user_id;
                                    } else {
                                        $order->custom_user_id = $request->user_id;
                                    }
                                    $order->save(); 
                                    $child_accounts = GetUser::where("parent_id",$user_id)->pluck('user_id')->all();
                                    array_push($child_accounts,$user_id);

                                    if(isset($request->custom_user_id))
                                    {
                                        array_push($child_accounts,$request->custom_user_id);
                                    }  
                                    $encode_already_like_user = json_encode($child_accounts);

                                    $already_like_follow = new already_like_follow;
                                    $already_like_follow->type  = $request->type;
                                    $already_like_follow->user_id  = $user_id;
                                    if(isset($request->custom_user_id))
                                    {
                                        $already_like_follow->custom_user_id  = $request->custom_user_id;
                                    } else {
                                        $already_like_follow->custom_user_id  = $request->user_id;
                                    }
                                    $already_like_follow->already_like_follow  = $encode_already_like_user;
                                    $already_like_follow->save();
                                 
                                    $userdata = [
                                        "total_coins" => $currentusercoinbalance
                                    ];
                                    GetUser::where('user_id',$user_id)->update($userdata);
                                    $success['user_id'] = $request->user_id;
                                    if(isset($request->custom_user_id))
                                    {
                                        $success['custom_user_id'] = $request->custom_user_id;
                                    }
                                    $success['total_coin'] = $currentusercoinbalance;
                                    // if(isset($user->ftoken))
                                    // { 
                                    //     $firebaseToken = array($user->ftoken);
                                    //     $data = array(
                                    //         "title" => "Instagram Like Follow",
                                    //         "description" => "Your ".$request->needed." Follow Order Successfully Placed",
                                    //     );
                                    //     $this->sendNotification($firebaseToken,$data);
                                    // }
                                    return $this->sendResponse($success,"Order Placed Successfully");
                                }
                            } 
                        } else {
                            return $this->sendError('Follow Currently in Maitenence Mode');
                        }
                    } else {
                        return $this->sendError("Argument Required");
                    }
                
                }
            } else {
                return $this->sendError("requested User is IN valid");
            }
        }  else {
            return $this->sendError("Argument Required");
        }
    }

    public function fetchPost(Request $request)
    {
        if(isset($request->user_id))
        {
            $user = GetUser::where("user_id",$request->user_id)->first();
            // dd($user->parent_id);
            if(isset($user->status))
            {
                if($user->status == "0")
                {
                    $data['fetch_post'] = false;
                    return $this->sendResponse($data,'Your Account is Suspended');
                } else {
                    if($user->parent_id == "0")
                    {
                        $total_coin = $user->total_coins;
                    }  else if($user->parent_id != "0") {
                        $parentAccount = GetUser::where("user_id",$user->parent_id)->first();
                        $total_coin = $parentAccount->total_coins;
                    }
                    if(isset($request->custom_user_id))
                    {
                        $user_id = $request->custom_user_id;
                    } else {
                        $user_id = $request->owner_id;
                    }
                    if($user->parent_id == "0")
                    {
                        $total_coins = $user->total_coins;
                    } else if($user->parent_id != "0")
                    {
                        $parentUser = GetUser::where("user_id",$user->user_id)->first();
                        $total_coins = $parentUser->total_coins;
                    }
                    if($request->already_like == "true" && isset($request->user_id) && isset($request->owner_id) && isset($request->type))
                    {
                        if($request->type == "0")
                        {
                            $order = Order::where("post_id",$request->post_id)->where("type",$request->type)->first();
                        } else {
                            return $this->sendError('Argument Required');
                        }
                        if(isset($order->user_id))
                        {  
                            $userowner = GetUser::where("user_id",$request->owner_id)->first();
                            if(isset($userowner->user_id))
                            {
                                $no_like_log = $order->no_like_log+1;
                                $nolikelogArray = array(
                                    "no_like_log" => $no_like_log
                                ); 
                                if($request->type == "0")
                                {  
                                    if(isset($request->custom_user_id))
                                    { 
                                        $order = Order::where("post_id",$request->post_id)->where("type",$request->type)->update($nolikelogArray);
                                    } else {
                                        $order = Order::where("post_id",$request->post_id)->where("type",$request->type)->update($nolikelogArray);
                                    }
                                } 

                                if($user->parent_id == "0")
                                {
                                    $total_coins = $user->total_coins;
                                } else {
                                    $parentDetail = GetUser::where("user_id",$user->parent_id)->first();
                                    $total_coins = $parentDetail->total_coins;
                                }
                                if($request->type == "0")
                                {
                                    $already_like_follow = already_like_follow::where('post_id',$request->post_id)->where("custom_user_id",$user_id)->where("type",$request->type)->first();
                                }  
                                // dd($user_id);
                                $already_lf_user = json_decode($already_like_follow->already_like_follow);
                                // dd($already_lf_user);
                                if(!in_array($request->user_id,$already_lf_user))
                                {
                                    array_push($already_lf_user,$request->user_id); 
                                } 
                                
                                $encode_like_user_data = json_encode($already_lf_user);
                                $update_like_already = array(
                                    "already_like_follow" => $encode_like_user_data
                                );

                                if($request->type == "0")
                                {
                                    already_like_follow::where('post_id',$request->post_id)->where("custom_user_id",$user_id)->where("type",$request->type)->update($update_like_already);
                                }  
                                // dd($request->user_id,$total_coin);
                                $data = $this->getRandomPost($request->user_id,$total_coins);
                                if($data['fetch_post'] == false)
                                {
                                    return $this->sendResponse($data,'No Post avaialble yet');
                                } else {
                                    return $this->sendResponse($data,'Success');
                                }
                            }  else {
                                if($order->fromapp == "0")
                                {
                                    $no_like_log = $order->no_like_log+1;
                                    $nolikelogArray = array(
                                        "no_like_log" => $no_like_log
                                    ); 
                                    if($request->type == "0")
                                    {  
                                        if(isset($request->custom_user_id))
                                        { 
                                            $order = Order::where("post_id",$request->post_id)->where("type",$request->type)->update($nolikelogArray);
                                        } else {
                                            $order = Order::where("post_id",$request->post_id)->where("type",$request->type)->update($nolikelogArray);
                                        }
                                    } 
    
                                    if($user->parent_id == "0")
                                    {
                                        $total_coins = $user->total_coins;
                                    } else {
                                        $parentDetail = GetUser::where("user_id",$user->parent_id)->first();
                                        $total_coins = $parentDetail->total_coins;
                                    }
                                    if($request->type == "0")
                                    {
                                        $already_like_follow = already_like_follow::where('post_id',$request->post_id)->where("custom_user_id",$user_id)->where("type",$request->type)->first();
                                    } 
                                    // dd($user_id);
                                    $already_lf_user = json_decode($already_like_follow->already_like_follow);
                                    // dd($already_lf_user);
                                    if(!in_array($request->user_id,$already_lf_user))
                                    {
                                        array_push($already_lf_user,$request->user_id); 
                                    } 
                                    
                                    $encode_like_user_data = json_encode($already_lf_user);
                                    $update_like_already = array(
                                        "already_like_follow" => $encode_like_user_data
                                    );
    
                                    if($request->type == "0")
                                    {
                                        already_like_follow::where('post_id',$request->post_id)->where("custom_user_id",$user_id)->where("type",$request->type)->update($update_like_already);
                                    }  
                                    // dd($request->user_id,$total_coin);
                                    $data = $this->getRandomPost($request->user_id,$total_coins);
                                    if($data['fetch_post'] == false)
                                    {
                                        return $this->sendResponse($data,'No Post avaialble yet');
                                    } else {
                                        return $this->sendResponse($data,'Success');
                                    }
                                } else {
                                    $data['fetch_post'] = false;
                                    return $this->sendResponse($data,'Request user is not Valid');
                                }
                            }
                        } else {
                            $data['fetch_post'] = false;
                            return $this->sendResponse($data,'Post Not Found');
                        }
                    } 
                    
                    else if(isset($request->owner_id) && isset($request->type))
                    {
                        if($request->type == "0")
                        {
                            if(!isset($request->post_id)){
                                $data = $this->getRandomPost($user_id,$total_coin);
                                if($data['fetch_post'] == false)
                                {
                                    return $this->sendResponse($data,'Currently record is not available');
                                } else {
                                    return $this->sendResponse($data,'Success');
                                }
                            }
                            $order = Order::where("post_id",$request->post_id)->where("type",$request->type)->where("custom_user_id",$user_id)->first();
                            // dd($order,"hello");

                        }  
                        if($order == null)
                        {
                            $data = $this->getRandomPost($user_id,$total_coin);
                            if($data['fetch_post'] == false)
                            {
                                return $this->sendResponse($data,'Currently record is not available');
                            } else {
                                return $this->sendResponse($data,'Success');
                            }
                        }

                        if(isset($order->user_id))
                        {
                            $userowner = GetUser::where("user_id",$request->owner_id)->first();
                            if(isset($userowner->user_id))
                            {
                                if($request->type == "0")
                                {
                                    // dd($user_id,$request->post_id,$request->type);
                                    $already_like_follow = already_like_follow::where('custom_user_id',$user_id)->where('post_id',$request->post_id)->where('type',$request->type)->first();
                                }  
                                $already_lf_user = json_decode($already_like_follow->already_like_follow);
                                // dd($request->user_id,$already_lf_user,in_array($request->user_id,$already_lf_user));
                                if(in_array($request->user_id,$already_lf_user))
                                {
                                    $data = $this->getRandomPost($request->user_id,$total_coin);
                                    if($data['fetch_post'] == false)
                                    {
                                        return $this->sendResponse($data,'No Post avaialble yet');
                                    } else {
                                        return $this->sendResponse($data,'Success');
                                    }
                                }else { 
                                    array_push($already_lf_user,$request->user_id); 
                                    $encode_like_user_data = json_encode($already_lf_user);
                                    $update_like_already = array(
                                        "already_like_follow" => $encode_like_user_data
                                    );
                                    if($request->type == "0")
                                    {
                                        already_like_follow::where('post_id',$request->post_id)->where("type",$request->type)->update($update_like_already);
                                        // $data = already_like_follow::where('post_id',$request->post_id)->where("type",$request->type)->get();
                                        
                                    }  
                                    $appdata = appData::find(1);
                                    if($user->parent_id == "0")
                                    {
                                        $total_coin = $user->total_coins;  
                                    } else {
                                        $parent = GetUser::where('user_id',$user->parent_id)->first();
                                        $total_coin = $parent->total_coins;  
                                    }
                                    
                                    if($request->type == "0")
                                    {
                                        $earn_like_coin = $appdata->earn_like_coin; 
                                        $getcoin =  $earn_like_coin+$total_coin;
                                    } 
                                    $totalCoinBalance = array(
                                        "total_coins" => $getcoin
                                    );
                                    if($user->parent_id == "0")
                                    {
                                        GetUser::where('user_id',$user->user_id)->update($totalCoinBalance);
                                    } else {
                                        GetUser::where('user_id',$user->parent_id)->update($totalCoinBalance);
                                    }

                                    $neededRecieved = $order->recieved+1;
                                    $neededCountData = array(
                                        "recieved" => $neededRecieved
                                    );
                                    if($request->type == "0")
                                    {
                                        if(isset($request->custom_user_id))
                                        {
                                            Order::where("user_id",$request->owner_id)->where("post_id",$request->post_id)->where('custom_user_id',$request->custom_user_id)->where("type",$request->type)->update($neededCountData); 
                                        } else {
                                            
                                            Order::where("user_id",$request->owner_id)->where("post_id",$request->post_id)->where('custom_user_id',$request->owner_id)->where("type",$request->type)->update($neededCountData); 
                                        }

                                    }                                    
                                        if($order->needed <= $neededRecieved)
                                        { 
                                            $UpdateNeeded = array(
                                                "needed" => "0",
                                                "recieved" => "0"
                                            );
                                            if($request->type == "0")
                                            {
                                                Order::where("user_id",$request->owner_id)->where("post_id",$request->post_id)->where("type",$request->type)->update($UpdateNeeded);
                                            } 
                                            if(isset($userowner->ftoken))
                                            {
                                                $firebaseToken = array($userowner->ftoken);
                                                $data = array(
                                                    "title" => "Instagram Like Follow",
                                                    "description" => "Your Order Completed Successfully",
                                                );
                                                $this->sendNotification($firebaseToken,$data);
                                            }
                                        }
                                        $data = $this->getRandomPost($request->user_id,$getcoin);
                                        if($data['fetch_post'] == false)
                                        {
                                            return $this->sendResponse($data,'No Post avaialble yet');
                                        } else {
                                            return $this->sendResponse($data,'Success');
                                        }
                                }
                            } else {
                                if($order->fromapp == "0")
                                {
                                    if($request->type == "0")
                                    {
                                        // dd($user_id,$request->post_id,$request->type);
                                        $already_like_follow = already_like_follow::where('custom_user_id',$user_id)->where('post_id',$request->post_id)->where('type',$request->type)->first();
                                    }  
                                    // dd($already_like_follow);
                                    $already_lf_user = json_decode($already_like_follow->already_like_follow);
                                
                                    if(in_array($request->user_id,$already_lf_user))
                                    {
                                        $data = $this->getRandomPost($request->user_id,$total_coin);
                                        if($data['fetch_post'] == false)
                                        {
                                            return $this->sendResponse($data,'No Post avaialble yet');
                                        } else {
                                            return $this->sendResponse($data,'Success');
                                        }
                                    }else { 
                                        array_push($already_lf_user,$request->user_id); 
                                        $encode_like_user_data = json_encode($already_lf_user);
                                        $update_like_already = array(
                                            "already_like_follow" => $encode_like_user_data
                                        );
                                        if($request->type == "0")
                                        {
                                            already_like_follow::where('post_id',$request->post_id)->where("type",$request->type)->update($update_like_already);
                                            // $data = already_like_follow::where('post_id',$request->post_id)->where("type",$request->type)->get();
                                            
                                        }  
                                        $appdata = appData::find(1);
                                        if($user->parent_id == "0")
                                        {
                                            $total_coin = $user->total_coins;  
                                        } else {
                                            $parent = GetUser::where('user_id',$user->parent_id)->first();
                                            $total_coin = $parent->total_coins;  
                                        }
                                        
                                        if($request->type == "0")
                                        {
                                            $earn_like_coin = $appdata->earn_like_coin; 
                                            $getcoin =  $earn_like_coin+$total_coin;
                                        }  
                                        $totalCoinBalance = array(
                                            "total_coins" => $getcoin
                                        );
                                        if($user->parent_id == "0")
                                        {
                                            GetUser::where('user_id',$user->user_id)->update($totalCoinBalance);
                                        } else {
                                            GetUser::where('user_id',$user->parent_id)->update($totalCoinBalance);
                                        }
    
                                        $neededRecieved = $order->recieved+1;
                                        $neededCountData = array(
                                            "recieved" => $neededRecieved
                                        );
                                        if($request->type == "0")
                                        {
                                            if(isset($request->custom_user_id))
                                            {
                                                Order::where("user_id",$request->owner_id)->where("post_id",$request->post_id)->where('custom_user_id',$request->custom_user_id)->where("type",$request->type)->update($neededCountData); 
                                            } else {
                                                
                                                Order::where("user_id",$request->owner_id)->where("post_id",$request->post_id)->where('custom_user_id',$request->owner_id)->where("type",$request->type)->update($neededCountData); 
                                            }
    
                                        }                                     
                                            if($order->needed <= $neededRecieved)
                                            { 
                                                $UpdateNeeded = array(
                                                    "needed" => "0",
                                                    "recieved" => "0"
                                                );
                                                if($request->type == "0")
                                                {
                                                    Order::where("user_id",$request->owner_id)->where("post_id",$request->post_id)->where("type",$request->type)->update($UpdateNeeded);
                                                }  
                                                if(isset($userowner->ftoken))
                                                {
                                                    $firebaseToken = array($userowner->ftoken);
                                                    $data = array(
                                                        "title" => "Instagram Like Follow",
                                                        "description" => "Your Order Completed Successfully",
                                                    );
                                                    $this->sendNotification($firebaseToken,$data);
                                                }
                                            }
                                            $data = $this->getRandomPost($request->user_id,$getcoin);
                                            if($data['fetch_post'] == false)
                                            {
                                                return $this->sendResponse($data,'No Post avaialble yet');
                                            } else {
                                                return $this->sendResponse($data,'Success');
                                            }
                                    }
                                } else {
                                    $data['fetch_post'] = false;
                                    return $this->sendResponse($data,'Request user is not Valid');
                                }
                            }
                        } else {
                            $data['fetch_post'] = false;
                            return $this->sendResponse($data,'Post Not Found');  
                        }
                    } 

                    else if(empty($request->owner_id) && empty($request->type) && empty($request->already_like) && empty($request->post_id))
                    {
                        // dd($user); 
                        $data = $this->getRandomPost($request->user_id,$total_coin);
                        if($data['fetch_post'] == false)
                        {
                            return $this->sendResponse($data,'No Post avaialble yet');
                        } else {
                            return $this->sendResponse($data,'Success');
                        }
                    } else {
                        return $this->sendError("Argument Required");
                    }
                }
            } else {
                return $this->sendError("Requested Account is not Found");
            }
        } else {
            return $this->sendError("Argument Required");
        } 
    }
    
    public function purchaseCoin(Request $request)
    {
        $data = json_decode(file_get_contents('php://input'), true);
        
        // dd($data['payment_method'],$data['user_id']);
       
        
        if(isset($data['user_id']))
        {
            $user = GetUser::where("user_id",$data['user_id'])->first();
            if(isset($user->status))
            {
                if($user->status == "0")
                {
                    return $this->sendError("Your Account is Suspended");
                } else {
                    if($data['from_ad'] == "true")
                    {   
                        $appData = appData::find(1);
                        if($user->parent_id == 0)
                        {
                            $user_id = $user->user_id;
                            $add_total_coin = $user->total_coins+$appData->from_add_coin;
                            $updatedCoin = array("total_coins" => $add_total_coin);
                            GetUser::where('user_id',$user->user_id)->update($updatedCoin);
                        } else {
                            $parentuser = GetUser::where("user_id",$user->parent_id)->first();
                            $add_total_coin = $parentuser->total_coins+$appData->from_add_coin;
                            $updatedCoin = array("total_coins" => $add_total_coin);
                            GetUser::where('user_id',$parentuser->user_id)->update($updatedCoin);
                        }
                        $success['user_id'] = $data['user_id'];
                        $success['total_coins'] = $add_total_coin;
                        return $this->sendResponse($success,"Add Coin Added Successfully");
                    } else if($data['from_ad'] == "false"){
                        $purchased_coin = $data['purchased_coin'];
                        $original_coin = $data['original_coin'];
                        $appdata = appData::find(1);
                        $coinDetail = CoinDetail::where("quantity",$original_coin)->first();
                        if($appdata->offer == "1")
                        {
                            $discountCoins =  $original_coin*$appdata->offer_percentage/100;
                            $discountTotalCoins = $discountCoins+$original_coin;
                            // dd("condition called ",$discountCoins,$original_coin,$appdata->offer_percentage);
                            if($discountTotalCoins != $purchased_coin)
                            {
                                return $this->sendError("Argument Required");
                            } 
                            
                        } else if($appdata->offer == "0"){
                            $discountCoins =  $original_coin*10/100;
                            $discountTotalCoins = $discountCoins+$original_coin;
                            if($discountTotalCoins != $purchased_coin)
                            {
                                return $this->sendError("Argument Required ");
                            } 
                        } else {
                            return $this->sendError("Argument Required");
                        }
                        if(isset($data['payment_method']))
                        {
                            if($data['payment_method'] == "0")
                            {
                                $amount = $coinDetail->other_rate;

                                $transaction_id = $data['response']['id'];
                                $payment_time = $data['response']['create_time'];
                                $country_code = $data['country_code'];
                                
                                if(isset($transaction_id)  && isset($purchased_coin))
                                {
                                    $purchasecoin = new PurchaseCoin;
                                    $purchasecoin->payment_id = $transaction_id;
                                    $purchasecoin->payment_type = "paypal";
                                    $purchasecoin->purchased_coin = $purchased_coin;
                                    if(isset($data['response']['state']))
                                    {
                                        $purchasecoin->payment_state = $request->payment_state;
                                    }
                                    $purchasecoin->amount = $amount;
                                    $purchasecoin->payment_time = $payment_time;
                                    $purchasecoin->country_code = $country_code;
                                    $purchasecoin->payment_method = $data['payment_method'];
                                    $purchasecoin->user_id = $data['user_id'];
                                    $purchasecoin->save();
                                } else {
                                    return $this->sendError("Argument Required");
                                }

                            } else if($data['payment_method'] == "1")
                            {
                                $amount = $coinDetail->indian_rate;
                                if(isset($data['transaction_id']))
                                {
                                    $purchasecoin = new PurchaseCoin;
                                    $purchasecoin->purchased_coin = $purchased_coin;
                                    $purchasecoin->payment_method = $data['payment_method'];
                                    $purchasecoin->transaction_id = $data['transaction_id'];
                                    $purchasecoin->payment_time = date('Y-m-d h:i:s');
                                    $purchasecoin->user_id = $data['user_id'];
                                    $purchasecoin->save();
                                } else {
                                    return $this->sendError("Argument Required");
                                }
                            } 
                            if($user->parent_id == "0")
                            {
                                $total_coins = $user->total_coins+$purchased_coin;
                            
                            } else {
                                $parentuser = GetUser::where('user_id',$user->parent_id)->first();
                                $total_coins = $parentuser->total_coins+$purchased_coin;
                            }
                            $totalcoin = array(
                                "total_coins" => $total_coins,
                                "is_purchase" => "1"
                            );
                            if($user->parent_id == "0")
                            {
                                GetUser::where('user_id',$user->user_id)->update($totalcoin);
                            } else {
                                GetUser::where('user_id',$user->parent_id)->update($totalcoin);
                            }
                            
                            $success['user_id'] = $data['user_id'];
                            $success['total_coins'] = $total_coins;
                            return $this->sendResponse($success,"You Purchased ".$purchased_coin." coins successfully.");
                        } else {
                            return $this->sendError("Argument required");
                        }
                    }
                }
            } else {
                return $this->sendError("Requested Account is not Found");
            }
        } else {
            return $this->sendError("Argument Required");
        } 
    }

    public function refferalCode(Request $request)
    {
        if(isset($request->user_id) && isset($request->refferal_code))
        {
            $user = GetUser::where("user_id",$request->user_id)->first();
            if(isset($user->status))
            {
                if($user->parent_id == "0")
                {
                    if($user->is_referred == "1")
                    {
                        return $this->sendError("You are already reffered User");
                    }
                } else {
                    $parentAccount = GetUser::where("user_id",$user->parent_id)->first();
                    if($user->is_referred == "1")
                    {
                        return $this->sendError("You are already reffered User");
                    }
                }
                if($user->status == "0")
                {
                    return $this->sendError("Your Account is Suspended");
                } else {
                    $checkrefferal_code = GetUser::where("referral_code",$request->refferal_code)->first();

                    if($request->refferal_code == $checkrefferal_code->referral_code)
                    {
                        if($user->parent_id == "0")
                        {
                            $requested_user_refferal_code =  $user->referral_code;
                        } else {
                            $user = GetUser::where("user_id",$user->parent_id)->first();
                            $requested_user_refferal_code = $parentAccount->referral_code;
                        }
                        
                        if($requested_user_refferal_code !== $request->refferal_code) {

                                $appdata = appData::where("id","1")->first();
                                if($user->parent_id == "0")
                                {
                                    $total_coins = $user->total_coins;
                                    $finalCoinAmount = $total_coins+$appdata->referral_coin;
                                    $totalCoins = array(
                                        "total_coins" =>  $finalCoinAmount,
                                        "is_referred" => "1",
                                    ); 
                                    GetUser::where("user_id",$request->user_id)->update($totalCoins);
                                } else {
                                    $parentAccount = GetUser::where("user_id",$user->parent_id)->first();
                                    $total_coins = $parentAccount->total_coins;
                                    $finalCoinAmount = $total_coins+$appdata->referral_coin;
                                    $totalCoins = array(
                                        "total_coins" =>  $finalCoinAmount
                                    ); 
                                    GetUser::where("user_id",$user->parent_id)->update($totalCoins);
                                }
                                $refferal_coinuser = $checkrefferal_code['total_coins']+$appdata->referral_coin;
                                
                                $is_reffereddata = array(
                                    "total_coins" =>  $refferal_coinuser
                                );
                                
                                GetUser::where("user_id",$checkrefferal_code['user_id'])->update($is_reffereddata);
                                
                                $success['total_coins'] = $finalCoinAmount;
                                $success['user_id'] = $request->user_id;
                                return $this->sendResponse($success,"Reffered Sucessfully");
                                
                        } else {
                            return $this->sendError("You can't use your own Refferal code");                            
                        }
                    } else {
                        return $this->sendError("Refferal code is invalid");
                    }
                }
            } else {
                return $this->sendError("Requested Account is not Found");
            }
        } else {
            return $this->sendError("Argument Required");
        } 

    }

    function getRandomPost($userId,$total_coins)
    {
        $appdata = appData::find(1);        
        if(count(json_decode($appdata->maintenence_mode)) == 2)
        {
            $orders = Order::where('needed','!=','0')->inRandomOrder()->get();
        } else {
            if(in_array("like",json_decode($appdata->maintenence_mode)))
            {
                $orders = Order::where('needed','!=','0')->where('type','0')->inRandomOrder()->get();
            } 
        }
        if(count($orders) != 0)
        { 
            foreach($orders as $order)
            {
                $user_id = $order->custom_user_id;
                if($order->type == "0")
                {
                    $already_like_follow = already_like_follow::where('post_id',$order->post_id)->where("type",$order->type)->first();    
                }       
                //  if(isset($order->post_id))
                //  {
                //      dd($already_like_follow,$order->type,$user_id,$order->post_id);
                //  }
                // dd($already_like_follow,$order->type,$user_id);
                $already_likefollow_user = json_decode($already_like_follow->already_like_follow);
            //    dd($userId,$already_likefollow_user,!in_array($userId,$already_likefollow_user)); 
                if(!in_array($userId,$already_likefollow_user))
                {
                    // dd("if condition");
                    $thubnail_url = $order->image_url;
                    if($order->type == "0")
                    {
                        $file = '/public/get_likes/instagram/like/'.$thubnail_url;

                    }  
                    $image_path = env('APP_URL').$file;

                    $success['user_id'] = $order->user_id; 
                    $success['total_coin'] = "$total_coins"; 
                    $success['post_id'] = $order->post_id; 
                    if(isset($order->custom_user_id))
                    {
                        $success['custom_user_id'] = $order->custom_user_id;
                    }
                    $success['type'] = $order->type; 
                    $success['image_url'] = $image_path; 
                    if($order->type == "0")
                    {
                        $success['short_code'] = $order->short_code; 
                    }
                    $success['fetch_post'] = true; 
                    return $success;

                } else {
                    // dd("else condition");
                    $i = 1;
                }
            }
            if($i == 1)
            {
                $success['total_coin'] = "$total_coins"; 
                $success['fetch_post'] = false; 
                return $success;
                // return $this->sendResponse($success,'No Post avaialble yet');
            }
        } else {
            $success['total_coin'] = "$total_coins"; 
            $success['fetch_post'] = false; 
            return $success;
            // return $this->sendResponse($success,'Data not found');
        }

    }
}
 