<?php

namespace App\Http\Controllers\get_likes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\get_likes\CoinDetail;

class CoinDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coindetails = CoinDetail::all();
        return view('get_likes.coin_detail.index',compact('coindetails'));         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('get_likes.coin_detail.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coinDetails = $request->all();
        if($request->is_popular == "on")
        {
            $coinDetails['is_popular'] = "1";
        } else {
            
            $coinDetails['is_popular'] = "0";
        }
        CoinDetail::create($coinDetails);
        return redirect('get_likes/coindetail')->with('success','Coin Detail Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function show(CoinDetail $coinDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(CoinDetail $coindetail)
    {

        return view('get_likes.coin_detail.edit',compact('coindetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CoinDetail $coindetail)
    {
        $coinDetails = $request->all();
        if($request->is_popular == "on")
        {
            $coinDetails['is_popular'] = "1";
        } else {
            
            $coinDetails['is_popular'] = "0";
        }
        $coindetail->update($coinDetails);
        return redirect('get_likes/coindetail')->with('info','Coin Detail Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            CoinDetail::find($id)->delete();
            return redirect('get_likes/coindetail')->with('error','Coin Detail Deleted Successfully');

    }
}
