<?php

namespace App\Http\Controllers\get_likes;

use Illuminate\Http\Request;
use App\Models\get_likes\User;
use App\Models\get_likes\Order;
use App\Models\get_likes\GetUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserLoginController extends Controller
{
    public function UserLogin(Request $request)
    {
        $credentials = $request->except(['_token']);
        // dd($credentials,auth()->attempt($credentials));
        
        if (auth()->attempt($credentials)) {
            
            // return redirect('get_likes/home');
            if($request->email == 'admin_instalike@instagramlike.com')
            {
                // dd("if Conition");
                return redirect('get_likes/home');
            } else {
                // dd("else Conition");
                Auth::logout();
                session()->flash('message', 'Invalid credentials');
                return redirect()->back();
            }
            
        }else{
            session()->flash('message', 'Invalid credentials');
            return redirect()->back();

        }
    }

    public function index()
    {
        $total_users = GetUser::where("id","!=","1")->count();
        $total_like_orders = Order::where("type","0")->count();
        $total_active_user = GetUser::where("id","!=","1")->where("status","1")->count();
        $total_purchase_users = GetUser::where("id","!=","1")->where("is_purchase","1")->count();
        // dd($total_users,$total_like_orders,$total_follow_orders,$total_active_user);
        return view('get_likes.home',compact("total_users","total_like_orders",
        "total_active_user",'total_purchase_users'));
    }

    public function UserLogout()
    { 
        Auth::logout();
        return view('get_likes.auth.login')->with('message', 'Logout Successfully');
    }
}
