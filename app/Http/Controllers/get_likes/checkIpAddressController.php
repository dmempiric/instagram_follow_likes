<?php

namespace App\Http\Controllers\get_likes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\get_likes\CheckIpAddress;

class checkIpAddressController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $CheckIpAddresses = CheckIpAddress::all();
        return view('get_likes.ipaddress.index',compact('CheckIpAddresses'));         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('get_likes.ipaddress.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            CheckIpAddress::create($request->all());
            return redirect('get_likes/ipaddress')->with('success','IP Address Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function show(CoinDetail $coinDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $CheckIpAddress = CheckIpAddress::find($id);
        return view('get_likes.ipaddress.edit',compact('CheckIpAddress'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        CheckIpAddress::find($id)->update($request->all());
        return redirect('get_likes/ipaddress')->with('info','Ip Address Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CoinDetail  $coinDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CheckIpAddress::find($id)->delete();
            return redirect('get_likes/ipaddress')->with('error','Ip Address Deleted Successfully');

    }

    public function iptesting()
    {
        $ipaddresses = CheckIpAddress::where('status',"1")->get();
        $ipaddresarray = array();
        if(count($ipaddresses) > 0)
        {
            foreach($ipaddresses as $ipaddress)
            {
                array_push($ipaddresarray,$ipaddress->ip);
            }
        }
        if(in_array("10.10.10.20",$ipaddresarray))
        {
                dd("stop");
        } else {
            dd("welcome");
        }
        
        dd($ipaddresarray);
    }



}
