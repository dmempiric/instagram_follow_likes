<?php

namespace App\Http\Controllers\get_likes;


use Illuminate\Http\Request;
use App\Models\get_likes\User;
use App\Models\get_likes\Order;
use App\Rules\MatchOldPassword;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_users = User::where("id","!=","1")->count();
        $total_like_orders = Order::where("type","0")->count();
        $total_active_user = User::where("id","!=","1")->where("status","1")->count();
        $total_purchase_users = User::where("id","!=","1")->where("is_purchase","1")->count();
        // dd($total_users,$total_like_orders,$total_follow_orders,$total_active_user);
        return view('get_likes.home',compact("total_users","total_like_orders",
        "total_active_user",'total_purchase_users'));
    }

    public function manage_profile()
    {
         $user = User::find(Auth::user()->id);
         return view('userprofile.manage_profile',compact('user'));
    } 

    public function update_password(Request $request)
    {
        
         $request->validate([
             'current_password' => ['required', new MatchOldPassword],
             'new_password' => ['required'],
             'new_confirm_password' => ['same:new_password'],
         ]);
         User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
         return back()->with('message','Password change successfully.');
     }
 
     public function manageProfile(Request $request)
     {
         $oldprofilepicture = auth()->user()->profile_picture;
         $profilepicture = $request->profile_picture;
         if(isset($request->profile_picture))
         {
             $originalPath = "assets/img/";
             if(file_exists($originalPath.$oldprofilepicture)){
                 unlink($originalPath.$oldprofilepicture);
             }
             $extenstion = $profilepicture->getClientOriginalExtension();
             // $imagename =  (explode(".",$filename));
             $profile_picture_name = "admin".'_'.uniqid().'' . '.' ."$extenstion";
             $profilepicture->move($originalPath, $profile_picture_name);
         } else {
            $profile_picture_name = auth()->user()->profile_picture;
         }    
         
         $userData = array(
             "name" => $request->name,
             "profile_picture" => $profile_picture_name);
         User::where("id",auth()->user()->id)->update($userData);
                     
         $user = Auth::user();
         $user->name = $request->name;
         if(isset($request->profile_picture)) {
             $user->profile_picture = $profile_picture_name;
         }
         $user->save();
         return back()->with('updateprofile','profile updated successfully.');
     }

}
