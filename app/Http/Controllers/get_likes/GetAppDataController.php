<?php

namespace App\Http\Controllers\get_likes;

use App\Models\get_likes\User;
use Illuminate\Http\Request;
use App\Models\get_likes\appData;
use Illuminate\Routing\Controller;

class GetAppDataController extends Controller
{
    public function TestData()
    {
        $appdata = appData::find(1);
        $appdata->setConnection('mysql2');
        return view('get_likes.appdata.index',compact('appdata'));
    }
    public function index()
    {
        $appdata = appData::find(1);
        return view('get_likes.appdata.index',compact('appdata'));

    }

    public function create()
    {
        return view('get_likes.appdata.create');
    }



    public function store(Request $request)
    {
        // $appdata = appData::find(1);
        // $appdata = array(
        //     "user_agent" => $request->user_agent,
        //     "share_url" => $request->share_url,
        //     "offer" => $request->offer,
        //     "offer_percentage" => $request->offer_percentage,
        //     "offer_endtime" => $request->offer_endtime
        // );

        //     appData::where("id","1")->update($appdata);
        //     dd("called");
        $appdata = new appData();
        $appdata->earn_like_coin = $request->earn_like_coin;
        $appdata->spend_like_coin = $request->spend_like_coin;
        $appdata->referral_coin = $request->referral_coin;
        $appdata->default_coin = $request->default_coin;
        $appdata->maintenence_mode   = json_encode($request->maintenence_mode);
        $appdata->maintenence_mode   = json_encode($request->maintenence_mode);
        $appdata->update_mode = $request->update_mode;
        $appdata->update_url = $request->update_url;
        $appdata->payment_methode = json_encode($request->payment_methode);
        $appdata->game_id = $request->game_id;
        $appdata->banner_id = $request->banner_id;
        $appdata->initial_id = $request->initial_id;
        $appdata->save();

    }
    public function edit($id)
    {
        $appdata = appData::find($id);
        return view('get_likes.appdata.edit',compact('appdata'));
    }

    public function show($id)
    {
        $appdata = appData::find($id);
        return view('get_likes.appdata.edit',compact('appdata'));
    }

    public function update(Request $request,$id)
    {
        // $appdata = appData::find($id);
        // $firebaseToken = User::where("is_purchase","1")->where("ftoken","!=","")->whereNotNull('ftoken')->pluck('ftoken')->all();
        
        $appdataImageName = $appdata->offer_discount_image;
        if(isset($request->offer_discount_image))
        {
            $filename = basename($request->offer_discount_image);
            $imagename =  (explode(".",$filename));
            $originalPath = public_path().'/instagram/app_data/';
            $product_image_name = uniqid().'_app_data' . '.' .'png';
            $request->offer_discount_image->move($originalPath, $product_image_name);
            if(file_exists($originalPath.$appdataImageName)){
                unlink($originalPath.$appdataImageName);
            }
        }

        $appDataRequest = $request->all();
        $appDataRequest['offer_discount_image'] = $product_image_name;
        $appDataRequest['payment_methode'] = json_encode($request->payment_methode);
        $appDataRequest['maintenence_mode'] = json_encode($request->maintenence_mode);
        appData::find($id)->update($appDataRequest);
        return redirect()->route('appdata.index')->with('info','App Data updated successfully.');
    }

    public function appDataCoinDetail()
    {
        $appdata = appData::find(1);
        return view('get_likes.appdata.coindetail.index',compact('appdata'));
    }

    public function appDataCoinDetailEdit($id)
    {
        $appdata = appData::find(1);
        return view('get_likes.appdata.coindetail.edit',compact('appdata'));
    }
    public function appDataCoinDetailUpdate(Request $request,$id)
    {
        appData::find($id)->update($request->all());
        return  redirect('get_likes/appdata-coindetail');
    }

    public function appDataMaintenence()
    {
        $appdata = appData::find(1);
        return view('get_likes.appdata.maintenence.index',compact('appdata'));
    }

    public function appDataMaintenenceEdit($id)
    {
        // $payment_methode_blankdata = array(["inapp","paypal"]);
        // $appDataRequest = json_encode($payment_methode_blankdata);
        // dd($appDataRequest);
        $appdata = appData::find($id);
       
        return view('get_likes.appdata.maintenence.edit',compact('appdata'));
    }

    public function appDataMaintenenceUpdate(Request $request,$id)
    {
        $appDataRequest = $request->all();
        // dd($appDataRequest);
        if(empty($request->payment_methode))
        {
            // $payment_methode_blankdata = array(["inapp","paypal"]);
            $payment_methode_blankdata = array();
            $appDataRequest['payment_methode'] = json_encode($payment_methode_blankdata);
        } else {
            $appDataRequest['payment_methode'] = json_encode($request->payment_methode);
        }
        if(empty($request->maintenence_mode))
        {
            // $maintenence_mode_blankdata = array(["like","follow"]);
            $maintenence_mode_blankdata = array();
            $appDataRequest['maintenence_mode'] = json_encode($maintenence_mode_blankdata);
        } else {
            $appDataRequest['maintenence_mode'] = json_encode($request->maintenence_mode);
        }
        // dd($appDataRequest['payment_methode'],$appDataRequest['maintenence_mode']);
        appData::find($id)->update($appDataRequest);
        return  redirect('get_likes/appdata-maintenence');
    }

    public function appDataGame()
    {
        $appdata = appData::find(1);
        return view('get_likes.appdata.game.index',compact('appdata'));
    }

    public function appDataGameEdit($id)
    {
        $appdata = appData::find($id);
        return view('get_likes.appdata.game.edit',compact('appdata'));
    }

    public function appDataGameUpdate(Request $request,$id)
    {
        appData::find($id)->update($request->all());
        return  redirect('get_likes/appdata-game');
    }

    public function appDataOffer()
    {
        $appdata = appData::find(1);
        return view('get_likes.appdata.offer.index',compact('appdata'));
    }

    public function appDataOfferEdit($id)
    {
        $appdata = appData::find($id);
        return view('get_likes.appdata.offer.edit',compact('appdata'));
    }
    
    public function appDataOfferUpdate(Request $request,$id)
    {
        $appData = appData::find(1);
        $updateofferupdate = $request->all();

        if($request->offer == 1)
        {
            if(date('Y-m-d H:i', strtotime($request->offer_starttime)) <= date('Y-m-d H:i'))
            {
                $updateofferupdate['offer'] = 1;
            } else {
                $updateofferupdate['offer'] = 0;
            }
        } else {
            $updateofferupdate['offer'] = 0;
        }
        $updateofferupdate['offer_starttime'] = date('Y-m-d H:i', strtotime($request->offer_starttime));
        $updateofferupdate['offer_endtime'] = date('Y-m-d H:i', strtotime($request->offer_endtime));
        $appdata = appData::find($id);

        $appdataImageName = $appdata->offer_discount_image;
        if(isset($request->offer_discount_image))
        {
            $filename = basename($request->offer_discount_image);
            $imagename =  (explode(".",$filename));
            $originalPath = public_path().'/get_likes/instagram/app_data/';
            $product_image_name = uniqid().'_app_data' . '.' .'png';
            $request->offer_discount_image->move($originalPath, $product_image_name);
            $updateofferupdate['offer_discount_image'] = $product_image_name;
            if(!empty($appdata->offer_discount_image)){
                if(file_exists($originalPath.$appdataImageName)){
                    unlink($originalPath.$appdataImageName);
                }
            }
        }
        appData::find($id)->update($updateofferupdate);
        return  redirect('get_likes/appdata-offer');
    }

    public function removeOfferImage(Request $request)
    {
        
        $appData = appData::find(1);
        
        $originalPath = public_path().'/instagram/app_data/';

        if(file_exists($originalPath.$appData->offer_discount_image)){
            unlink($originalPath.$appData->offer_discount_image);
        }
        $update['offer_discount_image'] = null;
        
        appData::where("id","1")->update($update);
        return "success";    
    }

    public function appDataCredentialTokenNotification()
    {
        $appdata = appData::find(1);
        return view('get_likes.appdata.credential.index',compact('appdata')); 
    }

    public function appDataCredentialTokenNotificationEdit($id)
    {
        $appdata = appData::find(1);
        return view('get_likes.appdata.credential.edit',compact('appdata')); 
    }

    public function appDataCredentialTokenNotificationUpdate(Request $request)
    {
        $appdata = appData::find(1)->update($request->all());
        return  redirect('get_likes/appdata-credential-token-notification');
        // return view('appdata.credential.index',compact('appdata')); 
    }


    public function appDataOther()
    {
        $appdata = appData::find(1);
        return view('get_likes.appdata.other.index',compact('appdata'));
    }

    public function appDataOtherEdit($id)
    {

        $appdata = appData::find($id);
        return view('get_likes.appdata.other.edit',compact('appdata'));
    }

    public function appDataOtherUpdate(Request $request,$id)
    {
        $appdataUpdate = array(
            "playstore_version" => $request->playstore_version,
            "web_login" => $request->web_login,
            "web" => $request->web,
            "user_agent" => $request->user_agent,
            "share_url" => $request->share_url,
            "email" => $request->email,
            "website" => $request->website,
            "privacy_policy" => $request->privacy_policy,
            "facebook" => $request->facebook,
            "instagram" => $request->instagram,
            "twitter" => $request->twitter,
            "telegram" => $request->telegram,
            "rate_dialog" => $request->rate_dialog,
            "share_dialog" => $request->share_dialog,

        );
        appData::where("id",$id)->update($appdataUpdate);
        return  redirect('get_likes/appdata-other');
    }

    

}
