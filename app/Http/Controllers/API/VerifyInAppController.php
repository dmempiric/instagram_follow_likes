<?php

namespace App\Http\Controllers\api;

use DateTime;
use App\Models\User;
use App\Models\appData;
use App\Models\userOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\API\BaseController;

class VerifyInAppController extends BaseController
{
    public function verifyInApp(Request $request)
    {
        $appdata = appData::find(1);
        $data = $this->getRefreshToken();
        $access_token = $appdata->access_token;
        
        $inappResponse =  $request['purchase_data'];
        $purchaseTime = $inappResponse['purchaseTime'];
        $now = date('Y-m-d H:i:s',strtotime($purchaseTime));
        $purchaseToken = $inappResponse['purchaseToken'];
        $productId = $inappResponse['productId'];
        $packageName = $inappResponse['packageName'];
        $user_id = $inappResponse['user_id'];

        // $data = json_decode(file_get_contents('php://input'), true);
        if(isset($user_id))
        {
            $user = User::where("user_id",$user_id)->first();
            if(isset($user->status))
            {
                if($user->status == "0")
                {
                    return $this->sendError("Your Account is Suspended By admin contact to support team");
                } else 
                {
                    if($user->parent_id == 0)
                    {
                        $user_id = $user->user_id;
                        $total_coins = $user->total_coins;
                    } else {
                        $parentaccount = User::where("user_id",$user->parent_id)->first();
                        $user_id = $parentaccount->user_id;
                        $total_coins = $parentaccount->total_coins;
                    }
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        // CURLOPT_URL => "https://www.googleapis.com/androidpublisher/v1.1/applications/$package_name/inapp/$product_id/purchases/$token?access_token=$access_token",
                        CURLOPT_URL => "https://www.googleapis.com/androidpublisher/v3/applications/$packageName/purchases/products/$productId/tokens/$purchaseToken?access_token=$access_token",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_SSL_VERIFYHOST => 0,
                        CURLOPT_SSL_VERIFYPEER => 0,
                    ));  
                    
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl); 
                    $decoderesponse = json_decode($response);
                    if (isset($decoderesponse->error)) {
                        return $this->sendError("Invalid purchase token payment failed");
                        // dd("fail");
                        // $info = $err;
                        // $msg = "unsuccess"; 
                        // return $this->sendError("-----response data------\n".$err."\n-------parameter------\n"."token =>".$purchaseToken."\nproduct_id =>".$productId."\nuser_id =>".$user_id."\nmessage =>".$msg."\ndate =>".date('d-m-y h:s:i')."\n********************END**************************\n");
                    } else { 
                        // dd($inappResponse,$decoderesponse,$decoderesponse->acknowledgementState,date('Y-m-d',strtotime($purchaseTime)));

                        $purchaseorder = new userOrder;
                        $purchaseorder->user_id = $inappResponse['user_id'];
                        $purchaseorder->packageName = $inappResponse['packageName'];
                        $purchaseorder->orderId = $inappResponse['orderId'];
                        $purchaseorder->productId = $inappResponse['productId'];
                        if(isset($inappResponse['purchaseToken']))
                        {
                            $purchaseorder->token = $inappResponse['purchaseToken'];
                        }
                        $purchaseorder->developerPayload = $inappResponse['developerPayload'];
                        $purchaseorder->purchaseToken = $purchaseToken;
                        $purchaseorder->purchaseTime = $inappResponse['purchaseTime'];
                        $purchaseorder->purchaseState = $inappResponse['purchaseState'];
                        $purchaseorder->acknowledgementState = $decoderesponse->acknowledgementState;
                        $purchaseorder->regionCode = $decoderesponse->regionCode;
                        // $purchaseorder->inapp = $verify_data['inapp'];
                        // $purchaseorder->username = $verify_data['username'];
                        // $purchaseorder->code = $verify_data['code'];
                        // $purchaseorder->two_factor_identifier = $verify_data['two_factor_identifier'];
                        $purchaseorder->save();

                        $updated_coin = $total_coins+$inappResponse['productId'];
                        $user_updatecoin = array(
                            "total_coins" => $updated_coin,
                            "is_purchase" => "1"
                        );
                        User::where("user_id",$user_id)->update($user_updatecoin);
                        $success["total_coins"] =  $updated_coin;
                        return $this->sendResponse($success,"You Purchased ".$inappResponse['productId']." hearts successfully.");
                    }
                }  
            }else {
                return $this->sendError("Requested Account is not Found");
            }
        } else {
            return $this->sendError("Argument Required");
        } 
    }
}
