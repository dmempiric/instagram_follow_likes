<?php

namespace App\Http\Controllers\API;

use App\Models\appData;
use App\Models\CoinDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;

class GetAppDataController extends BaseController
{
    public function appData()
    {
        // $appdata = appData::find(1);
        // if($appdata->offer == "1")
        // {
        //     $offer_end = date('Y-m-d h:i:s',strtotime($appdata->offer_endtime)-19800);
        //     $current_time = date('Y-m-d h:i:s');
        //     if($offer_end < $current_time)
        //     {
        //         $offerData = array(
        //             "offer" => "0"
        //         );
        //         appdata::where("id","1")->update($offerData);
        //     }   
        // }
        $appdata = appData::find(1);

        $success['earn_like_coin'] = $appdata['earn_like_coin'];
        $success['spent_like_coin']  = $appdata['spend_like_coin'];
        $success['earn_follow_coin'] = $appdata['earn_follow_coin'];
        $success['spent_follow_coin'] = $appdata['spend_follow_coin'];
        $success['referrel_coin'] = $appdata['referral_coin'];
        $success['default_coin'] = $appdata['default_coin'];
        $success['maintenence_mode'] = json_decode($appdata['maintenence_mode']);
        $success['playstore_version'] = $appdata['playstore_version'];
        $success['update_mode'] = $appdata['update_mode'];
        $success['update_url'] = $appdata['update_url'];
        $success['payment_method'] = json_decode($appdata['payment_methode']);
        $success['game_id'] = $appdata['game_id'];
        $success['banner_id'] = $appdata['banner_id'];
        $success['initial_id'] = $appdata['initial_id'];
        $success['maintenence_message'] = $appdata['maintenence_message'];
        $success['notification_title'] = $appdata['notification_title'];
        $success['notification_message'] = $appdata['notification_message'];
        $success['notification_show'] = $appdata['notification_show'];
        $success['update_message'] = $appdata['update_message'];
        $success['user_agent'] = $appdata['user_agent'];
        $success['share_url'] = $appdata['share_url'];
        $success['web'] = $appdata['web'];
        $success['website'] = $appdata['website'];
        $success['offer'] = $appdata['offer'];
        $success['offer_percentage'] = $appdata['offer_percentage'];
        


        $success['offer_starttime'] = strtotime($appdata['offer_starttime']);
        $success['offer_endtime'] = strtotime($appdata['offer_endtime']);
        $success['offer_discount_text'] = $appdata['offer_discount_text'];
            $file = '/public/instagram/app_data/'.$appdata['offer_discount_image'];
            $image_path = env('APP_URL').$file;
        $success['offer_discount_image'] = $image_path;
        $success['web_login'] = $appdata['web_login'];
        $success['email'] = $appdata['email'];
        $success['privacy_policy'] = $appdata['privacy_policy'];
        $success['facebook'] = $appdata['facebook'];
        $success['instagram'] = $appdata['instagram'];
        $success['twitter'] = $appdata['twitter'];
        $success['telegram'] = $appdata['telegram'];
        $success['rate_dialog'] = $appdata['rate_dialog'];
        $success['share_dialog'] = $appdata['share_dialog'];

        
       $coindetails = CoinDetail::where('coin_status',"1")->orderBy('quantity','asc')->get();
        $coindetailarray = array();
        foreach($coindetails as $coindetail)
        {
            $coindetailsingle['quantity'] = $coindetail->quantity;
            $coindetailsingle['indian_rate'] = $coindetail->indian_rate*100;
            $coindetailsingle['other_rate'] = $coindetail->other_rate;
            $coindetailsingle['notes'] = $coindetail->notes;
            $coindetailsingle['is_popular'] = $coindetail->is_popular;
            array_push($coindetailarray,$coindetailsingle); 
        }
        $success['coin_detail'] = $coindetailarray;

        return $this->sendResponse($success, 'App Data List');

    }
}
