<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function sendResponse($result,$message)
    {
        $response = [
            "success"   => true,
            'data'      => $result,
            'message'   => $message     
        ];

        return response()->json($response, 200);
    }

    public function sendPlainResponse($message)
    {
        $response = [
            "success"   => true,
            'message'   => $message     
        ];

        return response()->json($response, 200);
    }

    public function sendError($error, $errorMessages = [],$code = 200)
    {
        $response = [
            'success'   =>  false,
            'message'   =>$error    
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errormessages;
        }

        return response()->json($response , $code);
    }

    public function sendNotification($firebaseToken, $detailNotification)
    {
        // dd($firebaseToken, $detailNotification);
        // $firebaseToken = User::whereNotNull('device_token')->pluck('device_token')->all();
        // $firebaseToken = array("dK97QkG5R_qH3WErI4Nmw6:APA91bH1YYHAu3CGSYwPg63yEoAvulpX_zWmnBQ56xzm24sAgdveQ9Y-kXhZjhrYeB36P0mGvjIf7hmMnSYuae8JbEv9MiC8xGiAz5lMvDj4gOy2DQ2yX-_hThb6ocr57M-B9VwgveUZ","etTjkgV9SuC4-jY_I9K9Sn:APA91bHCWYMaCho--8lIPLCIsfUXI5CUe4OJDQVW3TT0d5mMP1Gv8crYbfrvbbTVzZ9fKaFvfyPKrk0rVvted97ZR-hFEERbnhSnBxmYyK0M_NK1_rQfbN4gjInNYyvU6TbWUXKF10791221231");
        $SERVER_API_KEY = "AAAAisqJu_Y:APA91bHPUEIGk7fGrfO-ZsqvqQRgk4itAIdfDVtnyZoSTi9WMRKz5omjxCfx1Loi1mkI-3Z72NnZd4EUEak8m_WWpLkHL9V2hoFbW-3X7VB4YOQ4dXm41Lkvbnsxw8DwSEdI9U1U0lfL";
  
        if(count($firebaseToken) >= "1")
        {
            if(count($detailNotification) == "3") {
                $data = [
                    "registration_ids" => $firebaseToken,
                    "notification" => [
                        "title" => $detailNotification['title'],
                        "body" => $detailNotification['description'],  
                        "image" => $detailNotification['image'],  
                    ]
                ];
            } else if (count($detailNotification) == "2")
            {
                $data = [
                    "registration_ids" => $firebaseToken,
                    "notification" => [
                        "title" => $detailNotification['title'],
                        "body" => $detailNotification['description'],  
                        "sound" =>  "default",
                    ]
                ];
            }
            
            $dataString = json_encode($data);
            $headers = [
                'Authorization: key=' . $SERVER_API_KEY,
                'Content-Type: application/json',
            ];
        
           $ch = curl_init();
           
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            $response = curl_exec($ch);

            // dd($response);
        }
       
    }


    function getRefreshToken()
    {

        $client_id = "896862870299-doc0jvrlip4t191ept9n13pue7gl9g11.apps.googleusercontent.com"; 
        $client_secret = "bwvsYiuo4rC5Db3Aq98uy_Ux";
        $refresh_token = "1//0g4p9vDBrzUxKCgYIARAAGBASNwF-L9Irc4epGbUvVR0sAK4eziRxXnQcYsg2Xm-wgNIFFDho4pZyv7rNbmhWmFTnM4BRnUvizck";

        $post_data = "grant_type=refresh_token&client_id=$client_id&client_secret=$client_secret&refresh_token=$refresh_token";

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://accounts.google.com/o/oauth2/token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POSTFIELDS => $post_data,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) { 			 
            $info = $err;
                $msg = "Not fetched"; 
        } else {  
            $decode_res = json_decode($response);
            return $decode_res;
        }

    }

}
