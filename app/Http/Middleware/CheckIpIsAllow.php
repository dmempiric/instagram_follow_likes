<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\CheckIpAddress;
use App\Http\Controllers\API\BaseController;

class CheckIpIsAllow extends BaseController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $ipaddresses = CheckIpAddress::where("status","1")->get();
        $ipaddresarray = array();
        if(count($ipaddresses) > 0)
        {
            foreach($ipaddresses as $ipaddress)
            {
                array_push($ipaddresarray,$ipaddress->ip);
            }
        }
        $clientIP = \Request::ip();
        if(in_array($clientIP,$ipaddresarray))
        {
            return $this->sendError("Your Account is Suspended");
        } else {
            return $next($request);
        }
    }
}
