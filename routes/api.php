<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['checkipsallow'])->group(function () {
    Route::get('appdata','API\GetAppDataController@appData');
    Route::post('getprofile','API\UserOrderController@getProfile');
    Route::post('getprofileparent','API\UserOrderController@getProfileParent');
    Route::post('order','API\UserOrderController@Order');
    Route::post('fetchpost','API\UserOrderController@fetchPost');
    Route::post('purchasecoin','API\UserOrderController@purchaseCoin');
    Route::post('refferalcode','API\UserOrderController@refferalCode');
    Route::post('verifyinapp','API\VerifyInAppController@verifyInApp');
});

Route::group(['prefix' => 'get_likes','namespace' => 'get_likes'], function() {
    Route::get('appdata','API\GetAppDataController@appData');
    Route::post('getprofile','API\UserOrderController@getProfile');
    Route::post('getprofileparent','API\UserOrderController@getProfileParent');
    Route::post('order','API\UserOrderController@Order');
    Route::post('fetchpost','API\UserOrderController@fetchPost');
    Route::post('purchasecoin','API\UserOrderController@purchaseCoin');
    Route::post('refferalcode','API\UserOrderController@refferalCode');
    Route::post('verifyinapp','API\VerifyInAppController@verifyInApp');
});