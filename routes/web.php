<?php

use App\Models\appData;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GetAppDataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
/this is our route file
*/

Route::get('/', function () {
    return redirect('admin/login');
});
Route::view('admin/login','auth.login')->name('adminlogin');
Auth::routes();

Route::middleware(['auth','checkipsallow'])->group(function () {
    
    // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
    Route::get('home','UserProfileController@index');
    Route::resource('appdata','GetAppDataController');
    Route::resource('coindetail','CoinDetailController');
    
    Route::get('manage_profile','UserProfileController@manage_profile');
    Route::post('update_password','UserProfileController@update_password');
    Route::post('manageprofile','UserProfileController@manageProfile');

    Route::resource('order','OrderController');
    Route::post('canceltheorder/{id}','OrderController@cancelTheOrder');

    Route::resource('ipaddress','checkIpAddressController');

    Route::get('appdata-coindetail','GetAppDataController@appDataCoinDetail');
    Route::get('appdata-coindetail/{id}','GetAppDataController@appDataCoinDetailEdit');
    Route::post('appdata-coindetailupdate/{id}','GetAppDataController@appDataCoinDetailUpdate');

    Route::get('appdata-maintenence','GetAppDataController@appDataMaintenence');
    Route::get('appdata-maintenence/{id}','GetAppDataController@appDataMaintenenceEdit');
    Route::post('appdata-maintenenceupdate/{id}','GetAppDataController@appDataMaintenenceUpdate');

    Route::get('appdata-game','GetAppDataController@appDataGame');
    Route::get('appdata-game/{id}','GetAppDataController@appDataGameEdit');
    Route::post('appdata-gameupdate/{id}','GetAppDataController@appDataGameUpdate');
    
    Route::get('notification-daily','NotificationController@NotificationDaily');
    Route::get('notification-daily/{id}','NotificationController@NotificationDailyEdit');
    Route::post('notification-dailyupdate/{id}','NotificationController@NotificationDailyUpdate');


    Route::get('notification-premium','NotificationController@NotificationPremium');
    Route::post('notification-premium','NotificationController@NotificationPremiumSend');

    Route::get('appdata-offer','GetAppDataController@appDataOffer');
    Route::get('appdata-offer/{id}','GetAppDataController@appDataOfferEdit');
    Route::post('appdata-offerupdate/{id}','GetAppDataController@appDataOfferUpdate');
    Route::post('removeofferimage','GetAppDataController@removeOfferImage');
    //thnis is just a check how it actualy work for us 
    Route::get('appdata-other','GetAppDataController@appDataOther');
    Route::get('appdata-other/{id}','GetAppDataController@appDataOtherEdit');
    Route::post('appdata-otherupdate/{id}','GetAppDataController@appDataOtherUpdate');

    Route::get('orderlike','OrderController@likeOrder');
    Route::get('orderfollow','OrderController@followOrder');
    Route::get('completeorder','OrderController@completeOrder');
    Route::get('allorder','OrderController@allOrder');

    Route::get('purchaseuser-list','UserController@purchaseUserList');
    Route::get('userlist','UserController@userList');
    Route::get('user/create','UserController@userCreate');
    Route::post('user/create','UserController@userCreates');
    Route::post('search-userlist','UserController@SearchUserList');
    Route::post('search-premiumuserlist','UserController@SearchPremiumUserList');

    Route::get('useredit/{id}','UserController@userEdit');
    Route::post('userupdate/{id}','UserController@userUpdate');
    Route::get('userdelete/{id}','UserController@userDelete');

    Route::get('purchasecoin-list','UserController@purchaseCoinList');
    Route::get('inappPurchase-list','UserController@inAppPurchaseList');
    Route::get('inapp_purchasedelete/{id}','UserController@inAppPurchaseDelete');

    Route::get('purchasecoinedit/{id}','UserController@purchaseCoinEdit');
    Route::post('purchasecoinupdate/{id}','UserController@purchaseCoinUpdate');
    Route::get('purchasecoindelete/{id}','UserController@purchaseCoinDelete');
        
    Route::get('iptesting','checkIpAddressController@iptesting')->middleware("checkipsallow");
});

Route::group(['prefix' => 'get_likes','namespace' => 'get_likes'], function() {

    Route::get('admin/login', function () {
        return view('get_likes.auth.login');
    });

    Route::get('login', function () {
        return view('get_likes.auth.login');
    });
    Route::post('login','UserLoginController@UserLogin');
    Route::get('logout','UserLoginController@UserLogout');
    Route::middleware(['auth','checkipsallow'])->group(function () {
            
            Route::get('home','UserLoginController@index');
            Route::get('/','UserLoginController@index');
            
            Route::resource('appdata','GetAppDataController');
            Route::resource('coindetail','CoinDetailController');
            
            Route::get('manage_profile','HomeController@manage_profile');
            Route::post('update_password','HomeController@update_password');
            Route::post('manageprofile','HomeController@manageProfile');
        
            Route::resource('order','OrderController');
            Route::post('canceltheorder/{id}','OrderController@cancelTheOrder');
        
            Route::resource('ipaddress','checkIpAddressController');
        
            Route::get('appdata-coindetail','GetAppDataController@appDataCoinDetail');
            Route::get('appdata-coindetail/{id}','GetAppDataController@appDataCoinDetailEdit');
            Route::post('appdata-coindetailupdate/{id}','GetAppDataController@appDataCoinDetailUpdate');
        
            Route::get('appdata-maintenence','GetAppDataController@appDataMaintenence');
            Route::get('appdata-maintenence/{id}','GetAppDataController@appDataMaintenenceEdit');
            Route::post('appdata-maintenenceupdate/{id}','GetAppDataController@appDataMaintenenceUpdate');
        
            Route::get('appdata-game','GetAppDataController@appDataGame');
            Route::get('appdata-game/{id}','GetAppDataController@appDataGameEdit');
            Route::post('appdata-gameupdate/{id}','GetAppDataController@appDataGameUpdate');
            
            Route::get('notification-daily','NotificationController@NotificationDaily');
            Route::get('notification-daily/{id}','NotificationController@NotificationDailyEdit');
            Route::post('notification-dailyupdate/{id}','NotificationController@NotificationDailyUpdate');
        
        
            Route::get('notification-premium','NotificationController@NotificationPremium');
            Route::post('notification-premium','NotificationController@NotificationPremiumSend');
        
            Route::get('appdata-offer','GetAppDataController@appDataOffer');
            Route::get('appdata-offer/{id}','GetAppDataController@appDataOfferEdit');
            Route::post('appdata-offerupdate/{id}','GetAppDataController@appDataOfferUpdate');
            Route::post('removeofferimage','GetAppDataController@removeOfferImage');
        //thnis is just a check how it actualy work for us 
            Route::get('appdata-other','GetAppDataController@appDataOther');
            Route::get('appdata-other/{id}','GetAppDataController@appDataOtherEdit');
            Route::post('appdata-otherupdate/{id}','GetAppDataController@appDataOtherUpdate');
            
            Route::get('appdata-credential-token-notification','GetAppDataController@appDataCredentialTokenNotification');
            Route::get('appdata-credential-token-notification/{id}','GetAppDataController@appDataCredentialTokenNotificationEdit');
            Route::post('appdata-credential-token-notification','GetAppDataController@appDataCredentialTokenNotificationUpdate');
        
            Route::get('orderlike','OrderController@likeOrder');
            
            Route::get('completeorder','OrderController@completeOrder');
            Route::get('allorder','OrderController@allOrder');
        
            Route::get('purchaseuser-list','UserController@purchaseUserList');
            Route::get('userlist','UserController@userList');
            Route::get('user/create','UserController@userCreate');
            Route::post('user/create','UserController@userCreates');
            Route::post('search-userlist','UserController@SearchUserList');
            Route::post('search-premiumuserlist','UserController@SearchPremiumUserList');
        
            Route::get('useredit/{id}','UserController@userEdit');
            Route::post('userupdate/{id}','UserController@userUpdate');
            Route::get('userdelete/{id}','UserController@userDelete');
        
            Route::get('purchasecoin-list','UserController@purchaseCoinList');
            Route::get('inappPurchase-list','UserController@inAppPurchaseList');
            Route::get('inapp_purchasedelete/{id}','UserController@inAppPurchaseDelete');
        
            Route::get('purchasecoinedit/{id}','UserController@purchaseCoinEdit');
            Route::post('purchasecoinupdate/{id}','UserController@purchaseCoinUpdate');
            Route::get('purchasecoindelete/{id}','UserController@purchaseCoinDelete');
                
            Route::get('iptesting','checkIpAddressController@iptesting')->middleware("checkipsallow");
    });
    
});

