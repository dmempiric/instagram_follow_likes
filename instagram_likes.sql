-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2021 at 02:17 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `instagram_likes`
--

-- --------------------------------------------------------

--
-- Table structure for table `already_like_follows`
--

CREATE TABLE `already_like_follows` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `post_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `already_like_follow` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `already_like_follows`
--

INSERT INTO `already_like_follows` (`id`, `type`, `user_id`, `custom_user_id`, `post_id`, `already_like_follow`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '0', '123456', '123456', '2106595947853066970122121', '[\"147147\",\"123456\",\"123123\"]', NULL, '2021-04-14 01:21:30', '2021-04-14 01:23:30'),
(2, '0', '123456', '123456', '16219586196519651', '[\"147147\",\"123456\",\"123123\"]', NULL, '2021-04-22 23:17:17', '2021-04-22 23:39:33'),
(3, '0', '123456', '123456', '21065959478530669701221211212121', '[\"147147\",\"123456\",\"123123\"]', NULL, '2021-04-22 23:36:17', '2021-04-22 23:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `app_data`
--

CREATE TABLE `app_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `access_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `earn_like_coin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spend_like_coin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_coin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_coin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_add_coin` int(150) DEFAULT NULL,
  `maintenence_mode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_mode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_methode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `game_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `initial_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maintenence_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_show` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `playstore_version` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `use_second_user_agent` int(11) NOT NULL DEFAULT 1,
  `second_user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `share_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '0 = offer end  / 1 = offer start',
  `offer_percentage` varchar(244) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '	this value is for discount on total price	',
  `offer_starttime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer_endtime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `offer_discount_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `offer_discount_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer_discount_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `privacy_policy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telegram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refresh_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_notification_server_api_key` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate_dialog` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `share_dialog` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_data`
--

INSERT INTO `app_data` (`id`, `access_token`, `earn_like_coin`, `spend_like_coin`, `referral_coin`, `default_coin`, `from_add_coin`, `maintenence_mode`, `update_mode`, `update_url`, `payment_methode`, `game_id`, `banner_id`, `initial_id`, `maintenence_message`, `notification_title`, `notification_message`, `notification_show`, `update_message`, `playstore_version`, `web`, `website`, `user_agent`, `use_second_user_agent`, `second_user_agent`, `share_url`, `offer`, `offer_percentage`, `offer_starttime`, `offer_endtime`, `deleted_at`, `created_at`, `offer_discount_title`, `updated_at`, `offer_discount_text`, `offer_discount_image`, `web_login`, `email`, `privacy_policy`, `facebook`, `instagram`, `twitter`, `telegram`, `client_id`, `client_secret`, `refresh_token`, `google_notification_server_api_key`, `rate_dialog`, `share_dialog`) VALUES
(1, 'ya29.a0AfH6SMDTXgJA17ZqAT3qX7omhE7mVtBu3QUnKIsYDgjBeaUGRnDhTjmQG18p2XuNNLB5953icYjMRMoyWbBJCIvJZuUwdzz2iub93_VD9Tz_4HVHi3F7aBZE0OOW3xZfpyIDWp_Py__IfzRk4yFxkdYv4dQ59j4', '801', '103', '1003', '104', 145, '[\"like\"]', 'none', 'https://profilepromoter.com/', '[\"inapp\",\"paypal\"]', '111111111123', '12344444444456', '12312312334343789', 'Our App is in currently in Maintence Mode.12', 'Hello sir', 'today we have an speacial oiffer for all the thing that we n eed to actually need to buy', '1', 'update is required123', '52', '1', 'https://profilepromoter.com/', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 123.1.0.26.115 (iPhone11,8; iOS 13_3; en_US; en-US; scale=2.00; 828x1792; 190542906)12', 0, NULL, 'SHARE URL112', '1', '20', '2021-04-01 02:00', '2021-05-19 05:00', NULL, '2020-12-03 06:40:58', 'Offer title 123', '2021-04-23 03:05:21', 'this is discount for you', '6081664715492_app_data.png', '1', 'ptowifihelp@gmail.com', 'https://tiklikesforlikes.com/privacy-policy', 'likeforfast', 'likeforfast', 'twiller', 'telegram', '896862870299-doc0jvrlip4t191ept9n13pue7gl9g11.apps.googleusercontent.com', 'bwvsYiuo4rC5Db3Aq98uy_Ux', '1//0g4p9vDBrzUxKCgYIARAAGBASNwF-L9Irc4epGbUvVR0sAK4eziRxXnQcYsg2Xm-wgNIFFDho4pZyv7rNbmhWmFTnM4BRnUvizck', 'AAAAisqJu_Y:APA91bHPUEIGk7fGrfO-ZsqvqQRgk4itAIdfDVtnyZoSTi9WMRKz5omjxCfx1Loi1mkI-3Z72NnZd4EUEak8m_WWpLkHL9V2hoFbW-3X7VB4YOQ4dXm41Lkvbnsxw8DwSEdI9U1U0lfL', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `check_ip_addresses`
--

CREATE TABLE `check_ip_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 = active / 0 = deactive',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `check_ip_addresses`
--

INSERT INTO `check_ip_addresses` (`id`, `ip`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '10.140.10.10', '1', '2020-12-31 07:42:17', '2020-12-07 07:35:28', '2020-12-31 07:42:17'),
(2, '10.10.10.20', '1', '2020-12-31 07:42:15', '2020-12-07 07:42:58', '2020-12-31 07:42:15'),
(3, '150.107.232.3', '1', '2020-12-29 09:45:33', '2020-12-07 22:14:28', '2020-12-29 09:45:33'),
(4, '103.251.19.101', '0', NULL, '2021-01-04 06:39:38', '2021-01-04 06:40:18'),
(5, '42.108.205.130', '1', '2021-01-05 07:11:42', '2021-01-05 07:07:34', '2021-01-05 07:11:42'),
(6, '125.236.250.214', '0', '2021-04-23 03:35:43', '2021-04-23 03:35:09', '2021-04-23 03:35:43');

-- --------------------------------------------------------

--
-- Table structure for table `coin_details`
--

CREATE TABLE `coin_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `indian_rate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_rate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_popular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `coin_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 = active / 0 = deactive',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coin_details`
--

INSERT INTO `coin_details` (`id`, `quantity`, `indian_rate`, `other_rate`, `notes`, `is_popular`, `coin_status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 5000, '1', '12', '5000', '1', '1', NULL, '2020-12-04 01:20:22', '2021-01-19 23:36:00'),
(2, 100000, '2500', '12', '100000', '0', '1', NULL, '2020-12-04 01:25:16', '2020-12-29 09:40:40'),
(3, 1000, '2.55', '12', 'Starter', '0', '1', '2020-12-04 01:28:27', '2020-12-04 01:25:57', '2020-12-04 01:28:27'),
(4, 130000, '15001', '201', '130000', '0', '1', NULL, '2020-12-18 01:14:32', '2020-12-29 09:40:51'),
(5, 1, '1', '20', 'this is just for trearing', '0', '0', '2021-01-19 23:27:29', '2021-01-19 23:26:45', '2021-01-19 23:27:29'),
(6, 232, '1500', '5', 'this is offer', '0', '0', '2021-01-19 23:28:05', '2021-01-19 23:27:45', '2021-01-19 23:28:05'),
(7, 7000, '2500', '25', 'thjid is best  ploan', '1', '1', '2021-01-20 21:49:58', '2021-01-20 21:49:05', '2021-01-20 21:49:58'),
(8, 1200, '12012', '36912', 'hello sir1212', '1', '1', '2021-04-23 01:49:17', '2021-04-23 01:45:10', '2021-04-23 01:49:17');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `get_users`
--

CREATE TABLE `get_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `ftoken` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_coins` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_purchase` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0 = NOT PURCHASED / 1 PURCHASE',
  `is_referred` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0 = NOt REFFERED  / 1 REFFERED ',
  `fromapp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '0 = web / 1 = app',
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 = aCIVE / 0 = iNaCTIVE',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `get_users`
--

INSERT INTO `get_users` (`id`, `name`, `email`, `email_verified_at`, `password`, `user_id`, `parent_id`, `ftoken`, `username`, `profile_picture`, `total_coins`, `referral_code`, `is_purchase`, `is_referred`, `fromapp`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Instagram likes', 'admin_instalike@instagramlike.com', NULL, '$2y$10$uIkXMAU3tm8sLwo4aPWIje313IUjM1JTb/zxCeigjrIS.VCVnrFrO', '', '0', '', '', 'admin_60050018d33fa.jpg', '', '', '0', '0', NULL, '1', NULL, '2020-12-03 05:36:06', '2021-04-14 01:09:58'),
(62, NULL, NULL, NULL, NULL, '123456', '0', 'assdbasioufbsaosdabnnklvnkdkksaknkassjmsakdjkasdnsamncdaskc', 'jenish bhai', NULL, '14399', 'EbdLWm', '1', '1', '1', '1', NULL, '2021-04-14 01:19:29', '2021-04-23 06:30:07'),
(63, NULL, NULL, NULL, NULL, '147147', '123456', '1231232124ddf13312dfgrwew', 'child account 3', NULL, NULL, NULL, '0', '0', '1', '1', NULL, '2021-04-14 01:19:57', '2021-04-23 00:30:18'),
(64, NULL, NULL, NULL, NULL, '123123', '0', 'assdbasioufbsaosdabnnklvnkdkksaknkassjmsakdjkasdnsamncdaskc', 'jenish bhai', NULL, '58720', 'eVCczb', '1', '0', '1', '1', NULL, '2021-04-14 01:22:45', '2021-04-23 06:30:09'),
(65, NULL, NULL, NULL, NULL, '159159', '123456', '1231232124ddf13312dfgrwew', 'child account 3', NULL, NULL, NULL, '0', '0', '1', '1', NULL, '2021-04-23 00:32:04', '2021-04-23 00:32:04'),
(66, NULL, NULL, NULL, NULL, '258258', '123456', '1231232124ddf13312dfgrwew', 'child account 3', NULL, NULL, NULL, '0', '0', '1', '1', NULL, '2021-04-23 00:32:14', '2021-04-23 06:30:04'),
(67, NULL, NULL, NULL, NULL, '1591591591591933964', '0', 'assdbasioufbsaosdabnnklvnkdkksaknkassjmsakdjkasdnsamncdaskc', 'jenish bhai', NULL, '1107', 'PwC8qG', '0', '0', '1', '1', NULL, '2021-04-23 00:37:28', '2021-04-23 00:42:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_12_03_111138_create_app_data_table', 2),
(5, '2020_12_04_063727_create_coin_details_table', 3),
(6, '2020_12_04_103201_create_orders_table', 4),
(7, '2020_12_04_113953_create_already_like_follows_table', 5),
(8, '2020_12_04_133719_create_purchase_coins_table', 6),
(9, '2020_12_07_125008_create_check_ip_addresses_table', 7),
(10, '2020_12_13_061914_add_discount_to_app_data_table', 8),
(11, '2020_12_14_045049_create_user_orders_table', 9),
(12, '2021_02_03_063108_create_testing_purposes_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '0 = like / 1 = follow',
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '''0''',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `short_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `needed` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recieved` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_like_log` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `reason_for_cancelorder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'reason for cancel the order',
  `fromapp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '0 = web / 1 = app',
  `order_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 = active / 0 = deactive',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `type`, `user_id`, `custom_user_id`, `username`, `post_id`, `short_code`, `needed`, `recieved`, `image_url`, `no_like_log`, `reason_for_cancelorder`, `fromapp`, `order_status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '0', '123456', '123456', NULL, '2106595947853066970122121', 'B08Idp1DszLa', '158', '120', '607690f19ffad_like_order.png', '000', NULL, '1', '1', NULL, '2021-04-14 01:21:30', '2021-04-22 23:36:02'),
(2, '0', '123456', '123456', NULL, '16219586196519651', '121sdfs123', '150323', '13', '60825176acb68_like_order.png', '2121', NULL, '0', '0', NULL, '2021-04-22 23:17:17', '2021-04-22 23:39:33'),
(3, '0', '123456', '123456', NULL, '21065959478530669701221211212121', 'B08Idp1DszLa1212', '12', '1', '608255c7dd952_like_order.png', '0', NULL, '1', '1', NULL, '2021-04-22 23:36:17', '2021-04-23 06:30:07');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_coins`
--

CREATE TABLE `purchase_coins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchased_coin` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `payment_state` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_time` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '0 = paypal 1 = razorpay',
  `transaction_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_coins`
--

INSERT INTO `purchase_coins` (`id`, `user_id`, `payment_id`, `payment_type`, `purchased_coin`, `amount`, `payment_state`, `payment_time`, `country_code`, `email`, `payment_method`, `transaction_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '123123', NULL, NULL, 5500, NULL, NULL, '2021-04-14 06:54:07', NULL, NULL, '1', 'ADAD16216513561211521', NULL, '2021-04-14 01:24:07', '2021-04-14 01:24:07'),
(2, '123123', NULL, NULL, 5500, NULL, NULL, '2021-04-23 05:22:40', NULL, NULL, '1', 'ADAD16216513561211521', NULL, '2021-04-22 23:52:40', '2021-04-22 23:52:40'),
(3, '123123', NULL, NULL, 5500, NULL, NULL, '2021-04-23 05:22:42', NULL, NULL, '1', 'ADAD16216513561211521', NULL, '2021-04-22 23:52:42', '2021-04-22 23:52:42'),
(4, '123123', NULL, NULL, 5500, NULL, NULL, '2021-04-23 05:22:43', NULL, NULL, '1', 'ADAD16216513561211521', NULL, '2021-04-22 23:52:43', '2021-04-22 23:52:43'),
(5, '123123', NULL, NULL, 5500, NULL, NULL, '2021-04-23 05:22:44', NULL, NULL, '1', 'ADAD16216513561211521', NULL, '2021-04-22 23:52:44', '2021-04-22 23:52:44'),
(6, '123123', NULL, NULL, 5500, NULL, NULL, '2021-04-23 05:22:45', NULL, NULL, '1', 'ADAD16216513561211521', NULL, '2021-04-22 23:52:45', '2021-04-22 23:52:45'),
(7, '123123', NULL, NULL, 6000, NULL, NULL, '2021-04-23 06:06:06', NULL, NULL, '1', 'ADAD16216513561211521', NULL, '2021-04-23 00:36:06', '2021-04-23 00:36:06'),
(8, '123123', NULL, NULL, 6000, NULL, NULL, '2021-04-23 06:06:10', NULL, NULL, '1', 'ADAD16216513561211521', NULL, '2021-04-23 00:36:10', '2021-04-23 00:36:10'),
(9, '123123121', 'pay_Gldrfn5UfXRLfz', 'Inapp', 6000, NULL, 'Gujarat', '1970-01-01UTC 00:00:00', 'India', 'admin@instagramlike.com', '1', 'ADAD16216513561211521', '2021-04-23 03:52:26', '2021-04-23 00:36:11', '2021-04-23 03:52:26'),
(10, '123123', NULL, NULL, 6000, NULL, NULL, '2021-04-23 12:00:09', NULL, NULL, '1', 'ADAD16216513561211521', NULL, '2021-04-23 06:30:09', '2021-04-23 06:30:09');

-- --------------------------------------------------------

--
-- Table structure for table `testing_purposes`
--

CREATE TABLE `testing_purposes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testing_purposes`
--

INSERT INTO `testing_purposes` (`id`, `data`, `created_at`, `updated_at`) VALUES
(1, '{\"purchase_data\":{\"user_id\":\"8609582088\",\"packageName\":\"getfans.getlikes.tikbooster\",\"acknowledged\":false,\"orderId\":\"transactionId.android.test.purchased\",\"productId\":50,\"token\":\"ABCDCGTDVDKIBDJKWDBIKWBDWDN\",\"developerPayload\":\"inapp:android.test.purchased:e11fd6ac-a378-4383-8295-1429e4a7803c\",\"purchaseTime\":0,\"purchaseState\":0,\"purchaseToken\":\"jcedkmpihomjapgmefcoakhl.AO-J1OwoYCyFGNlw-6ZoJR_Tofol7oZttNVhitSXV7f9AXf2OcV2c9ZsIpQCUdD5mQRrTCodJan9OVBnOEkVrjGNijH0JxoawvTbL6-cZKVMYHeziVDAZoM\"}}', '2021-02-03 01:10:59', '2021-02-03 01:10:59'),
(2, '{\"purchase_data\":{\"user_id\":\"8609582088\",\"packageName\":\"getfans.getlikes.tikbooster\",\"acknowledged\":false,\"orderId\":\"transactionId.android.test.purchased\",\"productId\":50,\"token\":\"ABCDCGTDVDKIBDJKWDBIKWBDWDN\",\"developerPayload\":\"inapp:android.test.purchased:e11fd6ac-a378-4383-8295-1429e4a7803c\",\"purchaseTime\":0,\"purchaseState\":0,\"purchaseToken\":\"jcedkmpihomjapgmefcoakhl.AO-J1OwoYCyFGNlw-6ZoJR_Tofol7oZttNVhitSXV7f9AXf2OcV2c9ZsIpQCUdD5mQRrTCodJan9OVBnOEkVrjGNijH0JxoawvTbL6-cZKVMYHeziVDAZoM\"}}', '2021-02-03 01:23:38', '2021-02-03 01:23:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `ftoken` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_coins` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_purchase` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0 = NOT PURCHASED / 1 PURCHASE',
  `is_referred` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0 = NOt REFFERED  / 1 REFFERED ',
  `fromapp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '0 = web / 1 = app',
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 = aCIVE / 0 = iNaCTIVE',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `user_id`, `parent_id`, `ftoken`, `username`, `profile_picture`, `total_coins`, `referral_code`, `is_purchase`, `is_referred`, `fromapp`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Instagram likes', 'admin@instagramlike.com', NULL, '$2y$10$uIkXMAU3tm8sLwo4aPWIje313IUjM1JTb/zxCeigjrIS.VCVnrFrO', '', '0', '', '', 'admin_60050018d33fa.jpg', '', '', '0', '0', NULL, '1', NULL, '2020-12-03 05:36:06', '2021-04-14 01:09:58'),
(62, NULL, NULL, NULL, NULL, '123456', '0', 'cD85dl1pR-W11iUF0d0v47:APA91bEkxboR8IY4yZrSwevchA-2rkpWMOWLML9VAL9x3Jtpx_4xhG0opKSFRHHRR9Zo-zqq17sboxGFyN6E5LUTZXHCDtzljbL07ntNzT1m57AjTB89pTbci1HATZNDaNcp0gLi3pvN\r\n', 'jenish bhai', NULL, '15090', 'EbdLWm', '1', '0', '1', '1', NULL, '2021-04-14 01:19:29', '2021-04-14 01:26:26'),
(63, NULL, NULL, NULL, NULL, '147147', '123456', '1231232124ddf13312dfgrwew', 'child account 3', NULL, NULL, NULL, '0', '0', '1', '1', NULL, '2021-04-14 01:19:57', '2021-04-14 01:21:45'),
(64, NULL, NULL, NULL, NULL, '123123', '0', 'assdbasioufbsaosdabnnklvnkdkksaknkassjmsakdjkasdnsamncdaskc', 'jenish bhai', NULL, '5618', 'eVCczb', '1', '0', '1', '1', NULL, '2021-04-14 01:22:45', '2021-04-14 01:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `user_orders`
--

CREATE TABLE `user_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `packageName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acknowledgementState` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'coin that purchase from the user',
  `token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `developerPayload` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchaseTime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchaseState` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchaseToken` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `inapp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `regionCode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_orders`
--

INSERT INTO `user_orders` (`id`, `user_id`, `packageName`, `acknowledgementState`, `orderId`, `productId`, `token`, `developerPayload`, `purchaseTime`, `purchaseState`, `purchaseToken`, `inapp`, `username`, `code`, `two_factor_identifier`, `regionCode`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '123456', 'com.reallikebooster.getmorelikes.increasefollowers', '1', 'GPA.3321-8259-3524-90634', '10', 'eklocfbkabhmkclaaojfneeh.AO-J1Oy1_u7jLhJE9LuPrd9KehcGlf6nFlYtACfKux0KlJ-ArI4qe4YXq1c0yRsBEFjQOBxpTOtozVNKBRabPuOkylRq1UMzb-tIid06oAlwYNTcwBvu10SudMWFkJtBytSqN6cVIBHc', 'inapp:10:04707adb-5b76-48e7-8e52-515daf6300dd', '1616127570111', '0', 'eklocfbkabhmkclaaojfneeh.AO-J1Oy1_u7jLhJE9LuPrd9KehcGlf6nFlYtACfKux0KlJ-ArI4qe4YXq1c0yRsBEFjQOBxpTOtozVNKBRabPuOkylRq1UMzb-tIid06oAlwYNTcwBvu10SudMWFkJtBytSqN6cVIBHc', NULL, NULL, NULL, NULL, 'IN', NULL, '2021-04-23 00:09:53', '2021-04-23 00:09:53'),
(20, '123456', 'com.reallikebooster.getmorelikes.increasefollowers', '1', 'GPA.3321-8259-3524-90634', '10', 'eklocfbkabhmkclaaojfneeh.AO-J1Oy1_u7jLhJE9LuPrd9KehcGlf6nFlYtACfKux0KlJ-ArI4qe4YXq1c0yRsBEFjQOBxpTOtozVNKBRabPuOkylRq1UMzb-tIid06oAlwYNTcwBvu10SudMWFkJtBytSqN6cVIBHc', 'inapp:10:04707adb-5b76-48e7-8e52-515daf6300dd', '1616127570111', '0', 'eklocfbkabhmkclaaojfneeh.AO-J1Oy1_u7jLhJE9LuPrd9KehcGlf6nFlYtACfKux0KlJ-ArI4qe4YXq1c0yRsBEFjQOBxpTOtozVNKBRabPuOkylRq1UMzb-tIid06oAlwYNTcwBvu10SudMWFkJtBytSqN6cVIBHc', NULL, NULL, NULL, NULL, 'IN', '2021-04-23 03:56:41', '2021-04-23 00:09:53', '2021-04-23 03:56:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `already_like_follows`
--
ALTER TABLE `already_like_follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_data`
--
ALTER TABLE `app_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `check_ip_addresses`
--
ALTER TABLE `check_ip_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coin_details`
--
ALTER TABLE `coin_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `get_users`
--
ALTER TABLE `get_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `purchase_coins`
--
ALTER TABLE `purchase_coins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testing_purposes`
--
ALTER TABLE `testing_purposes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_orders`
--
ALTER TABLE `user_orders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `already_like_follows`
--
ALTER TABLE `already_like_follows`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `app_data`
--
ALTER TABLE `app_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `check_ip_addresses`
--
ALTER TABLE `check_ip_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coin_details`
--
ALTER TABLE `coin_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `get_users`
--
ALTER TABLE `get_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `purchase_coins`
--
ALTER TABLE `purchase_coins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `testing_purposes`
--
ALTER TABLE `testing_purposes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `user_orders`
--
ALTER TABLE `user_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
